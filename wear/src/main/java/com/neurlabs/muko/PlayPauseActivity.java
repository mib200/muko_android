package com.neurlabs.muko;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;

/**
 * Created by Neurlabs on 12/17/2014.
 */
public class PlayPauseActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private ImageView PlayPause;
    private ImageView Next;
    private ImageView Previous;
    private ImageView IncreaseVolume;
    private ImageView DecreaseVolume;
    private ProgressBar mProgress;
    private RelativeLayout EntireView;
    private static final String SONG_CONTROL_WEAR_PATH = "/song-control-wear";
    private boolean mResolvingError=false;
    private boolean changer = true;
    private Button mBtnView;

    Node mNode; // the connected device to send the message to
    GoogleApiClient mGoogleApiClient;

    public static String ACTIVITY_IN_NOTIFICATION_BROADCAST = "com.neurlabs.muko.MSGTOWEARNOT";
    public static String SONG_CHANGE_BROADCAST = "com.neurlabs.muko.SONGCHNGDINHNDHLD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_pause_next);
        PlayPause = (ImageView)findViewById(R.id.play_pause);
        Next = (ImageView) findViewById(R.id.next);
        Previous = (ImageView) findViewById(R.id.prev);
        IncreaseVolume = (ImageView) findViewById(R.id.volIncrease);
        DecreaseVolume = (ImageView) findViewById(R.id.volDecrease);
        mProgress = (ProgressBar) findViewById(R.id.prgrssbar);
        EntireView = (RelativeLayout) findViewById(R.id.entireViews);
        mBtnView = (Button) findViewById(R.id.query_button2);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(ACTIVITY_IN_NOTIFICATION_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(songChngReceiver,
                new IntentFilter(SONG_CHANGE_BROADCAST));

        PlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("$%playpause%$");
                //TODO Find a way to get message from handheld when playing or pausing & try update using another function
                if(changer){
                    changer=false;
                    PlayPause.setImageResource(R.drawable.play_update);
                }
                else{
                    changer = true;
                    PlayPause.setImageResource(R.drawable.pause_update);
                }
            }
        });

        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress();
                sendMessage("$%next%$");
            }
        });

        Previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress();
                sendMessage("$%previous%$");
            }
        });
        IncreaseVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("$%volIncrease%$");
            }
        });
        DecreaseVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("$%volDecrease%$");
            }
        });

        mBtnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySpeechRecognizer();
            }
        });
    }

    private static final int SPEECH_REQUEST_CODE = 0;
    // Create an intent that can start the Speech Recognizer activity
    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
// Start the activity, the intent will be populated with the speech text
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    public void showProgress(){
        EntireView.setVisibility(View.GONE);
        mProgress.setVisibility(View.VISIBLE);
    }

    /**
     * Send message to mobile handheld
     */
    private void sendMessage(String query) {

        if (mNode != null && mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
            Wearable.MessageApi.sendMessage(
                    mGoogleApiClient, mNode.getId(), SONG_CONTROL_WEAR_PATH, query.getBytes()).setResultCallback(

                    new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {

                            if (!sendMessageResult.getStatus().isSuccess()) {
                                Log.e("TAG", "Failed to send message with status code: "
                                        + sendMessageResult.getStatus().getStatusCode());
                            }
                        }
                    }
            );
        }else{
            //Improve your code
        }

    }

    /**
     * Send message to mobile handheld
     */
    private static final String HELLO_WORLD_WEAR_PATH = "/hello-world-wear";
    private void sendMessage2(String query) {

        if (mNode != null && mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
            Wearable.MessageApi.sendMessage(
                    mGoogleApiClient, mNode.getId(), HELLO_WORLD_WEAR_PATH, query.getBytes()).setResultCallback(

                    new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {

                            if (!sendMessageResult.getStatus().isSuccess()) {
                                Log.e("TAG", "Failed to send message with status code: "
                                        + sendMessageResult.getStatus().getStatusCode());
                            }
                        }
                    }
            );
        }else{
            //Improve your code
        }

    }

    // This callback is invoked when the Speech Recognizer returns.
// This is where you process the intent and extract the speech text from the intent.
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            // Do something with spokenText
            EntireView.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
            Toast.makeText(this,"Fetching songs, your watch will notify you when songs start playing.",Toast.LENGTH_LONG).show();
            sendMessage2(spokenText);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {
            mGoogleApiClient.connect();
        }
    }

    /*
     * Resolve the node = the connected device to send the message to
     */
    private void resolveNode() {

        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                for (Node node : nodes.getNodes()) {
                    mNode = node;
                }
            }
        });
    }


    @Override
    public void onConnected(Bundle bundle) {
        resolveNode();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Improve your code
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Improve your code
    }

    @Override
    public void onDestroy(){

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(songChngReceiver);

        super.onDestroy();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
//            String message = intent.getStringExtra("MSG");
//            Toast.makeText(PlayPauseActivity.this,"Muko - Message Recieved", Toast.LENGTH_LONG).show();
            Boolean isPlayerPlaying = intent.getBooleanExtra("isitPlaying",false);
            if(isPlayerPlaying){
                PlayPause.setImageResource(R.drawable.pause_update);
            }
            else
                PlayPause.setImageResource(R.drawable.play_update);

        }
    };

    private BroadcastReceiver songChngReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showProgress();
        }
    };
}
