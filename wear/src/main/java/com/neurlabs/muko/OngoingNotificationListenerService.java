package com.neurlabs.muko;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Neurlabs
 */
public class OngoingNotificationListenerService extends WearableListenerService {
    private static final String TAG = OngoingNotificationListenerService.class.getSimpleName();
    private static final int NOTIFICATION_ID = 100;

    Boolean isPlayerPlaying;
    Boolean songchanged;

    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEvents);
        dataEvents.close();

        if (!mGoogleApiClient.isConnected()) {
            ConnectionResult connectionResult = mGoogleApiClient
                    .blockingConnect(30, TimeUnit.SECONDS);
            if (!connectionResult.isSuccess()) {
                Log.e(TAG, "Service failed to connect to GoogleApiClient.");
                return;
            }
        }

        for (DataEvent event : events) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                String path = event.getDataItem().getUri().getPath();
                if ("/ongoingnotification".equals(path)) {

                    sendMessagetoActivity();

                    // Get the data out of the event
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
                    final String songtitle = dataMapItem.getDataMap().getString("title");
                    final String songartist = dataMapItem.getDataMap().getString("artist");
                    final int LikeUnlikeInfo = dataMapItem.getDataMap().getInt("likeunlikeinfo");
                    //Asset asset = dataMapItem.getDataMap().getAsset(Constants.KEY_IMAGE);

                    // Build the intent to display our custom notification
                    Intent notificationIntent = new Intent(this, NotificationActivity.class);
                    notificationIntent.putExtra(NotificationActivity.EXTRA_SONGTITLE, songtitle);
                    notificationIntent.putExtra(NotificationActivity.EXTRA_SONGARTIST, songartist);
                    notificationIntent.putExtra(NotificationActivity.EXTRA_LIKEUNLIKEINFO,LikeUnlikeInfo);
                    PendingIntent notificationPendingIntent = PendingIntent.getActivity(
                            this,
                            0,
                            notificationIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);

                    // Create the ongoing notification

                    // Use this for multipage notifications
//                    Notification secondPageNotification =
//                            new Notification.Builder(this)
//                                    .extend(new Notification.WearableExtender()
//                                            .setDisplayIntent(notificationPendingIntent))
//                                    .build();

                    //Full Screen Page
                    Notification myFullScreenNotification = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentText("Full Screen Notification")
                            .extend(new NotificationCompat.WearableExtender()
                                    .setCustomSizePreset(NotificationCompat.WearableExtender.SIZE_FULL_SCREEN)
                                    .setDisplayIntent(PendingIntent.getActivity(this.getApplicationContext(), 0, new Intent(this.getApplicationContext(), PlayPauseActivity.class), 0)))
                            .build();

                    NotificationCompat.Builder notificationBuilder =
                            new NotificationCompat.Builder(this)
                                    .setSmallIcon(R.drawable.muko_app_icon)
//                                    .setContentTitle("Playing")
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.bg_300))
                                    .setContentText(songtitle)
                                    .setOnlyAlertOnce(true)
                                    .setPriority(NotificationCompat.PRIORITY_MAX)
                                    .setVibrate(new long[]{1000, 1000, 1000})
                                    .extend(new NotificationCompat.WearableExtender()
                                                    .setDisplayIntent(notificationPendingIntent)
                                                    .addPage(myFullScreenNotification)
                                    );

//                    ((NotificationManagerCompat) getSystemService(NOTIFICATION_SERVICE))
//                            .notify(NOTIFICATION_ID, notificationBuilder.build());

                    NotificationManagerCompat notificationManager =
                            NotificationManagerCompat.from(this.getApplicationContext());

                    // Issue the notification with notification manager.
                    notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());

                }
                else if("/playerstatus".equals(path)){

                    DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
                    isPlayerPlaying = dataMapItem.getDataMap().getBoolean("isPlaying");
                    songchanged = dataMapItem.getDataMap().getBoolean("sngcngd");
//                    Toast.makeText(OngoingNotificationListenerService.this,"Recieved in Listener Service",Toast.LENGTH_LONG).show();
                    if(songchanged)
                        sendMessagetoActivityabtsngchng();
                    else
                        sendMessagetoActivityinNotification(isPlayerPlaying);
                }
                else {
                    Log.d(TAG, "Unrecognized path: " + path);
                }
            }
        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equals("/dismissnotification")) {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.cancel(NOTIFICATION_ID);
        }
    }

    public static String SERVICE_BROADCAST = "com.neurlabs.muko.MSGFRMSRVWEAR";
    private void sendMessagetoActivity() {
        Intent intent = new Intent(SERVICE_BROADCAST);
        // add data
//        intent.putExtra("MSG", "Hiiiiiiiiiiiii");
//        sendBroadcast(intent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public static String ACTIVITY_IN_NOTIFICATION_BROADCAST = "com.neurlabs.muko.MSGTOWEARNOT";
    private void sendMessagetoActivityinNotification(Boolean isPlayerPlaying) {
        Intent intent = new Intent(ACTIVITY_IN_NOTIFICATION_BROADCAST);
        // add data
        intent.putExtra("isitPlaying",isPlayerPlaying);
//        sendBroadcast(intent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public static String SONG_CHANGE_BROADCAST = "com.neurlabs.muko.SONGCHNGDINHNDHLD";
    private void sendMessagetoActivityabtsngchng() {
        Intent intent2 = new Intent(SONG_CHANGE_BROADCAST);
        // add data
//        intent.putExtra("isitPlaying",isPlayerPlaying);
//        sendBroadcast(intent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
    }
}
