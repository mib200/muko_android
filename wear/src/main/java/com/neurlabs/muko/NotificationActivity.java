package com.neurlabs.muko;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.io.InputStream;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Activity that is in the big view notification
 *
 * Created by Neurlabs
 */
public class NotificationActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private static final String TAG = NotificationActivity.class.getSimpleName();

    public static final String EXTRA_SONGTITLE = "title";
    public static final String EXTRA_SONGARTIST = "artist";
    public static final String EXTRA_LIKEUNLIKEINFO = "likeandunlikeinfo";

    Node mNode; // the connected device to send the message to
    GoogleApiClient mGoogleApiClient;

//    private ImageView mImageView;
    private TextView mTextView;
    private TextView mTextArtistView;
    private ImageView mCircleBtn;
    private ImageView mLikeBtn;
    private ImageView mUnLikeBtn;
    private LinearLayout mLikeUnlikeSelector;
    private ImageView mLikeSelector;
    private ImageView mDislikeSelector;
    private String mSongName;
    private String mArtistName;
    Random random;
    private int mlikeOrUnlike;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

//        mImageView = (ImageView) findViewById(R.id.image_view);
        mTextView = (TextView) findViewById(R.id.text_song);
        mTextArtistView = (TextView) findViewById(R.id.text_artist);
        mCircleBtn = (ImageView) findViewById(R.id.wear_not_circle_button);
        mLikeBtn = (ImageView) findViewById(R.id.wear_not_like_button);
        mUnLikeBtn = (ImageView) findViewById(R.id.wear_not_dislike_button);
        mLikeUnlikeSelector = (LinearLayout) findViewById(R.id.wear_not_like_unlike_selector);
        mLikeSelector = (ImageView) findViewById(R.id.wear_not_like_selection);
        mDislikeSelector = (ImageView) findViewById(R.id.wear_not_dislike_selection);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }

        Intent intent = getIntent();
        if (intent != null) {
            mTextView.setText(intent.getStringExtra(EXTRA_SONGTITLE));
            mTextArtistView.setText(intent.getStringExtra(EXTRA_SONGARTIST));
            mlikeOrUnlike = intent.getIntExtra(EXTRA_LIKEUNLIKEINFO,2);
            mSongName = intent.getStringExtra(EXTRA_SONGTITLE);
            mArtistName = intent.getStringExtra(EXTRA_SONGARTIST);

//            final Asset asset = intent.getParcelableExtra(EXTRA_IMAGE);

//            loadBitmapFromAsset(this, asset, mImageView);

            //TODO Fetch User LikeUnlike Info on Current Song and set Visibility accordingly
            if(mlikeOrUnlike == 2){
                mLikeUnlikeSelector.setVisibility(View.GONE);
                mCircleBtn.setVisibility(View.VISIBLE);
            }
            else if(mlikeOrUnlike == 1){
                mLikeUnlikeSelector.setVisibility(View.GONE);
                mLikeBtn.setVisibility(View.VISIBLE);
            }
            else
            {
                mLikeUnlikeSelector.setVisibility(View.GONE);
                mUnLikeBtn.setVisibility(View.VISIBLE);
            }

        }

        random = new Random();
        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextView.setTextColor(Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256)));
            }
        });

        mCircleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCircleBtn.setVisibility(View.GONE);
                mLikeUnlikeSelector.setVisibility(View.VISIBLE);

            }
        });

        mLikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mLikeBtn.setVisibility(View.GONE);
                mLikeUnlikeSelector.setVisibility(View.VISIBLE);

            }
        });

        mUnLikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mUnLikeBtn.setVisibility(View.GONE);
                mLikeUnlikeSelector.setVisibility(View.VISIBLE);

            }
        });

        mLikeSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mLikeUnlikeSelector.setVisibility(View.GONE);
                mLikeBtn.setVisibility(View.VISIBLE);
                //TODO User Liked the Song
                mlikeOrUnlike = 1;
                sendMessage();

            }
        });

        mDislikeSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mLikeUnlikeSelector.setVisibility(View.GONE);
                mUnLikeBtn.setVisibility(View.VISIBLE);
                //TODO User Disliked the Song
                mlikeOrUnlike = 0;
                sendMessage();

            }
        });

    }

    /**
     * Send message to mobile handheld
     */
    private void sendMessage() {

        if (mGoogleApiClient.isConnected()) {
            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create("/updtLikeUnlikeJSON");
//            Toast.makeText(NotificationActivity.this,"Google API connected and sending message",Toast.LENGTH_SHORT).show();
            // Add data to the request
            putDataMapRequest.getDataMap().putString("mSongName", mSongName);
            putDataMapRequest.getDataMap().putString("mArtistName", mArtistName);
            putDataMapRequest.getDataMap().putInt("mlikeOrUnlike", mlikeOrUnlike);
            PutDataRequest request = putDataMapRequest.asPutDataRequest();

            Wearable.DataApi.putDataItem(mGoogleApiClient, request)
                    .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                        @Override
                        public void onResult(DataApi.DataItemResult dataItemResult) {
                            Log.d(TAG, "putDataItem status: " + dataItemResult.getStatus().toString());
                        }
                    });
        }
        else
            Toast.makeText(NotificationActivity.this,"Google API not connected",Toast.LENGTH_SHORT).show();

    }

//    private static void loadBitmapFromAsset(final Context context, final Asset asset, final ImageView target) {
//        if (asset == null) {
//            throw new IllegalArgumentException("Asset must be non-null");
//        }
//
//        new AsyncTask<Asset, Void, Bitmap>() {
//            @Override
//            protected Bitmap doInBackground(Asset... assets) {
//                GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
//                        .addApi(Wearable.API)
//                        .build();
//                ConnectionResult result =
//                        googleApiClient.blockingConnect(
//                                1000, TimeUnit.MILLISECONDS);
//                if (!result.isSuccess()) {
//                    return null;
//                }
//
//                // convert asset into a file descriptor and block until it's ready
//                InputStream assetInputStream = Wearable.DataApi.getFdForAsset(
//                        googleApiClient, assets[0]).await().getInputStream();
//                googleApiClient.disconnect();
//
//                if (assetInputStream == null) {
//                    Log.w(TAG, "Requested an unknown Asset.");
//                    return null;
//                }
//
//                // decode the stream into a bitmap
//                return BitmapFactory.decodeStream(assetInputStream);
//            }
//
//            @Override
//            protected void onPostExecute(Bitmap bitmap) {
//                if (bitmap != null) {
//                    target.setImageBitmap(bitmap);
//                }
//            }
//        }.execute(asset);
//    }

    /*
     * Resolve the node = the connected device to send the message to
     */
    private void resolveNode() {

        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                for (Node node : nodes.getNodes()) {
                    mNode = node;
                }
            }
        });
    }


    @Override
    public void onConnected(Bundle bundle) {
        resolveNode();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onDestroy(){
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }
}