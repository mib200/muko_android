package com.neurlabs.muko.application;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;

public class ParseApplication extends Application
{ 
    @Override
    public void onCreate()
    {
        super.onCreate();
 
        // Add initialization code here
        //Parse.initialize(this, "Application ID", "Client Key");

        //MUKO ID and KEY
        Parse.initialize(this, "DsPPzIvsmN6zgCjiJcmxOo12Q2RcHtwbShUdhQU5", "whqKHVCvc7VoYW73ZTQQPFPNRle1TIMJ9vCNu2NK");

        //android MuKoAndroid
//        Parse.initialize(this, "pZyQ8jZPi4q9Zpxv3wt9hBqLkQK6ZyfWed80GrxE", "fPeE78cjeuDBhb1gM6gaUoHliDq3E1dxkIQtzjpM");
        
        //Keerthi Test Account
        //Parse.initialize(this, "2Adlm72Wx1JdoK9VKyRnDerzpjJpSsdFWt6u7cg9", "BEWS1sINKhIbSWQkyEYZUqlQEptMG1JfVUTIvZmX");
        
        ParseACL defaultACL = new ParseACL();
 
        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        
        ParseACL.setDefaultACL(defaultACL, true);
    }

}
