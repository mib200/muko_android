package com.neurlabs.muko;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neurlabs.muko.adapters.GenreListAdapter;
import com.neurlabs.muko.models.GenreListModel;
import com.neurlabs.muko.services.MusicService;
import com.neurlabs.muko.utilis.InternetStatus;
import com.neurlabs.muko.utilis.TinyDB;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class GenreListActivity extends ActionBarActivity
{

    ListView genreListView;
    List<ParseObject> parseListObject;
    ProgressDialog genreProgressDialog;
    GenreListAdapter genreAdapter;
    private List<GenreListModel> genreList = null;
    ArrayList<Integer> checked_checkbox;
    ImageView iv_back;
    RelativeLayout rl_header;

    Button btn_genreList_next;
    Button btn_genreList_clear;
    Button btn_genreList_save;


    InternetStatus internetStatus = new InternetStatus();
    Boolean netStatus;

    TextView tv_genreHint;
    TextView tv_genreTitle;
    SharedPreferences settings;
    SharedPreferences nav_check;
    private static String userKey = null;
    JSONArray mJSONArray_genre = new JSONArray();


    private ArrayList<String> genrelistCheckbox = new ArrayList<String>();

    JSONArray parseJsonChecked_status = new JSONArray();
    private ArrayList<String> parseArrayChecked_status = new ArrayList<String>();

    MusicService musicSrv;

    private View footer_genre;

    private static int icons[] = {R.drawable.a_rock_icon, R.drawable.b_indie_icon,
            R.drawable.c_electronic_icon, R.drawable.d_pop_icon, R.drawable.e_metal_icon,R.drawable.f_punk_icon,
            R.drawable.g_folk_icon,	R.drawable.h_jazz_icon, R.drawable.i_hardcore_icon, R.drawable.j_country_icon,
            R.drawable.k_classical_icon,R.drawable.l_hip_hop_icon,R.drawable.m_soul_icon, R.drawable.n_ambient_icon,
            R.drawable.o_emo_icon, R.drawable.p_acoustic_icon, R.drawable.q_blues_icon, R.drawable.r_r_b_icon,
            R.drawable.s_reggae_icon, R.drawable.t_latin_icon, R.drawable.u_world_icon};

    @SuppressLint("NewApi") @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
//		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        netStatus = internetStatus.isOnline(getApplicationContext());
        Typeface typeface_muko = Typeface.createFromAsset(getAssets(),"futura-condensed-medium.ttf");
        nav_check = getApplicationContext().getSharedPreferences("Muko_nav_check", MODE_PRIVATE);

        iv_back = (ImageView) findViewById(R.id.genre_list_activity_layout_imageView_back);
        btn_genreList_next = (Button) findViewById(R.id.genre_list_activity_button_next);

        btn_genreList_clear = (Button) findViewById(R.id.genre_list_activity_button_clear);
        btn_genreList_save = (Button) findViewById(R.id.genre_list_activity_button_save);

        tv_genreHint = (TextView) findViewById(R.id.genre_list_activity_textView_hint);
        tv_genreTitle = (TextView) findViewById(R.id.genre_list_activity_layout_textView_title);
        rl_header = (RelativeLayout) findViewById(R.id.genre_list_activity_relativeLayout);

        btn_genreList_save.setVisibility(View.INVISIBLE);
        btn_genreList_clear.setEnabled(false);
        btn_genreList_clear.setBackgroundColor(Color.GRAY);

        btn_genreList_next.setTypeface(typeface_muko);
        btn_genreList_clear.setTypeface(typeface_muko);
        btn_genreList_save.setTypeface(typeface_muko);
        tv_genreHint.setTypeface(typeface_muko);
        tv_genreTitle.setTypeface(typeface_muko);

        iv_back.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // Navigates back to the previous activity
                dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            }
        });

        btn_genreList_next.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                netStatus = internetStatus.isOnline(getApplicationContext());
                if(netStatus)
                {
                    genreProgressDialog = new ProgressDialog(GenreListActivity.this);
                    // Set progressdialog title
                    genreProgressDialog.setTitle("Muko");
                    // Set progressdialog message
                    genreProgressDialog.setMessage("Loading...");
                    genreProgressDialog.setIndeterminate(false);
                    genreProgressDialog.setCancelable(false);
                    genreProgressDialog.setCanceledOnTouchOutside(false);
                    // Show progressdialog
                    genreProgressDialog.show();

                    TinyDB tinydb = new TinyDB(GenreListActivity.this);
                    tinydb.putList("genreList", genrelistCheckbox);

                    SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                    String customerObjectId = muko_pref.getString("customerObjectId", null);          // getting String
                    final String userKey = muko_pref.getString("userKey", null);
                    final String objectid = muko_pref.getString("objectid", null);

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");

                    // Retrieve the object by id
                    query.getInBackground(objectid, new GetCallback<ParseObject>() {
                        public void done(ParseObject gameScore, ParseException e) {
                            if (e == null) {
                                // Now let's update it with some new data. In this case, only cheatMode and score
                                // will get sent to the Parse Cloud. playerName hasn't changed.
                                gameScore.put("genres_selected", genrelistCheckbox);

                                gameScore.saveInBackground(new SaveCallback() {

                                    @Override
                                    public void done(ParseException e) {
                                        // TODO Auto-generated method stub
                                        if(e==null)
                                        {
                                            //Toast.makeText(getApplicationContext(), "genres_selected Has Been Updated!",Toast.LENGTH_SHORT).show();
                                            genreProgressDialog.dismiss();
                                            Intent artistIntent = new Intent(GenreListActivity.this,ArtistListActivity.class);
                                            startActivity(artistIntent);
                                            finish();
                                        }
                                        else
                                        {
                                            genreProgressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Exception/n"+e, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                else
                {
                    genreProgressDialog.dismiss();
                    alert("Need internet connection");
                }

            }
        });

//		btn_genreList_skip.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent artistListIntent = new Intent(GenreListActivity.this,HomeActivity.class);
//				startActivity(artistListIntent);
//			}
//		});

        btn_genreList_clear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                netStatus = internetStatus.isOnline(getApplicationContext());
                if(netStatus)
                {
                    genreAdapter.clearChecked();
                    genrelistCheckbox.clear();
                    btn_genreList_clear.setBackgroundColor(Color.GRAY);

                    if(userKey == null || userKey.isEmpty())
                    {
                        btn_genreList_save.setVisibility(View.VISIBLE);
                        btn_genreList_next.setVisibility(View.INVISIBLE);
                        tv_genreHint.setText("Pick more artist, or go back");
                    }
                    else
                    {
                        btn_genreList_save.setVisibility(View.INVISIBLE);
                        btn_genreList_next.setVisibility(View.VISIBLE);
                        tv_genreHint.setText("Pick more artist, or go next");
                    }
                }
                else
                {
                    alert("Need internet connection");
                }

            }
        });

        btn_genreList_save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                netStatus = internetStatus.isOnline(getApplicationContext());
                if(netStatus)
                {
                    genreProgressDialog = new ProgressDialog(GenreListActivity.this);
                    // Set progressdialog title
                    genreProgressDialog.setTitle("Muko");
                    // Set progressdialog message
                    genreProgressDialog.setMessage("Loading...");
                    genreProgressDialog.setIndeterminate(false);
                    genreProgressDialog.setCancelable(false);
                    genreProgressDialog.setCanceledOnTouchOutside(false);
                    // Show progressdialog
                    genreProgressDialog.show();


                    TinyDB tinydb = new TinyDB(GenreListActivity.this);
                    tinydb.putList("genreList", genrelistCheckbox);

                    SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                    String customerObjectId = muko_pref.getString("customerObjectId", null);          // getting String
                    final String userKey = muko_pref.getString("userKey", null);
                    final String objectid = muko_pref.getString("objectid", null);

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");

                    // Retrieve the object by id
                    query.getInBackground(objectid, new GetCallback<ParseObject>() {
                        public void done(ParseObject gameScore, ParseException e) {
                            if (e == null) {
                                // Now let's update it with some new data. In this case, only cheatMode and score
                                // will get sent to the Parse Cloud. playerName hasn't changed.
                                gameScore.put("genres_selected", genrelistCheckbox);
                                gameScore.saveInBackground(new SaveCallback() {

                                    @Override
                                    public void done(ParseException e) {
                                        // TODO Auto-generated method stub
                                        if(e==null)
                                        {
                                            //Toast.makeText(getApplicationContext(), "genres_selected Has Been Updated!",Toast.LENGTH_SHORT).show();
                                            genreProgressDialog.dismiss();
                                            dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                                            dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
                                        }
                                        else
                                        {
                                            genreProgressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Exception/n"+e, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                else
                {
                    genreProgressDialog.dismiss();
                    alert("Need internet connection");
                }

            }
        });

        userKey = nav_check.getString("userKey", null);

        if(userKey == null || userKey.isEmpty())
        {
            rl_header.setVisibility(View.VISIBLE);
            btn_genreList_save.setVisibility(View.VISIBLE);
            btn_genreList_next.setVisibility(View.INVISIBLE);
            tv_genreHint.setText("Pick genres you like or go back");
        }
        else
        {
            rl_header.setVisibility(View.GONE);
            btn_genreList_save.setVisibility(View.INVISIBLE);
            btn_genreList_save.setEnabled(true);
            btn_genreList_next.setVisibility(View.VISIBLE);
            tv_genreHint.setText("Pick genres you like or go next");
        }

        if(netStatus)
        {
            getCheckedGenres();
        }
        else
        {
            btn_genreList_save.setEnabled(false);
            alert("Need Internet Connection");
        }
    }




    // RemoteDataTask AsyncTask
    private class GenresDataTask extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            // Create a progressdialog
            genreProgressDialog = new ProgressDialog(GenreListActivity.this);
            // Set progressdialog title
            genreProgressDialog.setTitle("Muko");
            // Set progressdialog message
            genreProgressDialog.setMessage("Loading...");
            genreProgressDialog.setIndeterminate(false);
            genreProgressDialog.setCancelable(false);
            genreProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            genreProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            // Create the array
            genreList = new ArrayList<GenreListModel>();
            checked_checkbox = new ArrayList<Integer>();
            try
            {
                // Locate the class table named "Genres" in Parse.com
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Genres");
                // Locate the column named "key" in Parse.com and order list
                // by ascending
                //query.orderByAscending("key");
                parseListObject = query.find();
                for (ParseObject genreObject : parseListObject)
                {
//					int size = parseListObject.size();
//					for(int i=0;i<size;i++)
//					{
//						
//					}

                    GenreListModel GenreModel = new GenreListModel();
                    GenreModel.setGenre((String) genreObject.get("genreTitle"));
                    String genrecheck = genreObject.getString("genreTitle");
                    GenreModel.setGenreChecked(0);
                    genreList.add(GenreModel);

                    if(parseArrayChecked_status.contains(genrecheck))
                    {
                        checked_checkbox.add(1);
                    }
                    else
                    {
                        checked_checkbox.add(0);
                    }
                }
            }
            catch (ParseException e)
            {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            // Locate the listview in genre_list_activity_listview.xml
            genreListView = (ListView) findViewById(R.id.genre_list_activity_listview);

            genreListView.getCheckedItemPosition();
            // LoadMore button
            footerLayoutInflate();
            genreListView.addFooterView(footer_genre);

            // Pass the results into GenreListAdapter.java
            genreAdapter = new GenreListAdapter(GenreListActivity.this,genreList,icons,checked_checkbox);

            // Binds the Adapter to the ListView
            genreListView.setAdapter(genreAdapter);
            // Close the progressdialog
            genreProgressDialog.dismiss();
        }
    }

    public void footerLayoutInflate()
    {
        LayoutInflater inflater = (LayoutInflater) super.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        footer_genre = inflater.inflate(R.layout.genre_list_footer_layout, null);
    }

    public void buttonsState()
    {
        btn_genreList_clear.setEnabled(true);
        btn_genreList_clear.setBackgroundColor(getResources().getColor(R.color.muko_button_blue));
    }

    public void buttonsStateSkipNext()
    {
        btn_genreList_clear.setEnabled(false);
        btn_genreList_clear.setBackgroundColor(Color.GRAY);
    }


    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        //super.onListItemClick(l, v, position, id);
        SparseBooleanArray checked = genreListView.getCheckedItemPositions();
        for (int i = 0; i < genreList.size(); i++)
        {
            Log.i("ListViewTest", genreList.get(i) + ": " + checked.get(i));
            Toast.makeText(getApplicationContext(), genreList.get(i) + ": " + checked.get(i), Toast.LENGTH_SHORT).show();
        }
    }

    public void addGenre(String Artist)
    {
        ArrayList<String> testarray = new ArrayList<String>();
        testarray = genrelistCheckbox;
        genrelistCheckbox.add(Artist);



        testarray = genrelistCheckbox;
        String test = genrelistCheckbox.toString().replaceAll("\\[", "").replaceAll("\\]","");
    }

    /**
     * Removes one genre from the array. Accepts Genre name parameter from the adapter.
     * Called when we uncheck the selected genre
     */
    public void removeArtist(String Artist)
    {
        genrelistCheckbox.remove(Artist);

        String test = genrelistCheckbox.toString().replaceAll("\\[", "").replaceAll("\\]","");
    }

    /**
     * Method to Get Genres from the parse
     * */
    public void getCheckedGenres()
    {
        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
        final String userKey = muko_pref.getString("userKey", null);
        final String objectid = muko_pref.getString("objectid", null);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");
        query.whereEqualTo("userKey",userKey);
        query.findInBackground(new FindCallback<ParseObject>()
        {
            @Override
            public void done(List<ParseObject> parseList, ParseException e)
            {
                if (e == null)
                {
                    for (ParseObject parse : parseList)
                    {
                        String objectID = parse.getObjectId();
                        String firstname = parse.getString("firstName");

                        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                        Editor editor_parse = muko_pref.edit();
                        editor_parse.putString("objectid", objectID);
                        editor_parse.commit();

                        parseJsonChecked_status = parse.getJSONArray("genres_selected");
                        JSONArray test = new JSONArray();
                        test = parseJsonChecked_status;

                        if (parseJsonChecked_status != null)
                        {
                            for (int i=0;i<parseJsonChecked_status.length();i++)
                            {
                                try
                                {
                                    parseArrayChecked_status.add(parseJsonChecked_status.get(i).toString());
                                    genrelistCheckbox = parseArrayChecked_status;
                                }
                                catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                        String artist = parseArrayChecked_status.toString().replaceAll("\\[", "").replaceAll("\\]","");
                        int artistSize = parseArrayChecked_status.size();
                        if(artistSize<1)
                        {
                            btn_genreList_clear.setEnabled(false);
                            btn_genreList_clear.setBackgroundColor(Color.GRAY);
                        }
                        else
                        {
                            btn_genreList_clear.setBackgroundColor(getResources().getColor(R.color.muko_button_blue));
                            btn_genreList_clear.setEnabled(true);
                        }
                    }
                    new GenresDataTask().execute();
                }
                else
                {

                }
            }
        });
    }

    /**
     * Shows Alert Dialog. Accept string parameter
     * */
    @SuppressWarnings("deprecation")
    public void alert(String message)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(GenreListActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("MUKO");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //TODO Close the app
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

}



