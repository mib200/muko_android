package com.neurlabs.muko;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.neurlabs.muko.adapters.MukoPlayListAdapter;
import com.neurlabs.muko.models.TracksModel;
import com.neurlabs.muko.services.MusicService;
import com.neurlabs.muko.services.MusicService.MusicBinder;
import com.neurlabs.muko.services.SaveLikeDislikeToParse;
import com.neurlabs.muko.services.WearNotifierServiceIntent;
import com.neurlabs.muko.utilis.CustomKeyboard;
import com.neurlabs.muko.utilis.InternetStatus;
import com.neurlabs.muko.utilis.TinyDB;
import com.nuance.nmdp.speechkit.SpeechKit;
import com.rdio.android.api.Rdio;
import com.rdio.android.api.RdioApiCallback;
import com.rdio.android.api.RdioListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.rdio.android.api.OAuth1WebViewActivity;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import android.content.IntentFilter;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

public class MukoPlayListActivity extends ActionBarActivity implements RdioListener {
	
	private static final String TAG = "MukoPlayListActivity";
	
	private static final String appKey = "kcdb9ukqh5y2acahf39au6b3";
	private static final String appSecret = "5kNJ23JCJA";
	
	private static String accessToken = null;
	private static String accessTokenSecret = null;
	
	private static final String PREF_ACCESSTOKEN = "prefs.accesstoken";
	private static final String PREF_ACCESSTOKENSECRET = "prefs.accesstokensecret";
	
	private MusicService musicSrv;
	
	public static Rdio rdio = null;
	private ImageView playPause;
	private MediaPlayer player;
	private Queue<TracksModel> trackQueue = null;
	private ListView SongList;
	int playposition = 0;
	ArrayList<TracksModel> trackls;
	private boolean musicBound=false;
    public static boolean firsttimeload;
	private Intent playIntent;
	private static SpeechKit _speechKit;
	EditText QueryDisplay;
	TextView CurrentSong;
	TextView CurrentArtist;
	TextView TotalDuration;
	TextView CurrentPlayTime;
	SeekBar timeLine;
	private String currentplaybacktime = "00:00";

	protected int networkcheck = 0;
	public static Handler monitorHandler;
	AlertDialog.Builder alertDialogBuilder;
	AlertDialog alertDialog;
    AlertDialog.Builder AAlertDialogBuilder;

	protected boolean rdioready = false;
    MukoPlayListAdapter songadptr;

    String key;
    String songName;
    String artistName;
    int isTrueorFalse = 2;
    private ProgressDialog getTrackLoadingDialog;
    private boolean disablenetcheck = false;
    private int musicDuration;
    public static String[] songname;
    public static String[] artistname;
	private static String UserQuery = null;
	private static String url = "http://muko-music-search.elasticbeanstalk.com/api/search/queryinjson";
	private int lastqueryelementpos;
	private int k;
	private String USERID;
    private String wearQuery = null;
    ImageView iv_mukoPlayListClose;
    Button btn_mukoPlayListClose;
    JSONObject songsThumbInfoJsonObject = new JSONObject();
    JSONObject like_dislike_JsonObject = new JSONObject();
    static ArrayList<String> genrelist = new ArrayList<String>();
    static ArrayList<String> artistlist = new ArrayList<String>();
    RelativeLayout rl_mainLayout;

    CustomKeyboard keyboard = new CustomKeyboard();

    SharedPreferences speechText;
    InternetStatus internetStatus = new InternetStatus();
    Boolean netStatus;

    PowerManager pm;

	private String speechQuery;

	private ProgressDialog progressDialog;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    getSupportActionBar().hide();
		setVolumeControlStream(AudioManager.STREAM_MUSIC); // So that the 'Media Volume' applies to this activity
		setContentView(R.layout.activity_muko_play_list);

        netStatus = internetStatus.isOnline(getApplicationContext());
        Typeface typeface_muko = Typeface.createFromAsset(getAssets(),"futura-condensed-medium.ttf");
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        TinyDB tinydb = new TinyDB(MukoPlayListActivity.this);
        genrelist = tinydb.getList("genreList");
        artistlist = tinydb.getList("artistList");

        RecieverActivator();


		SongList = (ListView) findViewById(R.id.muko_play_list_activity_listView);
		CurrentArtist = (TextView) findViewById(R.id.muko_play_list_activity_textview_artistname);
		CurrentSong = (TextView) findViewById(R.id.muko_play_list_activity_textview_songname);
//		TotalDuration = (TextView) findViewById(R.id.total_duration);
//		CurrentPlayTime = (TextView) findViewById(R.id.current_playtime);
		timeLine = (SeekBar)findViewById(R.id.muko_play_list_activity_seekbar);
		QueryDisplay = (EditText) findViewById(R.id.muko_play_list_activity_edittext_searchresult);
		iv_mukoPlayListClose = (ImageView) findViewById(R.id.muko_play_list_activity_imageView_close);
		btn_mukoPlayListClose = (Button) findViewById(R.id.muko_play_list_activity_button_close);
        rl_mainLayout = (RelativeLayout) findViewById(R.id.muko_play_list_activity_mainlayout);

        QueryDisplay.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        CurrentArtist.setTypeface(typeface_muko);
        CurrentSong.setTypeface(typeface_muko);
        QueryDisplay.setTypeface(typeface_muko);

        QueryDisplay.setCursorVisible(false);
        keyboardCheck(rl_mainLayout);

        speechText = getSharedPreferences("LASTUSERQUERY", MODE_PRIVATE);

		//Setting up Seekbar
		timeLine.setThumb(null);
		//timeLine.setEnabled(false);
		timeLine.setOnTouchListener(new OnTouchListener(){
			@Override
	        public boolean onTouch(View v, MotionEvent event) {
	            return true;
	        }
	    });
		//timeLine.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY));
		setProgressBarColor(timeLine,getResources().getColor(R.color.muko_seekbar_blue));


		progressDialog = new ProgressDialog(MukoPlayListActivity.this);
		// Set progressdialog title
		progressDialog.setTitle("Muko");
		// Set progressdialog message
		progressDialog.setMessage("Loading...");
		progressDialog.setIndeterminate(false);
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);

        Intent wearIntent = getIntent();
        wearQuery = wearIntent.getStringExtra("Query");

        //TODO Some scenarios getIntent still returns value;
        Log.wtf("wearQuery",wearQuery);
        
        speechQuery = wearIntent.getStringExtra("speechquery");
        try {
        	songsThumbInfoJsonObject = new JSONObject(getIntent().getStringExtra("songsThumbInfo_Object"));
        	like_dislike_JsonObject = songsThumbInfoJsonObject;
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        QueryDisplay.setText(speechQuery);

        rl_mainLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    songadptr.disableLikeUnlike();
                }
                catch(NullPointerException e){
                    Log.e("SONGADAPTR IS NULL","IT USUALLY HAPPENS WHEN MUKO SEARCH CAUSES NULL POINTER EXCEPTION");
                }
            }
        });

        btn_mukoPlayListClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

                Intent intent = new Intent(MukoPlayListActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

//                musicSrv.stopForeground();

		    	finish();
		    	
			}
		});

        QueryDisplay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                QueryDisplay.setCursorVisible(true);
            }
        });
        
        QueryDisplay.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    QueryDisplay.setCursorVisible(false);
                	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        			imm.hideSoftInputFromWindow(QueryDisplay.getWindowToken(), 0);
                	
    	        	rdio = musicSrv.returnRDIOObject();
      	        	musicSrv.stopPlayerCompletely();
      	        	UserQuery = v.getText().toString();
                    saveLastUserQuery(UserQuery);
      	        	progressDialog.show();
      	        	new HttpAsyncTask().execute(url);
                    return true;
                }
                return false;
            }
        });

        
				final ImageView nextbutton = (ImageView)findViewById(R.id.muko_play_list_activity_imageView_next);
				nextbutton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {

						progressDialog.show();
						musicSrv.setplayerinitializingbooltofalse();
                        networkcheck = 0;
						musicSrv.nextSong();
                        if(trackls!=null)
						    musicSrv.next(true);
                        else {
                            progressDialog.dismiss();
                            Toast.makeText(MukoPlayListActivity.this,"Nothing to play",Toast.LENGTH_SHORT).show();
                        }
					}
				});

				playPause = (ImageView)findViewById(R.id.muko_play_list_activity_imageView_play);
				playPause.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						playPause();
					}
				});
				
				SongList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> a, View v, int position,	long id) {
						//Code to get clicked song details
                        progressDialog.show();
                        songadptr.disableLikeUnlike();
                        networkcheck = 0;
						playposition = position;
                        musicSrv.setplayerinitializingbooltofalse();
						musicSrv.setSong(playposition);
						musicSrv.next(true);
					}
				});

                SongList.setFocusable(false);
                SongList.setFocusableInTouchMode(false);
				
				alertDialogBuilder = new AlertDialog.Builder(this);
		 
					// set title
					alertDialogBuilder.setTitle("Network Issue Detected");
		 
					// set dialog message
					alertDialogBuilder
						.setMessage("Your network connectivity seems to have a problem, " +
                                "you can wait and see if the player is starting up in which case this alert will disappear" +
								"or you can quit and try again later.")
						.setCancelable(false)
						.setPositiveButton("Quit",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// if this button is clicked, close
								// current activity
								networkcheck = 0;
								finish();
							}
						  });
		 
						// create alert dialog
					alertDialog = alertDialogBuilder.create();
					setVolumeControlStream(AudioManager.STREAM_MUSIC); 
		 
						// show it
						
				
				ScheduledExecutorService myScheduledExecutorService = Executors.newScheduledThreadPool(1);
		        
		        myScheduledExecutorService.scheduleWithFixedDelay(
		          new Runnable(){
		     @Override
		     public void run() {
		      monitorHandler.sendMessage(monitorHandler.obtainMessage());
		     }}, 
		          200, //initialDelay
		          200, //delay
		          TimeUnit.MILLISECONDS);
		        
		        
				
		        try{
				monitorHandler = new Handler(){					  

					@Override
					  public void handleMessage(Message msg) {
						
					   //mediaPlayerMonitor();
						if((musicSrv!=null && musicSrv.isPng()) || (musicSrv!=null && musicSrv.hasplayerbeenstartedefore())){
							currentplaybacktime  = getDuration(Integer.toString(musicSrv.getPosn()));
                            //TODO Must occassionally check whether rdio fixed the issue
                            //below code is to handle rdio glitch as duration for
                            //some 30 second clips are not returned(I confirmed its returning for some songs).
                            musicDuration = musicSrv.getDur();
                            if(musicDuration==0)
                                musicDuration = 30000;
							timeLine.setMax(musicDuration);
						    timeLine.setProgress(musicSrv.getPosn());
//							TotalDuration.setText("/"+getDuration(Integer.toString(musicDuration)));
						    String trimsongname = musicSrv.currentSongName();
						    if(musicSrv.currentSongName().length()>30)
						    	trimsongname = trimsongname.substring(0, 30)+"...";
							CurrentSong.setText(trimsongname);
							CurrentArtist.setText(musicSrv.currentArtistName());
                            //Changes list row color
//                            songadptr.layoutBackgroundChange(musicSrv.currentSongId());
//							CurrentPlayTime.setText(currentplaybacktime);
							updatePlayPause(musicSrv.isPng());
                            networkcheck = 0;
                            //If for some reason the network picked up and started playing
                            //Dismiss the annoying notification
                            if(alertDialog.isShowing()){
                                alertDialog.dismiss();
                            }
                            
                            if(progressDialog.isShowing()){
                            	progressDialog.dismiss();
                            }
						}			
						networkcheck ++;
						if(networkcheck>75 && !rdioready && !disablenetcheck)
                            if(!isFinishing())//this is to prevent a crash which can happen when the user exits and the activity is still finishing
                            {

                                networkcheck = 0;
                            }
					  
							}
					  };
		        }
		        catch(Exception e){
		        	Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG).show();
		        }




        if(playIntent == null)
            playIntent = new Intent(this, MusicService.class);
        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
        startService(playIntent);
		
	}

    /**
     * Start Service Intent to save User Like/Dislike Information
     */
    public void startServiceIntentforLikeDislike() {
        Intent intent = new Intent(this, SaveLikeDislikeToParse.class);
        // add infos for the service which file to download and where to store
        intent.putExtra("thumbinfotoIntentService", like_dislike_JsonObject.toString());
        Log.v("startServiceIntentforLikeDislike()","Service Intent Starting");
        startService(intent);
    }

    /**
     * Start Service Intent to notify Wearable about User exit from playlist
     * this will ensure wear gets notified even when the user quits suddenly from recent menu
     */
    public void startServiceIntentforNotifyingWearable() {
        Intent intent = new Intent(this, WearNotifierServiceIntent.class);
        Log.v("startServiceIntentforNotifyingWearable()","Service Intent Starting");
        startService(intent);
    }

    /***
     * Save's Last User Query to Shard Preference
     * @param newQuery
     */
    public void saveLastUserQuery(String newQuery){
        Editor x = speechText.edit();
        x.putString("LastUserQuery", newQuery);
        x.commit();
    }

    //Using this so that the activity gets finished when user tries to go into the background
    //thereby preventing issue of wear query not working from backgrounded activity.
//    @Override
//    protected void onUserLeaveHint(){
//        finish();
//    }

	/**
     * this method is used to change the color of the seek bar
     *
     * @param seakBar
     *            the seekbar whose color has to be changed
     * @param newColor
     *            the color which has to be changed
     */
    public void setProgressBarColor(SeekBar seakBar, int newColor) {
        LayerDrawable ld = (LayerDrawable) seakBar.getProgressDrawable();
        ClipDrawable d1 = (ClipDrawable) ld
                .findDrawableByLayerId(R.id.progressshape);
        d1.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);
 
    }
    
    @Override
    public void onBackPressed(){
    	
    	btn_mukoPlayListClose.performClick();

    }

    private boolean startedfromMukoPlayListActivity = true;
    //connect to the service
  	private ServiceConnection musicConnection = new ServiceConnection(){

  		@Override
  		public void onServiceConnected(ComponentName name, IBinder service) {
  			Log.wtf(TAG, "Service Connected");
  			MusicBinder binder = (MusicBinder)service;
  			//get service
  			musicSrv = binder.getService();
  			//pass list
  			if(musicSrv.getSongList()!=null)
            {
                trackls = musicSrv.getSongList();
                
                int size = trackls.size();
                MukoPlayListAdapter songadptr =new MukoPlayListAdapter(MukoPlayListActivity.this,trackls,songsThumbInfoJsonObject,startedfromMukoPlayListActivity);
                SongList.setAdapter(songadptr);
                musicSrv.setList(trackls);
            }

            musicSrv.startedfromPlaylistWear(false);
  			//Getting playposition from service
  			playposition=musicSrv.returnsongpos();
  			
  	        if(speechQuery!=null){
  	        	rdio = musicSrv.returnRDIOObject();
                if(rdio==null){
                    Intent intent = new Intent(MukoPlayListActivity.this, SplashScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                else {
                    musicSrv.stopPlayerCompletely();
                    UserQuery = speechQuery;
                    speechQuery = null;
                    progressDialog.show();
                    new HttpAsyncTask().execute(url);
                }
  	        }
  			
  			//musicSrv.setList(trackls);

  			
  			//Stopping foreground status of the service
//  			musicSrv.stopForeground();
  			musicBound = true;
  		}

  		@Override
  		public void onServiceDisconnected(ComponentName name) {

            musicBound = false;
            if(!musicSrv.isPng() && rdio!=null){
                //rdio=null;
                Log.i(TAG,"Stopping Service from on Service Disconnected");
//                stopService(playIntent);
            }
  		}
  	};


  	//start and bind the service when the activity starts
//  	@Override
//  	protected void onStart() {
//  		super.onStart();
//  		if(playIntent==null){
//  			Log.wtf(TAG, "playIntent == null");
//  			
//  		}
//  	}

    public void updtlikeunlikeonwear(String songNametoUpdt,String artsisttoUpdt,int newValue, int SongListPosition){
        musicSrv.updtwearwithlikeunlikeinfo(songNametoUpdt,artsisttoUpdt,newValue,SongListPosition);
    }
  	
	public void doSomething(String[] songName_stringarray,String[] artistName_stringarray,String[] genre_stringarray){
        //set the boolean firsttimeload to true so that adapter creation in
        //rdio search api call will initialize and setup the list only once
        Log.v("DOSOMETHING FUNCTION","CALLED");
        firsttimeload = true;
		trackls = new ArrayList<TracksModel>();
		
		songname = songName_stringarray;
		artistname = artistName_stringarray;
		
		String[] tracks = new String[songname.length];

        //This is for populating the listview correctly
        k=-1;
        if(!trackls.isEmpty())
          trackls.clear();
        lastqueryelementpos = songname.length - 1;
		for (int i=0; i<songname.length; i++) {
			
            List<NameValuePair> args = new LinkedList<NameValuePair>();
            args.add(new BasicNameValuePair("query", songname[i]+','+artistname[i] ));
            args.add(new BasicNameValuePair("types", "Track"));
            args.add(new BasicNameValuePair("never_or", "false"));
            args.add(new BasicNameValuePair("count", "1"));
            rdio.apiCall("search", args, new RdioApiCallback() {
                

				@Override
                public void onApiFailure(String methodName, Exception e) {
                    Log.e(TAG, methodName + " failed: ", e);
                    key  = "NOT_FOUND";
                    k++;
                    if(k==lastqueryelementpos){
                    	musicSrv.setSong(0);
                    	musicSrv.next(true);
                    }
                }

                @Override
                public void onApiSuccess(JSONObject result) {

                    Log.i("API Success", "JSON Object Obtained");
                    try {
                    	//Position of k being the top most value to be incrememnted in critical
                    	//since exception is expected to happen from JSON
                        k++;

                        //TODO Service Binding doesnt happen immediately so one solution is set a handler and delay
                        //TODO launching of below code which is not a good solution
                        //TODO Leaving it alone is also fine as the Service exception happens only
                        //TODO after

                        //Test if screen On and bind service if not
//                        if(!pm.isScreenOn())
//                            serviceBinder();

                        JSONObject resultObtained;
                        JSONArray resultSobject;
                        resultObtained = new JSONObject();
                        resultSobject = new JSONArray();
                        resultObtained = result.getJSONObject("result");
                        resultSobject = resultObtained.getJSONArray("results");
                        //If the song is not present Exception will be happen when getting JSONObject
                        //skipping everything and moving onto next thread.
                        JSONObject getresult = resultSobject.getJSONObject(0);
                        key = getresult.getString("key");
                        songName = songname[k];
                        artistName = artistname[k];

                        Log.d("Got Key",key);

                        isTrueorFalse = 2;
                        if (songsThumbInfoJsonObject.has(songName + ":" + artistName)) {
                            Log.v("Entered Like Unlike Info Check","before try block");
                               try {
                                   boolean like_dislike_bool = songsThumbInfoJsonObject.getBoolean(songName+":"+artistName);
                                   if (like_dislike_bool)
                                       isTrueorFalse = 1;
                                   else
                                       isTrueorFalse = 0;
                               }
                               catch(JSONException e){
                                   Log.e("Caught Exception"," Bool value for song like/dislike not present");
                               }
                        }


                        trackls.add(new TracksModel(key, songName, artistName,isTrueorFalse/*, album, albumArt*/));

                        //Point of Interest
                        if(firsttimeload)
                        {
                            if(!progressDialog.isShowing())//When sending multiple queries we need the progressdialog to show
                                progressDialog.show();

                        	if(musicSrv.isPng()){
                        		musicSrv.stopPlayerCompletely();
                        	}
                            firsttimeload = false;
                            songadptr = new MukoPlayListAdapter(MukoPlayListActivity.this, trackls,songsThumbInfoJsonObject,startedfromMukoPlayListActivity );
                            SongList.setAdapter(songadptr);
                            musicSrv.setList(trackls);
//                            musicSrv.setSong(0);
//                            musicSrv.next(true);
                        }
                        else {
                            musicSrv.setList(trackls);
                            songadptr.updtSongList(trackls);
                            songadptr.notifyDataSetChanged();
                        }
                        //This wont get called when the last song is not present in rdio, so
                        //set this check in catch statement
                        if(k==lastqueryelementpos){
                            if(!musicSrv.isRdioSet()){
                                Log.e("Passing RDIO","Rdio Passing");
                                musicSrv.passRDIOObject(rdio);
                            }

                        	musicSrv.setSong(0);
                            musicSrv.foregroundstartersetter(true);
                        	//since the rdio object will be doing api call using threads
                        	//thread queue will be the size of lastqueryelementpos and the
                        	//first to finish among this thread will call the mediaplayer thread in next()
                        	//inside MusicService which will wait until all other threads are done to
                        	//start playback of songs.
                        	musicSrv.next(true);
                        }

//                        if(!pm.isScreenOn()) {
//                            serviceUnBinder();
//                        }
                    }
                    catch(JSONException e){
                    	//If rdio doesnt have the song this exception will be called
                        Log.e("Excepted Exception", e.toString());
                        if(k==lastqueryelementpos){
                            if(!musicSrv.isRdioSet()){
                                Log.e("Passing RDIO","Rdio Passing");
                                musicSrv.passRDIOObject(rdio);
                            }
                            musicSrv.foregroundstartersetter(true);
                        	//This is to start playback even if the last song
                        	//is not present in rdio
                            Log.e("Checking",musicSrv.toString());
                        	musicSrv.setSong(0);
                        	musicSrv.next(true);
                        }

//                        if(!pm.isScreenOn()) {
//                            serviceUnBinder();
//                        }
                    }
                    catch(Exception e){
                        Log.e("Unexpected Exception in onApiResult",e.toString());
                    }
                }
            });
		}

        Log.v("DOSOMETHING FUNCTION","END REACHED");
	}

    /**
     * Binds to service MusicService
     */
    public void serviceBinder(){
        if(playIntent == null)
            playIntent = new Intent(this, MusicService.class);
        bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
        startService(playIntent);
    }

    /**
     * Sets MusicService to foreground, unbinds it from MainActivity and finishes MainActivity
     * after 10 seconds if param is set
     *
     **/
    public void serviceUnBinder(){
        if(musicSrv!=null && musicSrv.isPng())
        {
            musicSrv.startForeground();

        }
        unbindService(musicConnection);
    }

	
	private void playPause() {
		if (musicSrv!=null) {
			if (musicSrv.isPng()) {
				musicSrv.pausePlayer();
				updatePlayPause(false);
			} else {
				musicSrv.go();
				updatePlayPause(true);
			}
		}
//        else {
//			musicSrv.nextSong();
//			musicSrv.next(true);
//		}
	}

	private void updatePlayPause(boolean playing) {
		if (playing) {
			playPause.setImageResource(R.drawable.pause_icon);
		} else {
			playPause.setImageResource(R.drawable.play_icon);
		}
	}
	
    public String getDuration(String _currentTimemilliSecond)
    {
        long _currentTimeMiles = 1;         
        int x = 0;
        int seconds = 0;
        int minutes = 0;
        int hours = 0;
        int days = 0;
        int month = 0;
        int year = 0;

        try 
        {
            _currentTimeMiles = Long.parseLong(_currentTimemilliSecond);
            /**  x in seconds **/   
            x = (int) (_currentTimeMiles / 1000) ; 
            seconds = x ;

            if(seconds >59)
            {
                minutes = seconds/60 ;

                if(minutes > 59)
                {
                    hours = minutes/60;

                    if(hours > 23)
                    {
                        days = hours/24 ;

                        if(days > 30)
                        {
                            month = days/30;

                            if(month > 11)
                            {
                                year = month/12;
                                return "Year "+year + " Month "+month%12 +" Days " +days%30 +" hours "+hours%24 +" Minutes "+minutes %60+" Seconds "+seconds%60;
                            }
                            else
                            {
                                return "Month "+month +" Days " +days%30 +" hours "+hours%24 +" Minutes "+minutes %60+" Seconds "+seconds%60;
                            }

                        }
                        else
                        {
                            return "Days " +days +" hours "+hours%24 +" Minutes "+minutes %60+" Seconds "+seconds%60;
                        }

                    }
                    else
                    {
                        return "hours "+hours+" Minutes "+minutes %60+" Seconds "+seconds%60;
                    }
                }
                else
                {
                	String sec;
                	String min;
                    String hrs;
                    
					if(seconds%60<10)  sec="0"+seconds%60;
                    else            sec= ""+seconds%60;
                    
					if(minutes<10)  min="0"+minutes;
                    else            min= ""+minutes;
					
					if(hours<10)    hrs="0"+hours;
                    else            hrs= ""+hours;
                    //return "Minutes "+minutes +" Seconds "+seconds%60;
                	return min+":"+sec;
                }
            }
            else
            {
            	String sec;
            	if(seconds<10)  sec="0"+seconds;
                else            sec= ""+seconds;
                //return " Seconds "+seconds;
                return "00:"+sec;
            }
        }
        catch (Exception e) 
        {
            Log.e(TAG, e.toString());
        }
        return "";
    }
    
    @Override
    public void onPause(){
    	super.onPause();
    }
    
    @Override
    public void onResume(){
    	super.onResume();
        Log.v("ONRESUME","is called");
    }

	
	@Override
	public void onDestroy() {
		Log.i(TAG, "OnDestroy called");

        startServiceIntentforLikeDislike();
        startServiceIntentforNotifyingWearable();

//        musicSrv.dismissNotification();
        musicSrv.stopForeground();

        RecieverDeactivator();

        unbindService(musicConnection);

		musicSrv.stopPlayerCompletely();
		musicSrv.setList(null);

		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				Log.v(TAG, "Login success");
				if (data != null) {
                    disablenetcheck = false;
					accessToken = data.getStringExtra("token");
					accessTokenSecret = data.getStringExtra("tokenSecret");
					onRdioAuthorised(accessToken, accessTokenSecret);
					rdio.setTokenAndSecret(accessToken, accessTokenSecret);
				}
			} else if (resultCode == RESULT_CANCELED) {
				if (data != null) {
					String errorCode = data.getStringExtra(OAuth1WebViewActivity.EXTRA_ERROR_CODE);
					String errorDescription = data.getStringExtra(OAuth1WebViewActivity.EXTRA_ERROR_DESCRIPTION);
					Log.v(TAG, "ERROR: " + errorCode + " - " + errorDescription);

                    //TODO Below code not put to the test, Do that or tell Teena
                    alertDialogBuilder.setTitle("Login Cancelled");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Your login has failed or been canceled " +
                                    "Please try again later. If you were not able to see the " +
                                    "login page please check your network connectivity")
                            .setCancelable(false)
                            .setPositiveButton("Quit",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, close
                                    // current activity
                                    networkcheck = 0;
                                    finish();
                                }
                            })
						.setNegativeButton("Retry",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// if this button is clicked, just close
								// the dialog box and restart activity
								networkcheck=0;
								dialog.cancel();
                                recreate();
							}
						});

                    // create alert dialog
                    alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
				}
				accessToken = null;
				accessTokenSecret = null;
			}
			rdio.prepareForPlayback();
		}
	}
	
	/*************************
	 * RdioListener Interface
	 *************************/

	/*
	 * Dispatched by the Rdio object when the Rdio object is done initializing, and a connection
	 * to the Rdio app service has been established.  If authorized is true, then we reused our
	 * existing OAuth credentials, and the API is ready for use.
	 * @see com.rdio.android.api.RdioListener#onRdioReady()
	 */
	@Override
	public void onRdioReadyForPlayback() {
		Log.i(TAG, "Rdio SDK is ready for playback");
		rdioready = true;
		if(wearQuery!=null){
            UserQuery = wearQuery;
            new HttpAsyncTask().execute(url);
        }
        //TODO Save below info into shared pref and set it work only once when the user is logging in for first time
		//Fetching User Information
        rdio.apiCall("currentUser", null, new RdioApiCallback() {

			@Override
            public void onApiFailure(String methodName, Exception e) {
                Log.e("API FAILURE","Failed to fetch User Information");
                }

            @Override
            public void onApiSuccess(JSONObject result) {

                Log.i("API Success", "JSON Object Obtained");
                try {

                    JSONObject resultObtained;
                    resultObtained = new JSONObject();
                    resultObtained = result.getJSONObject("result");
                    USERID = resultObtained.getString("key");
                    Log.e("USERID",USERID);
                }
                catch(Exception e){
                	Log.e("UserID Call",e.toString());
                    }
                }
        });

        //TODO When screen is off Service will not connect so rdio later on will be null in service
        //Passsing rdio
        if(musicSrv!=null){
            musicSrv.passRDIOObject(rdio);
        }
	}

	@Override
	public void onRdioUserPlayingElsewhere() {
		Log.w(TAG, "Tell the user that playback is stopping.");
	}

	/*
	 * Dispatched by the Rdio object once the setTokenAndSecret call has finished, and the credentials are
	 * ready to be used to make API calls.  The token & token secret are passed in so that you can
	 * save/cache them for future re-use.
	 * @see com.rdio.android.api.RdioListener#onRdioAuthorised(java.lang.String, java.lang.String)
	 */
	@Override
	public void onRdioAuthorised(String accessToken, String accessTokenSecret) {
		Log.i(TAG, "Application authorised, saving access token & secret.");
		Log.d(TAG, "Access token: " + accessToken);
		Log.d(TAG, "Access token secret: " + accessTokenSecret);

		SharedPreferences settings = getPreferences(MODE_PRIVATE);
		Editor editor = settings.edit();
		editor.putString(PREF_ACCESSTOKEN, accessToken);
		editor.putString(PREF_ACCESSTOKENSECRET, accessTokenSecret);
		editor.commit();
	}



    
    //SpeechKit Integeration..................................................................................................
    //........................................................................................................................

    
	/**
	 * This method executes asynchronously i.e in background.
	 * onPostExecute it returns the response from the server.
	 * 
	 * @params Url
	 * @returns response
	 */
	class HttpAsyncTask extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
        	return POST(urls[0]);
        }
        
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result)
        {
            if(result.equalsIgnoreCase("$%empty%$")){
                Toast.makeText(MukoPlayListActivity.this,"Songs not found, please try another query",Toast.LENGTH_LONG).show();
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
                playPause();
                return;
            }

            JSONArray resultArray;
            try
            {
            	resultArray = new JSONArray(result);
            	int size = resultArray.length();
            	
            	// declare a string array with initial size 
            	String[] songName_stringarray = new String[resultArray.length()];
                String[] artistName_stringarray = new String[resultArray.length()];
                String[] genre_stringarray = new String[resultArray.length()];
                for (int i = 0; i < resultArray.length(); i++)
                {
                	JSONObject mJsonObject = resultArray.getJSONObject(i);
                    songName_stringarray[i] = mJsonObject.getString("songName");
                    artistName_stringarray[i] = mJsonObject.getString("artistName");
                    genre_stringarray[i] = mJsonObject.getString("genre");
                }
                Log.e("FETCHING FROM RDIO", songName_stringarray.toString()+"SONG NAMES ^\n ARTIST NAMES v"+artistName_stringarray);
                doSomething(songName_stringarray, artistName_stringarray, genre_stringarray);
            }
            catch (JSONException e){
                Toast.makeText(MukoPlayListActivity.this,"Songs not found, please try another query",Toast.LENGTH_LONG).show();
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
                playPause();
                e.printStackTrace();
            }
        }
    }
    
	/**
	 * POST method accepts URL and data which needs to sent to the Muco Server.
	 * This method communicates with the server and fetches the result.
	 * 
	 * @params Url
	 * @returns response result
	 */
	
    public static String POST(String url)
    {
        InputStream inputStream = null;
        String result = "";
        try {
        	
            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
 
            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);
 
            String json = "";
            
            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();
            JSONArray mJSONArray_artist = new JSONArray();
            JSONArray mJSONArray_genre = new JSONArray();
            for(int i=0;i<artistlist.size();i++)
			{
            	mJSONArray_artist.put(artistlist.get(i));
			}
            for(int i=0;i<genrelist.size();i++)
			{
            	mJSONArray_genre.put(genrelist.get(i));
			}

            jsonObject.accumulate("userQuery", UserQuery);
            jsonObject.accumulate("artistList", mJSONArray_artist);
            jsonObject.accumulate("genreList", mJSONArray_genre);
            
 
            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();
            
            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);
 
            // 6. set httpPost Entity
            httpPost.setEntity(se);
 
            // 7. Set some headers to inform server about the type of the content   
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
 
            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);
            
            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
            
            // 10. convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
            
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        if(result.equalsIgnoreCase("[]")){
            result = "$%empty%$";
        }

        // 11. return result
        return result;
    }
    
    
    /**
	 * This method takes the inputstream(Response from the server) and decodes it to get a string which is readable
	 * 
	 * @params inputStream
	 * @returns response String
	 */
	 private static String convertInputStreamToString(InputStream inputStream) throws IOException
	 {
	        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	        String line = "";
	        String result = "";
	        while((line = bufferedReader.readLine()) != null)
	            result += line;
	 
	        inputStream.close();
	        return result;
	 }
    
    public void getselectedObject(JSONObject parse_like_dislike_object)
    {
    	JSONObject test = new JSONObject();
    	JSONObject test1 = new JSONObject();
    	like_dislike_JsonObject = parse_like_dislike_object;
    	test = like_dislike_JsonObject;
    	test1 = test;
    }


    //-------------------------------Handling Broadcast-----------------------------------------

    public static final String BROADCAST_ACTION = "com.neurlabs.muko.UPDATESONGLIST";
    public static final String SERVICE_BROADCAST = "com.neurlabs.muko.MSGFRMSRV";
    public static final String LIKE_UNLIKE_INFO_BROADCAST = "com.neurlabs.muko.LIKEUNLIKEINFOUPDT";
    public static final String HIGHLIGHTSONG_BROADCAST = "com.neurlabs.muko.HIGHLIGHTSNG";

    //Recieving Broadcast
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rdio = musicSrv.returnRDIOObject();
            musicSrv.stopPlayerCompletely();
            wearQuery = intent.getStringExtra("UserQueryFromWear");
            UserQuery = wearQuery;
            QueryDisplay.setText(UserQuery);
            saveLastUserQuery(UserQuery);
            progressDialog.show();
            new HttpAsyncTask().execute(url);
        }
    };

    /**
     * To Un register Reciever
     */
    public void RecieverDeactivator(){
        Log.e("Reciever Deactivator","called");
        unregisterReceiver(receiver);

        //Local Broadcast for communication with Service
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLikeUnlikeInfoReceiver);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(timeToHighlightcrntSong);
    }

    /**
     * To activate Reciever
     *
     */
    public void RecieverActivator(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION);
        registerReceiver(receiver, filter);

        //Local Broadcast for communication with Service
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(SERVICE_BROADCAST));

        LocalBroadcastManager.getInstance(this).registerReceiver(mLikeUnlikeInfoReceiver,
                new IntentFilter(LIKE_UNLIKE_INFO_BROADCAST));

        LocalBroadcastManager.getInstance(this).registerReceiver(timeToHighlightcrntSong,
                new IntentFilter(HIGHLIGHTSONG_BROADCAST));
    }

    //------------------Experimenting with LocalBroadcastManager for Service----------------------

    //TODO Try and adopt LocalBroadcastManager for improved security and closing loopholes and interferences -Sebi

    //This is a normal Broadcast Reciever the LocalBroadcastManager is used to register and unregister
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            progressDialog.show();
        }
    };

    /**
     * This Method will check for keypad is present or not
     * If keyboard is present will make hide
     */
    public void keyboardCheck(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    QueryDisplay.setCursorVisible(false);
                    keyboard.hideSoftKeyboard(MukoPlayListActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                keyboardCheck(innerView);
            }
        }
    }

    /**
     * Shows Alert Dialog. Accept string parameter
     * */
    @SuppressWarnings("deprecation")
    public void alert(String message)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(MukoPlayListActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("MUKO");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //TODO Close the app
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
    //--------------------------------Handling Wear Like/Dislike-----------------------------------------
    public void updateLikeUnlikeJsonFromWear(String songname, String artistname, boolean status)
    {
        String data = songname+":"+artistname;
        try {
            like_dislike_JsonObject.put(data, status);
            JSONObject test = new JSONObject();
            JSONObject test1 = new JSONObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        songadptr.updateJSONforLikeUnlike(like_dislike_JsonObject);
    }

    //This is a normal Broadcast Reciever the LocalBroadcastManager is used to register and unregister
    private BroadcastReceiver mLikeUnlikeInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String songName = intent.getStringExtra("songNameforLikeUnlike");
            String artistName = intent.getStringExtra("artistNameforLikeUnlike");
            boolean likedorunliked = intent.getBooleanExtra("likedorunliked", false);
            updateLikeUnlikeJsonFromWear(songName,artistName,likedorunliked);
        }
    };


//-------------------------------------- Highlighting of Songs------------------------------------


    /**
     * Function designed to highlight the current playing song
     */
    public void songHighlighter(){
        songadptr.layoutBackgroundChange(musicSrv.currentSongId());
    }

    private BroadcastReceiver timeToHighlightcrntSong = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            songHighlighter();
        }
    };

}




