package com.neurlabs.muko.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.neurlabs.muko.R;
import com.neurlabs.muko.SplashScreenActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ListenerServiceFromWear extends WearableListenerService {

    private static final String HELLO_WORLD_WEAR_PATH = "/hello-world-wear";
    private static final String SONG_CONTROL_WEAR_PATH = "/song-control-wear";
    String myPackage= "com.neurlabs.muko";
    public static final String BROADCAST_ACTION = "com.neurlabs.muko.UPDATESONGLIST";
    public static final String BROADCAST_ACTION_SERVICE = "com.neurlabs.muko.CONTROLSONGLIST";
    String query;
    String songcontrol;
    private String TAG = "ListenerServiceFromWear";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        /*
         * Receive the message from wear
         */
        if (messageEvent.getPath().equals(HELLO_WORLD_WEAR_PATH)) {

            Log.i("UPDATESONGLIST","CONTROLSONGLIST");

            query = new String( messageEvent.getData() );
            Toast.makeText(this, query, Toast.LENGTH_LONG).show();
            if(isForeground()){
                Log.i("WEAR MESSAGE","ACTIVITY IS FOREGROUND");
//                Toast.makeText(this, "Detected App in Foreground", Toast.LENGTH_LONG).show();
                sendBroadcast();
            }
            else {
                Log.i("WEAR MESSAGE","ACTIVITY IS BEING STARTED");
//                Toast.makeText(this, query, Toast.LENGTH_LONG).show();
                Intent startIntent = new Intent(this, SplashScreenActivity.class);
                startIntent.putExtra("Query", query);
                startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startIntent);
            }

        }
        else if(messageEvent.getPath().equals(SONG_CONTROL_WEAR_PATH)){
            songcontrol=new String(messageEvent.getData());
            sendBroadcasttoService();
        }

    }

    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEvents);
        dataEvents.close();

        if (!mGoogleApiClient.isConnected()) {
            ConnectionResult connectionResult = mGoogleApiClient
                    .blockingConnect(30, TimeUnit.SECONDS);
            if (!connectionResult.isSuccess()) {
                Log.e(TAG, "Service failed to connect to GoogleApiClient.");
                return;
            }
        }

        for (DataEvent event : events) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                String path = event.getDataItem().getUri().getPath();
                if ("/updtLikeUnlikeJSON".equals(path)) {

                    DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
                    final String songtitle = dataMapItem.getDataMap().getString("mSongName");
                    final String songartist = dataMapItem.getDataMap().getString("mArtistName");
                    final int LikeUnlikeInfo = dataMapItem.getDataMap().getInt("mlikeOrUnlike");

                    boolean likeorunliked = false;

                    switch(LikeUnlikeInfo){
                        case 0:
                            likeorunliked = false;
                            break;
                        case 1:
                            likeorunliked = true;
                            break;
                        default:
                            Log.e("Error","Unknown like info passed setting song to disliked by default");
                    }

                    sendMessageAboutLikeUnlikeInfo(songtitle,songartist,likeorunliked);
                }
                else {
                    Log.d(TAG, "Unrecognized path: " + path);
                }
            }
        }
    }

    /**
     * Checking whether an Activity is in Foreground/Runnning
     */

    public boolean isForeground(){
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List< ActivityManager.RunningTaskInfo > runningTaskInfo = manager.getRunningTasks(1);

        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        if(componentInfo.getPackageName().equals(myPackage)){

            return true;
        }
        return false;
    }

    public static String LIKE_UNLIKE_INFO_BROADCAST = "com.neurlabs.muko.LIKEUNLIKEINFOUPDT";

    private void sendMessageAboutLikeUnlikeInfo(String songName,String artistName, boolean likedorunliked) {
        Intent intent = new Intent(LIKE_UNLIKE_INFO_BROADCAST);
        intent.putExtra("songNameforLikeUnlike", songName);
        intent.putExtra("artistNameforLikeUnlike", artistName);
        intent.putExtra("likedorunliked",likedorunliked);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Send Broadcast to Activity
     */
    public void sendBroadcast(){
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_ACTION);
        broadcast.putExtra("UserQueryFromWear",query);
        sendBroadcast(broadcast);
    }

    /**
     * Sending Broadcast to Service
     */

    public void sendBroadcasttoService(){
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_ACTION_SERVICE);
        broadcast.putExtra("SongControlFromWear",songcontrol);
        sendBroadcast(broadcast);
    }

}