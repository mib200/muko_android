package com.neurlabs.muko.services;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Random;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.neurlabs.muko.MucoPlayListActivityforWear;
import com.neurlabs.muko.MukoPlayListActivity;
import com.neurlabs.muko.R;
import com.neurlabs.muko.SplashScreenActivity;
import com.neurlabs.muko.models.TracksModel;
import com.rdio.android.api.Rdio;
import com.rdio.android.api.RdioListener;

import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat.WearableExtender;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

/**
 * Music Service Class which handles music playback
 */

public class MusicService extends Service implements MediaPlayer.OnErrorListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener  {

	//media player
	private MediaPlayer player;
	//song list
	private ArrayList<TracksModel> songs = null;
	//current position
	private static int songPosn = 0;
	//binder
	private final IBinder musicBind = new MusicBinder();
	//title of current song
	private String songTitle="";
	//notification id
	private static final int NOTIFY_ID=1;
	//shuffle flag and random
	private boolean shuffle=false;
	private Random rand;
	private String TAG = "Okum Music Service";
	
	private static final String appKey = "kcdb9ukqh5y2acahf39au6b3";
	private static final String appSecret = "5kNJ23JCJA";
	
	private static final String PREF_ACCESSTOKEN = "prefs.accesstoken";
	private static final String PREF_ACCESSTOKENSECRET = "prefs.accesstokensecret";
	private static String accessToken = null;
	private static String accessTokenSecret = null;	
	private static Rdio rdio = null;
	String songname = "Song";
	String artistname = "Artist";
    int LikeUnlikeInfo = 2;
	private SwitchButtonListener switchButtonListener;
	private NextButtonListener nextButtonListener;
	RemoteViews notificationView;
	NotificationManager notificationManager;
	Notification not;
	boolean playerinitializedandstarted = false;
	AsyncTask<TracksModel, Void, TracksModel> task;
    AudioManager audio;

    private String wearSongCtrl;
    public static String BROADCAST_ACTION_SERVICE = "com.neurlabs.muko.CONTROLSONGLIST";
    private Boolean foregroundstarter = false;
    private boolean strtdfrmplylistwear = false;
    private String songID = "t13585611";//Familiar Feeling - Moloko
    private boolean serviceInForeground = false;


    public void onCreate(){
		//create the service
		super.onCreate();
		Log.wtf(TAG, "Service Created");
		//Initialize rdio
		SharedPreferences settings = getSharedPreferences("Okum", MODE_PRIVATE); //getPreferences(MODE_PRIVATE); 
		accessToken = settings.getString(PREF_ACCESSTOKEN, null);
		accessTokenSecret = settings.getString(PREF_ACCESSTOKENSECRET, null);
		songPosn=0;
		//random
		rand=new Random();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION_SERVICE);
        registerReceiver(receiver, filter);

        //Registering the reciver here ensures it gets called only once
        //because even though there was a lot of handling done the edittext query in
        //both the PlayList Activity would call the startForeground() again before
        //stopForeground() has been called
        notificationReciverRegister();

        int status = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getApplicationContext());
        if (status == ConnectionResult.SUCCESS) {
//            Toast.makeText(this,"Compatible Google Play Service Available",Toast.LENGTH_LONG).show();
            Log.v("MainActivity","Compatible Google Play Service's found");
        }
        else
            Toast.makeText(this,"Google Play Services Outdated, Please Update",Toast.LENGTH_LONG).show();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }

        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

	}
	
	public Integer returnsongpos(){
		return songPosn;
	}
	
	public void passRDIOObject(Rdio rdio){
		//Log.e("", msg);
		this.rdio = rdio;
		String test1 = rdio.toString();
		String test2 = test1;
		test2 = rdio.toString();
	}

    /**
     * Checking whether rdio is null
     *
     * @return true if rdio is not null
     */
	public boolean isRdioSet(){
		return (rdio == null)?false:true;
	}
	
	public Rdio returnRDIOObject()
	{
		return rdio;
	}

	public void initMusicPlayer(){
		//set player properties
		player.setWakeMode(getApplicationContext(), 
				PowerManager.PARTIAL_WAKE_LOCK);
		player.setAudioStreamType(AudioManager.STREAM_MUSIC);
		//set listeners
		//player.setOnPreparedListener(this);
		//player.setOnCompletionListener(this);
		player.setOnErrorListener(this);
	}

	//pass song list
	public void setList(ArrayList<TracksModel> theSongs){
		songs=theSongs;
	}

    public ArrayList<TracksModel> getSongList(){
        return songs;
    }

	//binder
	public class MusicBinder extends Binder {
		public MusicService getService() { 
			return MusicService.this;
		}
	}

	//activity will bind to service
	@Override
	public IBinder onBind(Intent intent) {
		return musicBind;
	}

	//release resources when unbind
	@Override
	public boolean onUnbind(Intent intent){
//		player.stop();
//		player.release();
		return false;
	}

    /**
     * This function will set value of pssdvalue to true if service is started from
     * MucoPlayListActivityforWear and false if started from MukoPlayListActivity
     * @param pssdvalue
     */
    public void startedfromPlaylistWear(Boolean pssdvalue){
        strtdfrmplylistwear = pssdvalue;
    }

    public Boolean checkstartedfromPlaylistWear(){
        return strtdfrmplylistwear;
    }
	
	public void startForeground() {


        Intent notIntent = new Intent(this, MukoPlayListActivity.class);

        if(strtdfrmplylistwear){
            notIntent = new Intent(this, MucoPlayListActivityforWear.class);
        }

//        notIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);


		PendingIntent pendInt = PendingIntent.getActivity(this, 0,
				notIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
	    NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
	    //Setting up buttons
	    notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	    notificationView = new RemoteViews(getPackageName(), R.layout.notification_buttons);
	    //this is the intent that is supposed to be called when the button is clicked
	    Intent switchIntent = new Intent("SWITCH_EVENT");
	    PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(this, 0, switchIntent, 0);
	    notificationView.setOnClickPendingIntent(R.id.notification_play_button, pendingSwitchIntent);
	    
	    Intent nextIntent = new Intent("NEXT_EVENT");
	    PendingIntent pendingNextIntent = PendingIntent.getBroadcast(this, 0, nextIntent, 0);
	    notificationView.setOnClickPendingIntent(R.id.notification_next_button, pendingNextIntent);
	    
	    notificationView.setTextViewText(R.id.Notification_song_name, songTitle);
	    notificationView.setTextViewText(R.id.Notification_artist_name, artistname);
	    
		builder.setContentIntent(pendInt)
		.setSmallIcon(R.drawable.muko_app_icon)
		.setTicker(songTitle)
		.setOngoing(true)
		.setContentTitle("Playing")
		.setContentText(songTitle);
		not = builder.build();
		//Setting up buttons
		not.contentView = notificationView;

        serviceInForeground = true;
		startForeground(NOTIFY_ID, not);
	}

    public void notificationReciverRegister(){
        switchButtonListener = new SwitchButtonListener();
        registerReceiver(switchButtonListener, new IntentFilter("SWITCH_EVENT"));

        nextButtonListener = new NextButtonListener();
        registerReceiver(nextButtonListener, new IntentFilter("NEXT_EVENT"));
    }

    public void notificationReciverUnRegister(){
        try {
            if (switchButtonListener != null) {
                unregisterReceiver(switchButtonListener);
                //This is necessary to prevent an exception from
                //happening when user query comes from wear when
                // app is minimized or in foreground
                switchButtonListener = null;
            }
            if (nextButtonListener != null) {
                unregisterReceiver(nextButtonListener);
                //This is necessary to prevent an exception from
                //happening when user query comes from wear when
                // app is minimized or in foreground
                nextButtonListener = null;
            }
        }
        catch(Exception e){
            //Happens sometimes when user Query comes from wear
            Log.e("Catching Reciever Error if Any",e.toString());
        }
    }

    public void foregroundstartersetter(Boolean starter){
        foregroundstarter = starter;
    }
	
	public void stopForeground() {
        //Otherwise the condition in onPostexecute of AsyncTask object 'task' will execute
        notificationView=null;

        serviceInForeground = false;
	    stopForeground(true);
	}
	
	public class SwitchButtonListener extends BroadcastReceiver {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        Log.d(TAG, "Broadcast Reciever Detected");
	        if(player.isPlaying()){
	        	Log.d(TAG, "Pausing");
                pausePlayer();
	        }
	        else{
	        	Log.d(TAG, "Playing");
                go();
	        }
	    }

	}
	
	public class NextButtonListener extends BroadcastReceiver {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        Log.d(TAG, "Broadcast Reciever Detected");
	       setplayerinitializingbooltofalse();
	       nextSong();
           sendMessagetoActivity();
	       next(true);	       
	    }

	}



    /**
     * Set the song by position specified
     *
     * @param songIndex The position of the Song
     */

	public void setSong(int songIndex){
        Log.e("setSong", "called");
		songPosn=songIndex;	
	}

    /**
     * Increments value of the song position by 1
     */
    public void nextSong(){
        songPosn++;
    }

    /**
     * Decrements value of the song position by 1
     */
    public void prevSong(){
        songPosn--;
    }


	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		Log.v("MUSIC PLAYER", "Playback Error");
		mp.reset();
		return false;
	}


    /**
     *
     * @return true if player is not null and flase
     */
	public boolean isPlayerNull(){
		return player != null;
	}

    /**
     * @return Curresnt Position of the song
     */
	public int getPosn(){
		if(player!=null && playerinitializedandstarted == true)	
				return player.getCurrentPosition();
			return 0;
		}


    /**
     *
     * @return Current Playback Duration
     */
	public int getDur(){
		if(player!=null && playerinitializedandstarted == true)
		     return player.getDuration();
		return 0;
	}

    /**
     * Check if Player is Playing
     * @return True if player is playing and not null
     */
	public boolean isPng(){
		if(player!=null)
            try {
                return player.isPlaying();
            }
            catch(Exception e){
                Log.e("Weird Exception than can happen because of phone calls",e.toString());
                return false;
            }
		else
			return false;
	}

    /**
     * Pause Player
     */
	public void pausePlayer(){
        if(player!=null) {
            player.pause();
            sendPlayerStatus(false, false);
            if (notificationView != null) {
                notificationView.setImageViewResource(R.id.notification_play_button, R.drawable.play_update);
                notificationManager.notify(NOTIFY_ID, not);
            }
        }
	}

    /**
     *
     * Starts or Resumes the music player.
     */
	public void go(){
        if(player!=null) {
            player.start();
            sendPlayerStatus(true, false);
            if (notificationView != null) {
                notificationView.setImageViewResource(R.id.notification_play_button, R.drawable.pause_update);
                notificationManager.notify(NOTIFY_ID, not);
            }
        }
        else{
            if(songs!=null){
                sendMessagetoActivity();
                Toast.makeText(MusicService.this,"Resuming Highlighted Song",Toast.LENGTH_SHORT).show();
                next(true);
            }
        }
	}

    public void increaseVolume(){
        audio.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
    }

    public void decreaseVolume(){
        audio.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);
    }


	public void stopPlayerCompletely(){
		setplayerinitializingbooltofalse();
		if (player != null) {
			player.stop();
			player.release();
			player = null;
		}
        sendPlayerStatus(true,true);//The isPlaying variable is irrevelant whether true or false since songchng value is true
        if(notificationView!=null){
            notificationView.setTextViewText(R.id.Notification_song_name, "Changing Song");
            notificationView.setTextViewText(R.id.Notification_artist_name, "");
            notificationView.setViewVisibility(R.id.prgrssbarinnot, View.VISIBLE);
            notificationView.setViewVisibility(R.id.notification_next_button, View.GONE);
            notificationView.setViewVisibility(R.id.notification_play_button, View.GONE);
            notificationManager.notify(NOTIFY_ID, not);
        }
	}

    /**
     *The main function which handles the music streaming using the rdio object
     *
     * @param manualPlay Whether the song should be played automatically.
     */
	public void next(final boolean manualPlay) 
	{
        sendPlayerStatus(true,true);//The isPlaying variable is irrevelant whether true or false since songchng value is true

        if(notificationView!=null){
            notificationView.setTextViewText(R.id.Notification_song_name, "Changing Song");
            notificationView.setTextViewText(R.id.Notification_artist_name, "");
            notificationView.setViewVisibility(R.id.prgrssbarinnot, View.VISIBLE);
            notificationView.setViewVisibility(R.id.notification_next_button, View.GONE);
            notificationView.setViewVisibility(R.id.notification_play_button, View.GONE);
            notificationManager.notify(NOTIFY_ID, not);
        }

		if (player != null) {
			player.stop();
			player.release();
			player = null;
		}

        if(songs == null){
            return;
        }
        //Reset song position to 0 if end of track reached to repeat
		if (songPosn == songs.size()) {
			Log.i(TAG, "Tracks depleted playing first song");
			//LoadMoreTracks();//Instead we can call doSomething() to make the songs play in a loop
							   //Be careful when fetching trackid from server
			songPosn = 0;
		}
        if(songPosn<0){
            songPosn=songs.size()-1;
        }

        //Fetching song from ArrayList object
		final TracksModel track = songs.get(songPosn);//Top of the queue is retrieved(given to track) and removed 

        //To stop playback and exit function in case no more songs avaiable in the List
		if
		 (track == null) {
			Log.e(TAG, "Track is null!");
			return;
		}
		 songTitle=track.getSongName();
			

		// Load the next track in the background and prep the player (to start buffering)
		// Do this in a bkg thread so it doesn't block the main thread in .prepare()
		 task = new AsyncTask<TracksModel, Void, TracksModel>() {
			
			@Override
			protected TracksModel doInBackground(TracksModel... params) {
				TracksModel track = params[0];
				try {
					player = rdio.getPlayerForTrack(track.key, null, manualPlay);
					player.prepare();
					player.setOnCompletionListener(new OnCompletionListener() {
						@Override
						public void onCompletion(MediaPlayer mp) {
                            //To prevent exception in handler set value to false
							setplayerinitializingbooltofalse();
                            sendMessagetoActivity();
							mp.reset();
                            //Increment position to next Song position
							nextSong();
//                            Log.wtf("Song Position",Integer.toString(songPosn));
							next(false);							
						}
					});
					
					
					player.start();
				} catch (Exception e) {
					Log.e("MusicService", "Exception in Asynctask " + e);
				}
				return track;
			}


			@Override
			protected void onPostExecute(TracksModel track) {
                //To allow handler in MainActivity to update
				playerinitializedandstarted = true;

                tellActivityToHighlightCurrentlyPlayingSong();

                if(foregroundstarter){
                    if(!serviceInForeground)
                        startForeground();
                    foregroundstarter = false;
                }

                //Sending any connected wearables that player has started playing
                sendPlayerStatus(true,false);

				songname = track.getSongName();
				artistname = track.getArtistName();
                songID = track.getID();
                LikeUnlikeInfo = track.getLikeUnlikeInfo();

                //Alerting wearable to start its notification
                sendMessage(songname,artistname,LikeUnlikeInfo);

                //To update the Notification when new song is being played
				if(notificationView!=null){
				notificationView.setTextViewText(R.id.Notification_song_name, songTitle);
			    notificationView.setTextViewText(R.id.Notification_artist_name, artistname);
                    notificationView.setViewVisibility(R.id.prgrssbarinnot, View.GONE);
                    notificationView.setViewVisibility(R.id.notification_next_button, View.VISIBLE);
                    notificationView.setViewVisibility(R.id.notification_play_button, View.VISIBLE);
                    notificationView.setImageViewResource(R.id.notification_play_button,R.drawable.pause_update);
			    notificationManager.notify(NOTIFY_ID, not);
				}
			}
		};
		task.execute(track);

	}

    /**
     *
     * @return The Boolean Value of whether the Player has been Initialized
     */

	public boolean hasplayerbeenstartedefore()
	{
		return playerinitializedandstarted;
	}

    /**Thia function sets the boolean value to false whose only function is to prevent a Nullpointer
     * Exception in the handler which updates every 200 microsecond in the Main Activity
     *because values related to player will be null when transitioning from one song to another
     */
	public void setplayerinitializingbooltofalse(){
		playerinitializedandstarted = false;
	}

    /**
     *
     * @return The Current Song Name
     */
	public String currentSongName(){		
		return songname;
	}

    /**
     *
     * @return The current Artist Name
     */
	public String currentArtistName(){		
		return artistname;
	}

    public String currentSongId(){
        return songID;
    }
	
	public void stopServicefromitself(){
		stopSelf();
	}

	@Override
	public void onDestroy() {

        notificationReciverUnRegister();

        unregisterReceiver(receiver);
        //Null check to prevent crash if the service is being stopped before
        //the rdio object in Main Activity got initialized.
        if(rdio!=null){
            rdio.cleanup();
            rdio=null;
        }

        //Null check to prevent crash if the service is being stopped before
        //the player object in TracksModel<Asynctask> got initialized.
        if(player!=null) {
            player.stop();
            player.reset();
            player.release();
            player=null;
        }
//		stopForeground(true);
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

	}

    /**
     *
     * Code for Wearables
     *
     *
     *
     */


    /**
     * This function is used to send message to the wear when user updates like or unlike
     * and also this is used to update the trackls object of the MusicService class.
     * @param songNametoUpdt
     * @param artsisttoUpdt
     * @param newLikeInfoValue
     */
    public void updtwearwithlikeunlikeinfo(String songNametoUpdt,String artsisttoUpdt,int newLikeInfoValue, int SongListPosition){
        if((songname==songNametoUpdt) && (artistname==artsisttoUpdt))
            sendMessage(songname,artistname,newLikeInfoValue);

        TracksModel updttrack;
        updttrack = songs.get(SongListPosition);
        updttrack.setLikeUnlikeInfo(newLikeInfoValue);
        songs.remove(SongListPosition);
        songs.add(SongListPosition,updttrack);
    }

        private GoogleApiClient mGoogleApiClient;

    private void sendMessage(String songname,String artistname,int LikeUnlikeInfo) {
        if (mGoogleApiClient.isConnected()) {
            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create("/ongoingnotification");

            // Add data to the request
            putDataMapRequest.getDataMap().putString("title", songname);
            putDataMapRequest.getDataMap().putString("artist", artistname);
            putDataMapRequest.getDataMap().putInt("likeunlikeinfo", LikeUnlikeInfo);

            Log.e("LikeUnlikeInfo in Message Passing",Integer.toString(LikeUnlikeInfo));

            PutDataRequest request = putDataMapRequest.asPutDataRequest();

            Wearable.DataApi.putDataItem(mGoogleApiClient, request)
                    .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                        @Override
                        public void onResult(DataApi.DataItemResult dataItemResult) {
                            Log.d(TAG, "putDataItem status: " + dataItemResult.getStatus().toString());
                        }
                    });
        }
    }

    private void sendPlayerStatus(Boolean isPlaying,Boolean songchng) {
        if (mGoogleApiClient.isConnected()) {
            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create("/playerstatus");

                // Add data to the request
                putDataMapRequest.getDataMap().putBoolean("isPlaying", isPlaying);
                putDataMapRequest.getDataMap().putBoolean("sngcngd", songchng);

            PutDataRequest request = putDataMapRequest.asPutDataRequest();

            Wearable.DataApi.putDataItem(mGoogleApiClient, request)
                    .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                        @Override
                        public void onResult(DataApi.DataItemResult dataItemResult) {
                            Log.d(TAG, "putDataItem in sendPlayerStatus:" + dataItemResult.getStatus().toString());
                        }
                    });
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "Failed to connect to Google Api Client with error code "
                + connectionResult.getErrorCode());
    }


    //Broadcast Reciever
    //Recieving Broadcast
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            wearSongCtrl = intent.getStringExtra("SongControlFromWear");

            if(wearSongCtrl.equalsIgnoreCase("$%next%$")){
                setplayerinitializingbooltofalse();
                nextSong();
                sendMessagetoActivity();
                if(player!=null)
                    next(true);
            }
            else if(wearSongCtrl.equalsIgnoreCase("$%previous%$")){
                setplayerinitializingbooltofalse();
                prevSong();
                sendMessagetoActivity();
                if(player!=null)
                    next(true);
            }
            else if(wearSongCtrl.equalsIgnoreCase("$%playpause%$")){
                if(isPng()){
                    if(notificationView!=null) {
                        notificationView.setImageViewResource(R.id.notification_play_button, R.drawable.play_update);
                        notificationManager.notify(NOTIFY_ID, not);
                    }
                    if(player!=null)
                        pausePlayer();
                }

                else {
                    if(notificationView!=null) {
                        notificationView.setImageViewResource(R.id.notification_play_button, R.drawable.pause_update);
                        notificationManager.notify(NOTIFY_ID, not);
                    }
                    if(player!=null)
                        go();
                }
            }
            else if(wearSongCtrl.equalsIgnoreCase("$%volIncrease%$")){
                increaseVolume();
            }
            else if(wearSongCtrl.equalsIgnoreCase("$%volDecrease%$")){
                decreaseVolume();
            }
            else if(wearSongCtrl.equalsIgnoreCase("$%pause%$")){
                if(isPng()){
                    if(notificationView!=null) {
                        notificationView.setImageViewResource(R.id.notification_play_button, R.drawable.play_update);
                        notificationManager.notify(NOTIFY_ID, not);
                    }
                    if(player!=null)
                        pausePlayer();
                }
            }

        }
    };

    public static final String SERVICE_BROADCAST = "com.neurlabs.muko.MSGFRMSRV";

    /**
     * Sends message to Activity telling it to show progressbar
     */
    private void sendMessagetoActivity() {
        Intent intent = new Intent(SERVICE_BROADCAST);
        // add data
//        intent.putExtra("MSG", "Hiiiiiiiiiiiii");
//        sendBroadcast(intent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public static final String HIGHLIGHTSONG_BROADCAST = "com.neurlabs.muko.HIGHLIGHTSNG";
    /**
     * Sends message to Activity telling it to highlight the current playing song
     */
    private void tellActivityToHighlightCurrentlyPlayingSong() {
        Intent intent = new Intent(HIGHLIGHTSONG_BROADCAST);
        // add data
//        intent.putExtra("MSG", "Hiiiiiiiiiiiii");
//        sendBroadcast(intent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
