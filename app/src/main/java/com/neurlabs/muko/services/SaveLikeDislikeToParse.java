package com.neurlabs.muko.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Neurlabs on 1/16/2015.
 */
public class SaveLikeDislikeToParse extends IntentService {


    JSONObject like_dislike_JsonObject = new JSONObject();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public SaveLikeDislikeToParse() {
        super("SaveLikeDislikeIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.v("Inside Intent Service","starting up");

        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
        final String userKey = muko_pref.getString("userKey", null);
        final String objectid = muko_pref.getString("objectid", null);

        try {
            like_dislike_JsonObject = new JSONObject(intent.getStringExtra("thumbinfotoIntentService"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try
        {
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("MKRdioUser");
            query.whereEqualTo("objectId", objectid);
            List<ParseObject> list_businessinfo;
            list_businessinfo = query.find();
            for (ParseObject parseBusinessinfo : list_businessinfo)
            {
                parseBusinessinfo.put("songsThumbInfo", like_dislike_JsonObject);

                parseBusinessinfo.saveInBackground((new SaveCallback()
                {
                    @Override
                    public void done(ParseException e)
                    {
                        // Handle success or failure here ...
                        if(e==null)
                        {
                            //Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            //Toast.makeText(getApplicationContext(), "FAILURE", Toast.LENGTH_SHORT).show();
                        }
                    }
                }));
            }
        }
        catch (ParseException e)
        {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

    }
}
