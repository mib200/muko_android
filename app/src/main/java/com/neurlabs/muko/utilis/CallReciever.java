package com.neurlabs.muko.utilis;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/**
 * Created by Neurlabs on 1/12/2015.
 */

public class CallReciever extends BroadcastReceiver{
    TelephonyManager telManager;
    Context context;
    final String BROADCAST_ACTION_SERVICE = "com.rdiotest.sample.CONTROLSONGLIST";

    @Override
    public void onReceive(Context context, Intent intent) {


        this.context=context;

        telManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        telManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

    }

    public void sendBroadcasttoService(){
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_ACTION_SERVICE);
        broadcast.putExtra("SongControlFromWear","$%pause%$");
        context.sendBroadcast(broadcast);
    }

    private final PhoneStateListener phoneListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            try {
                switch (state) {
                    case TelephonyManager.CALL_STATE_RINGING: {
                        //PAUSE
                        sendBroadcasttoService();
                        break;
                    }
                    case TelephonyManager.CALL_STATE_OFFHOOK: {

                        break;
                    }
                    case TelephonyManager.CALL_STATE_IDLE: {
                        //PLAY
                        sendBroadcasttoService();
                        break;
                    }
                    default: { }
                }
            } catch (Exception ex) {

            }
        }
    };
}