package com.neurlabs.muko.utilis;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

public class CustomKeyboard
{
	public static void hideSoftKeyboard(Activity activity)
	{
	    InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}
	
	public void sample()
	{
		
	}
}
