package com.neurlabs.muko;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.neurlabs.muko.services.MusicService;
import com.neurlabs.muko.services.MusicService.MusicBinder;
import com.neurlabs.muko.utilis.InternetStatus;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.rdio.android.api.OAuth1WebViewActivity;
import com.rdio.android.api.Rdio;
import com.rdio.android.api.RdioApiCallback;
import com.rdio.android.api.RdioListener;

public class SplashScreenActivity extends ActionBarActivity implements RdioListener
{
	private static final String TAG = "Muko App";
	
	Button splash_screen_activity_layout_signup;
	Button splash_screen_activity_layout_signin;
	
	private static final String appKey = "kcdb9ukqh5y2acahf39au6b3";
	private static final String appSecret = "5kNJ23JCJA";
	
	private static String accessToken = null;
	private static String accessTokenSecret = null;
	
	private static final String PREF_ACCESSTOKEN = "prefs.accesstoken";
	private static final String PREF_ACCESSTOKENSECRET = "prefs.accesstokensecret";

    InternetStatus internetStatus = new InternetStatus();
    Boolean netStatus;

    public static Rdio rdio;
	boolean userLoggedIn;
	
	protected String USERID;
	String firstName;
	String lastName;
	SharedPreferences settings;
	SharedPreferences nav_check;
	SharedPreferences logout;
	SharedPreferences muko_pref;
    SharedPreferences skipperdude;
	ProgressDialog genreProgressDialog;
	
	private boolean userMovedtoActivity = false;
	JSONArray parseJsonChecked_status = new JSONArray();

	private boolean logoutchecker = false;

    private String wearQuery;
    private boolean donotstart = false;
    private boolean skip = false;

    @SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);
//		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		
	    getSupportActionBar().hide();
	    
		setContentView(R.layout.activity_splash_screen);
        Typeface typeface_muko = Typeface.createFromAsset(getAssets(),"futura-condensed-medium.ttf");
        netStatus = internetStatus.isOnline(getApplicationContext());

        Intent wearIntent = getIntent();
        wearQuery = wearIntent.getStringExtra("Query");
		
		splash_screen_activity_layout_signup = (Button) findViewById(R.id.splash_screen_activity_layout_signup);
		splash_screen_activity_layout_signin = (Button) findViewById(R.id.splash_screen_activity_layout_signin);
		
		splash_screen_activity_layout_signup.setVisibility(View.INVISIBLE);
		splash_screen_activity_layout_signin.setVisibility(View.INVISIBLE);

        splash_screen_activity_layout_signup.setTypeface(typeface_muko);
        splash_screen_activity_layout_signin.setTypeface(typeface_muko);
		
		settings = getPreferences(MODE_PRIVATE);
		accessToken = settings.getString(PREF_ACCESSTOKEN, null);
		accessTokenSecret = settings.getString(PREF_ACCESSTOKENSECRET, null);

        speechText = getSharedPreferences("LASTUSERQUERY", MODE_PRIVATE);
		
		muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
		nav_check = getApplicationContext().getSharedPreferences("Muko_nav_check", MODE_PRIVATE);
        skipperdude = getApplicationContext().getSharedPreferences("skipperdude", MODE_PRIVATE);
		
		rdio = new Rdio(appKey, appSecret, accessToken, accessTokenSecret, this, this);
		
		logout = getSharedPreferences("LogoutTrigger", Context.MODE_PRIVATE);
		logoutchecker = logout.getBoolean("TriggerLogout",false);
		
		if(logoutchecker){
			accessToken = null;
			accessTokenSecret = null;
			
			Editor editor = settings.edit();
			editor.clear();
			editor.commit();
			
			Editor editor1 = muko_pref.edit();
			editor1.clear();
			editor1.commit();

            Editor editor2 = skipperdude.edit();
            editor2.clear();
            editor2.commit();

            Editor logoutreset = logout.edit();
            logoutreset.clear();
            logoutreset.commit();
		}


        if(skipperdude.getBoolean("isThisDudesASkipper",false)){
            Toast.makeText(this,"Default User, to personalize your account please Logout and Login with your Premium rdio" +
                    " account",Toast.LENGTH_LONG).show();
            skip = true;
        }
		
		if ((accessToken == null || accessTokenSecret == null) && !skip) {
			
	        /****** Create Thread that will sleep for 5 seconds *************/
			new Handler().postDelayed(new Runnable()
			{
	            // Using handler with postDelayed called runnable run method
	            @Override
	            public void run()
	            {
	            	splash_screen_activity_layout_signup.setVisibility(View.VISIBLE);
	        		splash_screen_activity_layout_signin.setVisibility(View.VISIBLE);
	            }
	        }, 2*1000); // wait for 3 seconds
            
			
			//TODO Move to OnClick
//			Intent myIntent = new Intent(SplashScreenActivity.this,
//					OAuth1WebViewActivity.class);
//			myIntent.putExtra(OAuth1WebViewActivity.EXTRA_CONSUMER_KEY, appKey);
//			myIntent.putExtra(OAuth1WebViewActivity.EXTRA_CONSUMER_SECRET, appSecret);
//			SplashScreenActivity.this.startActivityForResult(myIntent, 1);

		} else {
			Log.d(TAG, "Found cached credentials:");
			Log.d(TAG, "Access token: " + accessToken);
			Log.d(TAG, "Access token secret: " + accessTokenSecret);
			userLoggedIn = true;
			rdio.prepareForPlayback();
		}

		splash_screen_activity_layout_signup.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent genrelistactivityIntent = new Intent(SplashScreenActivity.this,GenreListActivity.class);
//				startActivity(genrelistactivityIntent);
				
				// If either one is null, reset both of them
                netStatus = internetStatus.isOnline(getApplicationContext());
                if(netStatus)
                {
                    accessToken = accessTokenSecret = null;
                    Intent myIntent = new Intent(SplashScreenActivity.this,
                            OAuth1WebViewActivity.class);
                    myIntent.putExtra(OAuth1WebViewActivity.EXTRA_CONSUMER_KEY, appKey);
                    myIntent.putExtra(OAuth1WebViewActivity.EXTRA_CONSUMER_SECRET, appSecret);
                    SplashScreenActivity.this.startActivityForResult(myIntent, 1);
                }
                else
                {
                    alert("Need Internet Connection");
                }
			}
		});
		
		splash_screen_activity_layout_signin.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				splash_screen_activity_layout_signup.performClick();

                Toast.makeText(SplashScreenActivity.this,"Default User, to personalize your account please Logout and Login with your Premium rdio" +
                        " account",Toast.LENGTH_LONG).show();
                accessToken = null;
                accessTokenSecret = null;
                skip = true;
                Editor bla = skipperdude.edit();
                bla.putBoolean("isThisDudesASkipper",true);
                bla.commit();

                firstName = "Default";
                lastName = "User";
                USERID = "defaultuser";

                Editor editor = settings.edit();
                editor.putString("userid",USERID);
                editor.putString("userKey", USERID);

                editor.putString("firstName", firstName);
                editor.putString("lastName", lastName);
                //commit changes to shared preference
                editor.commit();
                Log.e("USERID",USERID);

                SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                Editor editor_parse = muko_pref.edit();
                editor_parse.putString("userKey", USERID);
                editor_parse.putString("firstName", firstName);
                editor_parse.putString("lastName", lastName);
                editor_parse.commit();
                //commit changes to shared preference
                editor.commit();



                rdio.prepareForPlayback();

			}
		});
		

	}

	
	/*
	 * Dispatched by the Rdio object once the setTokenAndSecret call has finished, and the credentials are
	 * ready to be used to make API calls.  The token & token secret are passed in so that you can
	 * save/cache them for future re-use.
	 * @see com.rdio.android.api.RdioListener#onRdioAuthorised(java.lang.String, java.lang.String)
	 */
	@Override
	public void onRdioAuthorised(String arg0, String arg1) {
		Log.i(TAG, "Application authorised, saving access token & secret.");
		Log.d(TAG, "Access token: " + accessToken);
		Log.d(TAG, "Access token secret: " + accessTokenSecret);

		//SharedPreferences settings = getPreferences(MODE_PRIVATE);
		Editor editor = settings.edit();
		editor.putString(PREF_ACCESSTOKEN, accessToken);
		editor.putString(PREF_ACCESSTOKENSECRET, accessTokenSecret);
		editor.commit();
		
		Editor editor1 = logout.edit();
		editor1.putBoolean("TriggerLogout", false);
		editor1.commit();
	}

	@Override
	public void onRdioReadyForPlayback() {
		
		//Making SignUp And SignIn Buttons Invisible
		splash_screen_activity_layout_signup.setVisibility(View.GONE);
		splash_screen_activity_layout_signin.setVisibility(View.GONE);
		
		//Calling ProgressDialog
		genreProgressDialog = new ProgressDialog(SplashScreenActivity.this);
		// Set progressdialog title
		genreProgressDialog.setTitle("Muko");
		// Set progressdialog message
		genreProgressDialog.setMessage("Loading...");
		genreProgressDialog.setIndeterminate(false);
		genreProgressDialog.setCancelable(false);
		genreProgressDialog.setCanceledOnTouchOutside(false);
		// Show progressdialog
        try {
            genreProgressDialog.show();
        }
        catch(Exception error){
            Log.e("Exception with ProgressDialog show()", error.toString());
        }
		
		settings = getPreferences(MODE_PRIVATE);
		String userKey = settings.getString("userKey", null);

        //Resuming MukoPlayListActivityforWear if its running in the background
//        if(musicSrv.checkstartedfromPlaylistWear()){
//            userMovedtoActivity = true;
//            Intent intent = new Intent(SplashScreenActivity.this, MucoPlayListActivityforWear.class);
//            startActivity(intent);
//            donotstart = true;
//            finish();
//        }
	    if(musicSrv!=null)//Pass RDIO Object only when its not initialized hence the else if
        {
            musicSrv.passRDIOObject(rdio);
            String test = rdio.toString(); //com.rdio.android.api.Rdio@426bd8d0  //com.rdio.android.api.Rdio@426bd8d0
			String test1 = test;		//com.rdio.android.api.Rdio@426bd8d0
			 test1 = rdio.toString();
            //doSomething();
        }

        //If userkey is null we need to fetch it from rdio with the available information from rdio login
		if(userKey == null && !skip)
		{
			rdio.apiCall("currentUser", null, new RdioApiCallback() {

				@Override
				public void onApiFailure(String methodName, Exception e)
				{
					Log.e("API FAILURE","Failed to fetch User Information");
				}

				@Override
				public void onApiSuccess(JSONObject result)
				{
					Log.i("API Success", "JSON Object Obtained");
				    try {
				    		JSONObject resultObtained;
				    		resultObtained = new JSONObject();
				    		resultObtained = result.getJSONObject("result");
				    		firstName = resultObtained.getString("firstName");
				    		lastName = resultObtained.getString("lastName");
				    		USERID = resultObtained.getString("key");
				    		//TODO Save below info into shared pref and set it work only once when
				    		//the user is logging in for first time
				                    
				    		Editor editor = settings.edit();
				    		editor.putString("userid",USERID);
				    		editor.putString("userKey", USERID);
				    		
				    		editor.putString("firstName", firstName);
				    		editor.putString("lastName", lastName);
					    	//commit changes to shared preference
					    	editor.commit();
				            Log.e("USERID",USERID);
				            
				            SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
				            Editor editor_parse = muko_pref.edit();
				            editor_parse.putString("userKey", USERID);
				            editor_parse.putString("firstName", firstName);
				            editor_parse.putString("lastName", lastName);
				    		editor_parse.commit();


                            if(netStatus)
                            {
                                parseCheck();
                                //Launch GenreListActivity screen
                                userMovedtoActivity = true;
                                genreProgressDialog.cancel();
                                Intent genrelistactivityIntent = new Intent(SplashScreenActivity.this,GenreListActivity.class);
                                startActivity(genrelistactivityIntent);
                                finish();
                            }
                            else
                            {
                                alert("Need Internet Connection");
                            }

//				            //Launch GenreListActivity screen
//				            userMovedtoActivity = true;
//				            genreProgressDialog.cancel();
//			        		Intent genrelistactivityIntent = new Intent(SplashScreenActivity.this,GenreListActivity.class);
//							startActivity(genrelistactivityIntent);
//							finish();
				    	}
				    catch(Exception e){
				    	Log.e("UserID Call",e.toString());
				    }
				}
				});
			
	        if(musicSrv!=null){
	            musicSrv.passRDIOObject(rdio);
	            String test = rdio.toString(); //com.rdio.android.api.Rdio@426bd8d0  //com.rdio.android.api.Rdio@426bd8d0
				String test1 = test;		//com.rdio.android.api.Rdio@426bd8d0
	            //doSomething();
	        }
		}
		else
		{
            if(!donotstart)//This is needed so that the home activity
            // wont get started when user is trying to come back to MucoPlayListActivityforWear
            {
                parseCheck();
                if(netStatus)
                {
                    ////Launch HomeActivity screen
                    userMovedtoActivity = true;
                    genreProgressDialog.cancel();

                    if (wearQuery != null) {
                        wearHandler();
                    } else {
                        Intent genrelistactivityIntent = new Intent(SplashScreenActivity.this, HomeActivity.class);
                        startActivity(genrelistactivityIntent);
                        finish();
                    }
                }
                else
                {
                    alert("Need Internet Connection");
                }
            }
		}
		
		//finish();
		
//		new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                finish();
//            }
//        }, 40000);
		
		
	}

	@Override
	public void onRdioUserPlayingElsewhere() {
		// TODO keep empty
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				Log.v(TAG, "Login success");
				if (data != null) {
					accessToken = data.getStringExtra("token");
					
					accessTokenSecret = data.getStringExtra("tokenSecret");
					onRdioAuthorised(accessToken, accessTokenSecret);
					rdio.setTokenAndSecret(accessToken, accessTokenSecret);
					rdio.prepareForPlayback();
				}
			} else if (resultCode == RESULT_CANCELED) {
				if (data != null) {
					String errorCode = data.getStringExtra(OAuth1WebViewActivity.EXTRA_ERROR_CODE);
					String errorDescription = data.getStringExtra(OAuth1WebViewActivity.EXTRA_ERROR_DESCRIPTION);
					Log.v(TAG, "ERROR: " + errorCode + " - " + errorDescription);
				}
				accessToken = null;
				accessTokenSecret = null;
			}
			
		}
	}
	
	@Override
	public void onDestroy()
	{
		unbindService(musicConnection);
		//If user moves to other Activity dont stop service
        if(!userMovedtoActivity ){
        stopService(playIntent);
        Log.wtf(TAG, "Service Terminated");
        playIntent=null;
		rdio=null;
		musicSrv=null;
		}
		
		super.onDestroy();	
	}
	
    @Override
    public void onResume(){

    	Log.wtf(TAG, "OnResume Connected");
    	if(playIntent == null)
    		playIntent = new Intent(this, MusicService.class);
		bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
		startService(playIntent);
		
    	super.onResume();
    }
	
	@Override
	public void onPause()
	{
		super.onPause();
	}
	
	
	//------------------------------------------Service Connection----------------------------------------------
	
	private Intent playIntent;
	
	MusicService musicSrv;
	
	
  	private ServiceConnection musicConnection = new ServiceConnection(){

  		@Override
  		public void onServiceConnected(ComponentName name, IBinder service) {
  			//Toast.makeText(getApplicationContext(), "Service Connected", Toast.LENGTH_LONG).show();
  			MusicBinder binder = (MusicBinder)service;
  			//get service
            if(netStatus)
            {
                musicSrv = binder.getService();
            }
            else
            {
                alert("Need Internet Connection");
            }
//  			
//  			musicSrv.passRDIOObject(rdio);
  			
  		}

  		@Override
  		public void onServiceDisconnected(ComponentName name) {
  			Log.wtf(TAG, "Service Disconnected");
  		}
  	};
  	
  	
  	/**
  	 * This Method checks for the userkey present or not
  	 * If not present Creates new user in Parse
  	 * */
  	public void parseCheck()
  	{
        Editor editor = muko_pref.edit();
        final String userKey = muko_pref.getString("userKey", null);
		final String firstName = muko_pref.getString("firstName", null);
		final String lastName = muko_pref.getString("lastName", null);
		
		Editor editor_nav_check = nav_check.edit();
		editor_nav_check.putString("userKey", userKey);
		editor_nav_check.commit();

			ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");
			query.whereEqualTo("userKey",userKey);
			query.findInBackground(new FindCallback<ParseObject>()
			{
			    @Override
				public void done(List<ParseObject> parseList, ParseException e)
			    {
			        if (e == null)
			        {
			    		for (ParseObject parse : parseList)
			    		{
			    			String ObjectId = parse.getObjectId();
			    			String firstname = parse.getString("firstName");
			    			
			    			parseJsonChecked_status = parse.getJSONArray("genres_selected");
			    			
			    			SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE); 
			    	        Editor editor = muko_pref.edit();
			    			editor.putString("objectid", ObjectId);
			    			editor.commit();
			    			
			    			settings = getPreferences(MODE_PRIVATE);
							final String userKey = settings.getString("userKey", null);
							final String objectid = settings.getString("objectid", null);
							String test = objectid;
			    			
			    		}
			        	
			        	if(parseList.isEmpty())
			        	{
				        	final ParseObject mukoParse = new ParseObject("MKRdioUser");
							mukoParse.put("firstName", firstName);
							mukoParse.put("lastName", lastName);
							mukoParse.put("userKey", userKey);
							mukoParse.saveInBackground((new SaveCallback()
							{
								@Override
								public void done(ParseException e)
								{
									// Handle success or failure here ...
									if(e==null)
									{
										String objectId = mukoParse.getObjectId();
										
										SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE); 
						    	        Editor editor = muko_pref.edit();
						    			editor.putString("objectid", objectId);
						    			editor.commit();
										
//						    			userMovedtoActivity = true;
//										Intent genrelistactivityIntent = new Intent(SplashScreenActivity.this,GenreListActivity.class);
//										startActivity(genrelistactivityIntent);
//										finish();
									}
									else
									{
										  Toast.makeText(getApplicationContext(), "FAILURE", Toast.LENGTH_SHORT).show();
									}
								}
							}));
			        	}
			        	else
			        	{
//			        		//Call HomeActivity activity
//			        		userMovedtoActivity = true;
//			        		Intent genrelistactivityIntent = new Intent(SplashScreenActivity.this,GenreListActivity.class);
//							startActivity(genrelistactivityIntent);
//							finish();
			        	}
			        }
			        else
			        {

			        }
			    }
			});


		
		

  	}

    //----------------------------------------------Wear Handling Function------------------------/

    JSONObject songsThumbInfoJsonObject = new JSONObject();
    JSONObject songsThumbInfoJsonObject_null = new JSONObject();

    void wearHandler()
    {
        // TODO Auto-generated method stub
//        // Create a progressdialog
//        genreProgressDialog = new ProgressDialog(getActivity());
//        // Set progressdialog title
//        genreProgressDialog.setTitle("Muko");
//        // Set progressdialog message
//        genreProgressDialog.setMessage("Loading...");
//        genreProgressDialog.setIndeterminate(false);
//        genreProgressDialog.setCancelable(false);
//        genreProgressDialog.setCanceledOnTouchOutside(false);
//        // Show progressdialog

//        genreProgressDialog.show();

        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko",MODE_PRIVATE);
        final String userKey = muko_pref.getString("userKey", null);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");
        query.whereEqualTo("userKey", userKey);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseList, ParseException e) {
                if (e == null) {
                    for (ParseObject parse : parseList) {
                        String objectID = parse.getObjectId();
                        String firstname = parse.getString("firstName");

                        songsThumbInfoJsonObject = parse.getJSONObject("songsThumbInfo");
                        JSONObject test = new JSONObject();
                        test = songsThumbInfoJsonObject_null;

                        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                        Editor editor_parse = muko_pref.edit();
                        editor_parse.putString("objectid", objectID);
                        editor_parse.commit();

                        if (songsThumbInfoJsonObject == null) {
                            songsThumbInfoJsonObject = songsThumbInfoJsonObject_null;
                        }
                    }

//                    genreProgressDialog.cancel();
                    Intent intent = new Intent(SplashScreenActivity.this, MucoPlayListActivityforWear.class);
                    intent.putExtra("speechquery", wearQuery);
                    //Saving user query to shared pref
                    saveLastUserQuery(wearQuery);
                    intent.putExtra("songsThumbInfo_Object", songsThumbInfoJsonObject.toString());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    SharedPreferences speechText;

    /**
     * Function to save last user Query to preference
     * @param newQuery
     */
    public void saveLastUserQuery(String newQuery){
        Editor x = speechText.edit();
        x.putString("LastUserQuery", newQuery);
        x.commit();
    }

    /**
     * Shows Alert Dialog. Accept string parameter
     * */
    @SuppressWarnings("deprecation")
    public void alert(String message)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(SplashScreenActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("MUKO");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //TODO Close the app
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

}
