package com.neurlabs.muko;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.neurlabs.muko.adapters.NavDrawerListAdapter;
import com.neurlabs.muko.fragments.HomeFragment;
import com.neurlabs.muko.fragments.ProfileFragment;
import com.neurlabs.muko.models.NavDrawerItem;
import com.neurlabs.muko.services.MusicService;
import com.neurlabs.muko.services.MusicService.MusicBinder;
import com.neurlabs.muko.utilis.AppInfo;
import com.neurlabs.muko.utilis.CustomKeyboard;
import com.neurlabs.muko.utilis.DataThrower;
import com.neurlabs.muko.utilis.InternetStatus;
import com.nuance.nmdp.speechkit.Prompt;
import com.nuance.nmdp.speechkit.SpeechKit;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.rdio.android.api.Rdio;

public class HomeActivity extends ActionBarActivity implements DataThrower
{
	private static String url = "http://muko-music-search.elasticbeanstalk.com/api/search/queryinjson";
	private static final String TAG = "Muko App";
	private DrawerLayout mDrawerLayout;
	private ListView HomeListView;
	private ActionBarDrawerToggle mDrawerToggle;

	// navigation drawer title
	private CharSequence mDrawerTitle;

	// used to store application title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	SharedPreferences nav_check;
    SharedPreferences skipperdude;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
    CustomKeyboard keyboard = new CustomKeyboard();

    InternetStatus internetStatus = new InternetStatus();
    Boolean netStatus;
	
	Rdio rdio;

	
	 private static SpeechKit _speechKit;
	 
	    public static SpeechKit getSpeechKit()
	    {
	        return _speechKit;
	    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
//		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

        netStatus = internetStatus.isOnline(getApplicationContext());
		nav_check = getApplicationContext().getSharedPreferences("Muko_nav_check", MODE_PRIVATE);

        skipperdude = getApplicationContext().getSharedPreferences("skipperdude", MODE_PRIVATE);

		Editor editor = nav_check.edit();
		editor.clear();
		editor.commit();

        if(netStatus)
        {
            _speechKit = SpeechKit.initialize(getApplication().getApplicationContext(), AppInfo.SpeechKitAppId, AppInfo.SpeechKitServer, AppInfo.SpeechKitPort, AppInfo.SpeechKitSsl, AppInfo.SpeechKitApplicationKey);
            _speechKit.connect();
        }
        else
        {
            alert("Need internet connection");
        }
        // TODO: Keep an eye out for audio prompts not working on the Droid 2 or other 2.2 devices.
        Prompt beep = _speechKit.defineAudioPrompt(R.raw.beep);
        _speechKit.setDefaultRecognizerPrompts(beep, Prompt.vibration(100), null, null);

        speechText = getSharedPreferences("LASTUSERQUERY", MODE_PRIVATE);
		
		mTitle = mDrawerTitle = getTitle();
		
		getSupportActionBar().setTitle("HOME");
		//getSupportActionBar().setBackgroundDrawable(new ColorDrawable(conte.getResources().getColor(Color.BLACK)));
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#26AE60"));
		getSupportActionBar().setBackgroundDrawable(colorDrawable);
		
		/**/
		mTitle = mDrawerTitle = getTitle();
		
		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		
		// nav drawer icons from resources
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		HomeListView = (ListView) findViewById(R.id.list_slidermenu);
		
		navDrawerItems = new ArrayList<NavDrawerItem>();
		
		// adding nav drawer items to array
		// Home
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		// Profile
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		
		// Recycle the typed array
		navMenuIcons.recycle();
		
		HomeListView.setOnItemClickListener(new SlideMenuClickListener());
		
		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),	navDrawerItems);
		HomeListView.setAdapter(adapter);
		
//		// enabling action bar app icon and behaving it as toggle button
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		getSupportActionBar().setCustomView(R.layout.home_fragment_custom_actionbar);
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		
		//getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent_top_bar));
		
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.drawable.hamburger_icon, R.string.app,R.string.app)
		{
			@Override
			public void onDrawerClosed(View view)
			{
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
                keyboard.hideSoftKeyboard(HomeActivity.this);
			}
			
			@Override
			public void onDrawerOpened(View drawerView)
			{
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
                keyboard.hideSoftKeyboard(HomeActivity.this);
			}
			
			@Override
			public void onDrawerSlide(View drawerView, float offset)
			{
				super.onDrawerSlide(drawerView, 0);
                keyboard.hideSoftKeyboard(HomeActivity.this);
			}
		};
//		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
//				R.drawable.ic_launcher, //nav menu toggle icon
//				R.string.app_name, // nav drawer open - description for accessibility
//				R.string.app_name // nav drawer close - description for accessibility
//		) {
//			public void onDrawerClosed(View view)
//			{
//				getSupportActionBar().setTitle(mTitle);
//				// calling onPrepareOptionsMenu() to show action bar icons
//				invalidateOptionsMenu();
//			}
//			
//			public void onDrawerOpened(View drawerView)
//			{
//				getSupportActionBar().setTitle(mDrawerTitle);
//				// calling onPrepareOptionsMenu() to hide action bar icons
//				invalidateOptionsMenu();
//			}
//		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		
		if (savedInstanceState == null)
		{
			// on first time display view for first nav item
			displayView(0);
		}

        RecieverActivator();

        isVisibleActivity();
	}


    /**
     * Alert User to Login
     */
    public void userAlerttoLoginProperly(){
        if(skipperdude.getBoolean("isThisDudesASkipper",false)){
            Toast.makeText(HomeActivity.this,"To personalize your account and for a better experience please Logout and Login with your Premium rdio" +
                    " account",Toast.LENGTH_LONG).show();
        }
    }

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements ListView.OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,	long id)
		{
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
//		// toggle nav drawer on selecting action bar app icon/title
//		if (mDrawerToggle.onOptionsItemSelected(item))
//		{
//			return true;
//		}
//		// Handle action bar actions click
//		switch (item.getItemId())
//		{
//			case R.id.action_settings:
//				return true;
//			default:
//				return super.onOptionsItemSelected(item);
//		}
		
		// toggle nav drawer on selecting action bar app icon/title
				if (mDrawerToggle.onOptionsItemSelected(item))
				{
					return true;
				}
				// Handle action bar actions click
				switch (item.getItemId())
				{
//					case R.id.action_settings:
//						recreate();
//						return true;
					default:
						return super.onOptionsItemSelected(item);
				}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
//	@Override
//	public boolean onPrepareOptionsMenu(Menu menu)
//	{
//		// if nav drawer is opened, hide the action items
//		boolean drawerOpen = mDrawerLayout.isDrawerOpen(HomeListView);
//		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
//		return super.onPrepareOptionsMenu(menu);
//	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */

    Fragment fragment = null;

    private void displayView(int position)
	{
		// update the main content by replacing fragments

		switch (position)
		{
		case 0:
			fragment = new HomeFragment();
			break;
		case 1:
            userAlerttoLoginProperly();
			fragment = new ProfileFragment();
			break;

		default:
			break;
		}

		if (fragment != null)
		{
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

			// update selected item and title, then close the drawer
			HomeListView.setItemChecked(position, true);
			HomeListView.setSelection(position);
			setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(HomeListView);
		}
		else
		{
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title)
	{
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();

        RecieverDeactivator();

		stopService(playIntent);
		
		if (_speechKit != null)
        	{
            	_speechKit.release();
            	_speechKit = null;
        	}
	}

	@Override
	public void onResume(){

		Log.wtf(TAG, "OnResume Connected");
		if(playIntent == null)
			playIntent = new Intent(this, MusicService.class);
			bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
			startService(playIntent);
		super.onResume();
}
	
    @Override
    public void onPause(){
    	super.onPause();

//        unregisterReceiver(receiver);

//    	if(musicSrv!=null && musicSrv.isPng())
//		{
//			musicSrv.startForeground();
//			
//		}
    	unbindService(musicConnection);
    }
	
	//------------------------------------------Service Connection----------------------------------------------

	private Intent playIntent;

	MusicService musicSrv;


	private ServiceConnection musicConnection = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
//			Toast.makeText(getApplicationContext(), "Service Connected", Toast.LENGTH_LONG).show();
            try{
                MusicBinder binder = (MusicBinder)service;
                //get service

                if(netStatus)
                {
                    musicSrv = binder.getService();

                    rdio=musicSrv.returnRDIOObject();
                    String test = rdio.toString();
                    String test1 = test;
                }
                else
                {
                    alert("Need Internet Connection");
                }
            }
            catch (Exception e) {
                Intent intent = new Intent(HomeActivity.this, SplashScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.wtf(TAG, "Service Disconnected");
		}
	};
//----------------------------------------Retrieving Query---------------------------------------
	
	@Override
	public void userQuery(String UserQuery) {
		// TODO Auto-generated method stub
		
		Intent intent = new Intent(HomeActivity.this,MukoPlayListActivity.class);
		intent.putExtra("speechquery", UserQuery);
		startActivity(intent);
	}
	
	private boolean doubleBackToExitPressedOnce = false;

//---------------------------------------BackPress Functionality---------------------------------
	@Override
	public void onBackPressed() {
		if (doubleBackToExitPressedOnce) {
			super.onBackPressed();
			return;
		}

		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, "Please click back again to exit",Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce = false;
			}
		}, 2000);
	}

    //-------------------------------Handling Broadcast-----------------------------------------

    public static String BROADCAST_ACTION = "com.neurlabs.muko.UPDATESONGLIST";

    //Recieving Broadcast
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Toast.makeText(MainActivity.this, "Received in MainActivity", Toast.LENGTH_SHORT).show();

            String wearQuery = intent.getStringExtra("UserQueryFromWear");
            saveLastUserQuery(wearQuery);
            if(isVisibleActivity()){
//                Toast.makeText(HomeActivity.this,"Developer Reminder set ProgressBar",Toast.LENGTH_LONG).show();

                if(netStatus)
                {
                    wearHandler(wearQuery);
                }
                else
                {
                    alert("Need Internet Connection");
                }
            }

        }
    };
    SharedPreferences speechText;

    public void saveLastUserQuery(String newQuery){
        Editor x = speechText.edit();
        x.putString("LastUserQuery", newQuery);
        x.commit();
    }

    JSONObject songsThumbInfoJsonObject = new JSONObject();
    JSONObject songsThumbInfoJsonObject_null = new JSONObject();
    ProgressDialog mProgressDialog;

    void wearHandler(final String QueryfromWear)
    {
        // TODO Auto-generated method stub
        // Create a progressdialog
        mProgressDialog = new ProgressDialog(HomeActivity.this);
        // Set progressdialog title
        mProgressDialog.setTitle("Muko");
        // Set progressdialog message
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        // Show progressdialog

        mProgressDialog.show();

        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko",MODE_PRIVATE);
        final String userKey = muko_pref.getString("userKey", null);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");
        query.whereEqualTo("userKey", userKey);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseList, ParseException e) {
                if (e == null) {
                    for (ParseObject parse : parseList) {
                        String objectID = parse.getObjectId();
                        String firstname = parse.getString("firstName");

                        songsThumbInfoJsonObject = parse.getJSONObject("songsThumbInfo");
                        JSONObject test = new JSONObject();
                        test = songsThumbInfoJsonObject_null;

                        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                        Editor editor_parse = muko_pref.edit();
                        editor_parse.putString("objectid", objectID);
                        editor_parse.commit();

                        if (songsThumbInfoJsonObject == null) {
                            songsThumbInfoJsonObject = songsThumbInfoJsonObject_null;
                        }
                    }

                    mProgressDialog.cancel();
                    Intent intent = new Intent(HomeActivity.this, MukoPlayListActivity.class);
                    intent.putExtra("speechquery", QueryfromWear);
                    intent.putExtra("songsThumbInfo_Object", songsThumbInfoJsonObject.toString());
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    /**
     * To Un register Reciever
     */
    public void RecieverDeactivator(){
        Log.e("Reciever Deactivator", "called");
        unregisterReceiver(receiver);
    }

    /**
     * To activate Reciever
     *
     */
    public void RecieverActivator(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION);
        registerReceiver(receiver, filter);
    }

    public Boolean isVisibleActivity(){
        ArrayList<String> runningactivities = new ArrayList<String>();

        ActivityManager activityManager = (ActivityManager)getBaseContext().getSystemService (Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> services = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (int i1 = 0; i1 < services.size(); i1++) {
            runningactivities.add(0,services.get(i1).topActivity.toString());
        }

//        Log.v("running activity",runningactivities.toString());
//        Toast.makeText(HomeActivity.this,"isVisible:"+runningactivities.toString(),Toast.LENGTH_LONG).show();

        //[ComponentInfo{com.android.launcher/com.android.launcher2.Launcher}, ComponentInfo{com.neurlabs.muko/com.neurlabs.muko.HomeActivity}]

        if(runningactivities.contains("ComponentInfo{com.neurlabs.muko/com.neurlabs.muko.HomeActivity}")){
            return true;
        }
        return false;
    }

    /**
     * Shows Alert Dialog. Accept string parameter
     * */
    @SuppressWarnings("deprecation")
    public void alert(String message)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(HomeActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("MUKO");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //TODO Close the app
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


}
