package com.neurlabs.muko.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.neurlabs.muko.R;
import com.neurlabs.muko.models.ProfileModel;

public class ProfileListAdapter extends ArrayAdapter<ProfileModel>
{
	private List<ProfileModel> profileList;
	private Context context;


	public ProfileListAdapter(List<ProfileModel> profileList, Context ctx)
	{
		super(ctx, R.layout.profile_screen_list_row, profileList);
		this.profileList = profileList;
		this.context = ctx;
	}

	@Override
	public int getCount() {
		return profileList.size();
	}

	@Override
	public ProfileModel getItem(int position) {
		return profileList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return profileList.get(position).hashCode();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;

		IdeasHolder holder = new IdeasHolder();

		// First let's verify the convertView is not null
		if (convertView == null)
		{
			// This a new view we inflate the new layout
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.profile_screen_list_row, null);
			// Now we can fill the layout with the right values
            Typeface typeface_muko = Typeface.createFromAsset(context.getAssets(),"futura-condensed-medium.ttf");
			TextView tv_heading1 = (TextView) view.findViewById(R.id.profile_screen_list_row_textView);
			ImageView iv_universe = (ImageView) view.findViewById(R.id.profile_screen_list_row_imageView);
			tv_heading1.setTypeface(typeface_muko);
			
			holder.Profile_name = tv_heading1;
			holder.Profile_image = iv_universe;
			
			view.setTag(holder);
		}
		else
			holder = (IdeasHolder) view.getTag();

		ProfileModel profileModel = profileList.get(position);
		holder.Profile_name.setText(profileModel.getProfileName());
		holder.Profile_image.setImageResource(profileModel.getProfileImage());

        // Listen for ListView Item Click
//        view.setOnClickListener(new OnClickListener()
//        {
//            @Override
//            public void onClick(View arg0)
//            {
//            	
//            }
//        });	
		
		
		
		return view;
	}

	/* *********************************
	 * We use the holder pattern        
	 * It makes the view faster and avoid finding the component
	 * **********************************/

	private static class IdeasHolder {
		public TextView Profile_name;
		public ImageView Profile_image;
	}


}
