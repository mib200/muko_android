package com.neurlabs.muko.adapters;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neurlabs.muko.MucoPlayListActivityforWear;
import com.neurlabs.muko.MukoPlayListActivity;
import com.neurlabs.muko.R;
import com.neurlabs.muko.models.TracksModel;

public class MukoPlayListAdapter extends BaseAdapter
{
	private LayoutInflater layoutInflater;
	ArrayList<TracksModel> SongList;
	Context context;
	private int bckgrndsetter;
	private int selectedIndex = -1;
	private int selected_like_dislike = -1;
	private int selected_dislike = -1;
    //private int clear_like_dislike = -1;
	JSONObject parse_like_dislike;
    String Songkey;
    int position_like_unlike;
    TracksModel track;
    boolean startedFrmDfltPlayList;

	ArrayList<Integer> selected_like_dislike_array = new ArrayList<Integer>(15);

	public MukoPlayListAdapter(Context context, ArrayList<TracksModel> SongList,JSONObject songsThumbInfoJsonObject,boolean startedfromDefaultPlaylist/*, String[] imageArray*/) {
		this.SongList = SongList;
		this.parse_like_dislike = songsThumbInfoJsonObject;
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		bckgrndsetter=1;
		int size = SongList.size();
		int s = size;
        startedFrmDfltPlayList = startedfromDefaultPlaylist;
	}

    public void updtSongList(ArrayList<TracksModel> SongList){
        this.SongList = SongList;
    }

	@Override
	public int getCount() {
		return SongList.size();
	}

	@Override
	public Object getItem(int position) {
		return SongList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final ViewHolder holder;
		final int pos = position;
        position_like_unlike = position;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.muko_play_list_row, null);
			holder = new ViewHolder();

            Typeface typeface_muko = Typeface.createFromAsset(context.getAssets(),"futura-condensed-medium.ttf");
            holder.mainLayout = (RelativeLayout) convertView.findViewById(R.id.muko_play_list_row_relativelayout);
			holder.LikeUnlike = (RelativeLayout) convertView.findViewById(R.id.muko_play_list_row_relativelayout_like);
			holder.dislike_selected = (ImageView) convertView.findViewById(R.id.muko_play_list_row_imageView_dislike_selected);
			holder.like_selected = (ImageView) convertView.findViewById(R.id.muko_play_list_row_imageView_like_selected);
			holder.rowClick = (ImageView) convertView.findViewById(R.id.muko_play_list_row_imageView_click);
			holder.like = (ImageView) convertView.findViewById(R.id.muko_play_list_row_imageView_like);
			holder.dislike = (ImageView) convertView.findViewById(R.id.muko_play_list_row_imageView_dislik);
			holder.SongName = (TextView) convertView.findViewById(R.id.muko_play_list_row_textView_songname);
			holder.ArtistName = (TextView) convertView.findViewById(R.id.muko_play_list_row_textView_artist);
            holder.SongName.setTypeface(typeface_muko);
            holder.ArtistName.setTypeface(typeface_muko);

            //Setting holder listeners to null if recycled
            holder.LikeUnlike.setOnClickListener(null);
            holder.dislike_selected.setOnClickListener(null);
            holder.like_selected.setOnClickListener(null);
            holder.like.setOnClickListener(null);
            holder.dislike.setOnClickListener(null);

			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		track = SongList.get(position);
		String Song = track.getSongName();
		String Artist = track.getArtistName();
        String key = track.getID();

        if(key.equals(Songkey))
        {
            holder.mainLayout.setBackgroundColor(Color.parseColor("#73D66C"));
        }
        else
        {
            holder.mainLayout.setBackgroundColor(Color.TRANSPARENT);
        }

		holder.LikeUnlike.setVisibility(View.GONE);
//		holder.dislike_selected.setVisibility(View.GONE);
//		holder.like_selected.setVisibility(View.GONE);
		holder.like.setVisibility(View.GONE);
		holder.dislike.setVisibility(View.GONE);

		holder.SongName.setText(Song);
		holder.ArtistName.setText(Artist);

		if(position == selectedIndex)
		{
			holder.rowClick.setVisibility(View.GONE);
			holder.like.setVisibility(View.GONE);
			holder.dislike.setVisibility(View.GONE);
			holder.LikeUnlike.setVisibility(View.VISIBLE);
//			holder.dislike_selected.setVisibility(View.VISIBLE);
//			holder.like_selected.setVisibility(View.VISIBLE);
		}
		else
		{

		}

//        if(clear_like_dislike == 99999)
//        {
//            if (holder.LikeUnlike.getVisibility() == View.VISIBLE )
//            {
//                // Its visible
//                holder.LikeUnlike.setVisibility(View.GONE);
//            } else {
//                // Either gone or GONE
//            }
//        }
//        else
//        {
//
//        }

		String songname = SongList.get(pos).getSongName();
		String artistname = SongList.get(pos).getArtistName();
        String songkey = SongList.get(pos).getID();
		if(parse_like_dislike.has(songname+":"+artistname))
		{
			try {
				boolean like_dislike_bool = parse_like_dislike.getBoolean(songname+":"+artistname);
				if(like_dislike_bool)
				{
					holder.like.setVisibility(View.VISIBLE);
//					holder.like_selected.setVisibility(View.GONE);
//					holder.dislike_selected.setVisibility(View.GONE);
					holder.rowClick.setVisibility(View.GONE);
					holder.LikeUnlike.setVisibility(View.GONE);
				}
				else
				{
					holder.dislike.setVisibility(View.VISIBLE);
//					holder.like_selected.setVisibility(View.GONE);
//					holder.dislike_selected.setVisibility(View.GONE);
					holder.rowClick.setVisibility(View.GONE);
					holder.LikeUnlike.setVisibility(View.GONE);
				}
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		else
		{
			holder.rowClick.setVisibility(View.VISIBLE);
		}
		
		if(position == selected_like_dislike)
		{
			holder.rowClick.setVisibility(View.GONE);
			holder.like.setVisibility(View.GONE);
			holder.dislike.setVisibility(View.GONE);
			holder.LikeUnlike.setVisibility(View.VISIBLE);
//			holder.dislike_selected.setVisibility(View.VISIBLE);
//			holder.like_selected.setVisibility(View.VISIBLE);
		}
		else
		{
			
		}
		
		if (holder.LikeUnlike.getVisibility() == View.VISIBLE && 
				holder.rowClick.getVisibility() == View.VISIBLE)
		{
		    // Its visible
			holder.rowClick.setVisibility(View.GONE);
		} else {
		    // Either gone or GONE
		}


		holder.rowClick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				selectedIndex=pos;
				selected_like_dislike = -1;
//                clear_like_dislike = pos;
				holder.rowClick.setVisibility(View.GONE);
				holder.LikeUnlike.setVisibility(View.VISIBLE);
				//rl_relative.setBackgroundColor(Color.parseColor("#73D66C"));
				notifyDataSetChanged();
				
			}
		});
		
		holder.like_selected.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String songname = SongList.get(pos).getSongName();
				String artistname = SongList.get(pos).getArtistName();
				String parse_data = songname+":"+artistname;

//                clear_like_dislike = pos;
				selectedIndex= -1;
				selected_like_dislike = -1;
				try {
					parse_like_dislike.put(parse_data, true);
					JSONObject test = new JSONObject();
					JSONObject test1 = new JSONObject();
					test = parse_like_dislike;
					test1=test;
				} catch (JSONException e) {
					e.printStackTrace();
				}

//                track.setLikeUnlikeInfo(1);


                if(startedFrmDfltPlayList) {
                    ((MukoPlayListActivity) context).getselectedObject(parse_like_dislike);
                    ((MukoPlayListActivity) context).updtlikeunlikeonwear(songname,artistname,1,pos);
                }
                else {
                    ((MucoPlayListActivityforWear) context).getselectedObject(parse_like_dislike);
                    ((MucoPlayListActivityforWear) context).updtlikeunlikeonwear(songname,artistname,1,pos);
                }


				holder.like.setVisibility(View.VISIBLE);
//
//				holder.like_selected.setVisibility(View.GONE);
//				holder.dislike_selected.setVisibility(View.GONE);
//				holder.rowClick.setVisibility(View.GONE);
				holder.LikeUnlike.setVisibility(View.GONE);
				notifyDataSetChanged();
				
			}
		});
		
		holder.dislike_selected.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String songname = SongList.get(pos).getSongName();
				String artistname = SongList.get(pos).getArtistName();
				String parse_data = songname+":"+artistname;

//                clear_like_dislike = pos;
				selectedIndex= -1;
				selected_like_dislike = -1;
				
				try {
					parse_like_dislike.put(parse_data, false);
					JSONObject test = new JSONObject();
					JSONObject test1 = new JSONObject();
					test = parse_like_dislike;
					test1=test;
				} catch (JSONException e) {
					e.printStackTrace();
				}

                //TODO Probably should update wear here
//                track.setLikeUnlikeInfo(0);

                if(startedFrmDfltPlayList) {
                    ((MukoPlayListActivity) context).getselectedObject(parse_like_dislike);
                    ((MukoPlayListActivity) context).updtlikeunlikeonwear(songname,artistname,0,pos);
                }
				else {
                    ((MucoPlayListActivityforWear) context).getselectedObject(parse_like_dislike);
                    ((MucoPlayListActivityforWear) context).updtlikeunlikeonwear(songname,artistname,0,pos);
                }

				holder.dislike.setVisibility(View.VISIBLE);
//
//				holder.dislike_selected.setVisibility(View.GONE);
//				holder.like_selected.setVisibility(View.GONE);
//				holder.rowClick.setVisibility(View.GONE);
				holder.LikeUnlike.setVisibility(View.GONE);
				notifyDataSetChanged();
			}
		});
		
		
		holder.like.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String songname = SongList.get(pos).getSongName();
				String artistname = SongList.get(pos).getArtistName();
				String parse_data = songname+":"+artistname;

//                clear_like_dislike = pos;
				selected_like_dislike=pos;
				selectedIndex = -1;
				holder.like.setVisibility(View.GONE);
//				holder.rowClick.setVisibility(View.GONE);
				holder.LikeUnlike.setVisibility(View.VISIBLE);
				//rl_relative.setBackgroundColor(Color.parseColor("#73D66C"));
				notifyDataSetChanged();
				
			}
		});
		
		holder.dislike.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				selected_like_dislike=pos;
//                clear_like_dislike = pos;
				selectedIndex = -1;
				holder.dislike.setVisibility(View.GONE);
				holder.LikeUnlike.setVisibility(View.VISIBLE);
				notifyDataSetChanged();
				
			}
		});
		
		
		return convertView;
	}
	
	static class ViewHolder {
		TextView SongName;
		TextView ArtistName;
		RelativeLayout LikeUnlike;
        RelativeLayout mainLayout;
		ImageView dislike_selected;
		ImageView like_selected;
		ImageView like;
		ImageView dislike;
		ImageView rowClick;
	}


    public void disableLikeUnlike()
    {
        selectedIndex=-1;
        selected_like_dislike = -1;
//        clear_like_dislike = -1;


//        holder.rowClick.setVisibility(View.GONE);
//        holder.LikeUnlike.setVisibility(View.VISIBLE);
//        holder.dislike_selected.setVisibility(View.VISIBLE);
//        holder.like_selected.setVisibility(View.VISIBLE);
        //rl_relative.setBackgroundColor(Color.parseColor("#73D66C"));
        notifyDataSetChanged();

//        clear_like_dislike = 99999;
//        selected_like_dislike = -1;
//        notifyDataSetChanged();
    }

    /**
     * Sets the background color for the current playing song*/
    public void layoutBackgroundChange(String songKey){
        Songkey = songKey;
        notifyDataSetChanged();
    }

    public void updateJSONforLikeUnlike(JSONObject passedJSON){
        parse_like_dislike = passedJSON;
        notifyDataSetChanged();
    }

}
//rl_relative.setBackgroundColor(Color.TRANSPARENT);
//rl_relative.setBackgroundColor(Color.parseColor("#73D66C"));