package com.neurlabs.muko.adapters;

import java.util.ArrayList;
import java.util.List;

import com.neurlabs.muko.GenreListActivity;
import com.neurlabs.muko.R;
import com.neurlabs.muko.models.GenreListModel;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GenreListAdapter extends BaseAdapter
{
    //genrelist , genrearraylist
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    private List<GenreListModel> genrelist = null;
    private ArrayList<GenreListModel> genrearraylist;
    public int[] checked;
    ArrayList<Integer> checkedarray;
    ArrayList<Integer> testarray;

    private int bckgrndsetter;
    public int images[];
    boolean[] itemChecked;

    public GenreListAdapter(Context context,List<GenreListModel> genrelist, int[] images, ArrayList<Integer> checked_checkbox)
    {
        this.context = context;
        this.genrelist = genrelist;
        this.images = images;
        inflater = LayoutInflater.from(context);
        this.genrearraylist = new ArrayList<GenreListModel>();
        this.genrearraylist.addAll(genrelist);
        bckgrndsetter=1;
        this.checkedarray = checked_checkbox;
        itemChecked = new boolean[genrelist.size()];
        //this.checkedarray.addAll(checked_checkbox);

    }

    public void clearChecked()
    {
        for(int i=0;i<checkedarray.size();i++)
        {
            checkedarray.set(i, 0);
        }
        notifyDataSetChanged();
    }

    public class ViewHolder
    {
        TextView Genre;
        RelativeLayout Relativelayout;
        CheckBox GenreCheckBox;
        TextView test;
    }

    @Override
    public int getCount()
    {
        return genrelist.size();
    }

    @Override
    public Object getItem(int position)
    {
        return genrelist.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;
        Typeface typeface_muko = Typeface.createFromAsset(context.getAssets(),"futura-condensed-medium.ttf");
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.genre_listview_row, null);

            holder = new ViewHolder();

            holder.Genre = (TextView) convertView.findViewById(R.id.genre_listview_row_textView_genre);
            holder.Relativelayout = (RelativeLayout) convertView.findViewById(R.id.genre_listview_row_relativeLayout);
            holder.GenreCheckBox = (CheckBox) convertView.findViewById(R.id.genre_listview_row_checkBox);
            holder.test = (TextView) convertView.findViewById(R.id.genre_listview_row_textView_test);

            holder.Genre.setTypeface(typeface_muko);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        // Set the results into TextViews
        holder.Genre.setText(genrelist.get(position).getGenre());
        holder.test.setText(String.valueOf(checkedarray.get(position)));
        holder.Relativelayout.setBackgroundResource(images[position]);

        holder.GenreCheckBox.setChecked(false);

//		if (itemChecked[position])
//			holder.GenreCheckBox.setChecked(true);
//		else
//		   holder.GenreCheckBox.setChecked(false);

        if(checkedarray.get(position) == 1)
        {
            holder.GenreCheckBox.setChecked(true);
        }
        else
        {
            holder.GenreCheckBox.setChecked(false);
        }






        int siz1 = checkedarray.size();
        int siz2 = checkedarray.size();

        holder.GenreCheckBox.setClickable(false);

//		holder.GenreCheckBox.setOnClickListener(new OnClickListener()
//		{
//			@Override
//			public void onClick(View v)
//			{
//				// TODO Auto-generated method stub
//				if (holder.GenreCheckBox.isChecked())
//				{
//					ArrayList<Integer> testarray;
//					
//					checkedarray.set(position, 1);
//					testarray = checkedarray;
//					
//					((GenreListActivity)context).buttonsState();
//					notifyDataSetChanged();
//				}
//				else
//				{
//					ArrayList<Integer> testarray;
//					
//					checkedarray.set(position, 0);
//					
//					testarray = checkedarray;
//					notifyDataSetChanged();
//				}
//				
//				if(checkedarray.contains(1))
//				{
//					((GenreListActivity)context).buttonsState();
//				}
//				else
//				{
//					((GenreListActivity)context).buttonsStateSkipNext();
//				}    
//		   }
//		 });

        // Listen for ListView Item Click
        convertView.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {
                if(checkedarray.get(position) == 0)
                {
                    holder.GenreCheckBox.setChecked(true);
                    ArrayList<Integer> testarray;

                    checkedarray.set(position, 1);
                    testarray = checkedarray;

                    String Genre = genrearraylist.get(position).getGenre();
                    ((GenreListActivity)context).addGenre(Genre);

                    notifyDataSetChanged();
                }
                else
                {
                    holder.GenreCheckBox.setChecked(false);

                    ArrayList<Integer> testarray;
                    int pos = position;

                    checkedarray.set(position, 0);
                    testarray = checkedarray;

                    String Genre = genrearraylist.get(position).getGenre();
                    ((GenreListActivity)context).removeArtist(Genre);
                    notifyDataSetChanged();
                }

                if(checkedarray.contains(1))
                {
                    ((GenreListActivity)context).buttonsState();
                }
                else
                {
                    ((GenreListActivity)context).buttonsStateSkipNext();
                }
            }
        });
        return convertView;
    }




}





