package com.neurlabs.muko.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neurlabs.muko.ArtistListActivity;
import com.neurlabs.muko.R;
import com.neurlabs.muko.models.ArtistListModel;
import com.neurlabs.muko.utilis.DataTransferInterface;

public class ArtistListAdapter extends BaseAdapter
{
    //Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<ArtistListModel> artistlist = null;
    private ArrayList<ArtistListModel> arraylist;
    protected int count;
    boolean[] artistChecked;
    private int bckgrndsetter;
    DataTransferInterface dtInterface;
    int pos;
    ArrayList<Integer> checkedarray;
    boolean[] itemChecked;

    public ArtistListAdapter(Context context, List<ArtistListModel> artistlist, ArrayList<Integer> checked_checkbox)
    {
        mContext = context;
        this.artistlist = artistlist;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<ArtistListModel>();
        this.arraylist.addAll(artistlist);
        //this.dtInterface = dtInterface;
        artistChecked = new boolean[artistlist.size()];
        bckgrndsetter = 1;
        this.checkedarray = checked_checkbox;
    }

    public void clearChecked()
    {
        for(int i=0;i<checkedarray.size();i++)
        {
            checkedarray.set(i, 0);
        }
        notifyDataSetChanged();
    }

    public class ViewHolder
    {
        TextView artist;
        CheckBox artistCheckBox;
        RelativeLayout background;
        TextView test;
    }

    @Override
    public int getCount()
    {
        return artistlist.size();
    }

    @Override
    public ArtistListModel getItem(int position)
    {
        return artistlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent)
    {
        final ViewHolder holder;
        pos=position;
        Typeface typeface_muko = Typeface.createFromAsset(mContext.getAssets(),"futura-condensed-medium.ttf");
        if (view == null)
        {

            view = inflater.inflate(R.layout.artist_listview_row, null);
            // Locate the TextView in listview_item.xml
            holder = new ViewHolder();
            holder.background = (RelativeLayout) view.findViewById(R.id.artist_listview_row_relativeLayout);
            holder.artist = (TextView) view.findViewById(R.id.artist_listview_row_textview_artist);
            holder.artistCheckBox = (CheckBox) view.findViewById(R.id.artist_listview_row_checkBox);
            holder.test = (TextView) view.findViewById(R.id.artist_listview_row_textView_test);

            holder.artist.setTypeface(typeface_muko);

            view.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) view.getTag();
        }

//        if (position % 2 == 0) {
//            view.setBackgroundResource(R.drawable.bg_0);
//        } else {
//            view.setBackgroundResource(R.drawable.bg_1);
//        }

        switch((position%5)){

            case 0:
                //view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.a_rock_icon));
                holder.background.setBackgroundDrawable( mContext.getResources().getDrawable(R.drawable.bg_0) );
                break;
            case 1:
                //view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.b_indie_icon));
                holder.background.setBackgroundDrawable( mContext.getResources().getDrawable(R.drawable.bg_1) );
                break;
            case 2:
                //view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.c_electronic_icon));
                holder.background.setBackgroundDrawable( mContext.getResources().getDrawable(R.drawable.bg_2) );
                break;
            case 3:
                //view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.d_pop_icon));
                holder.background.setBackgroundDrawable( mContext.getResources().getDrawable(R.drawable.bg_3) );
                break;
            case 4:
                //view.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.h_jazz_icon));
                holder.background.setBackgroundDrawable( mContext.getResources().getDrawable(R.drawable.bg_4) );
                break;
        }
        // Set the results into TextView
        holder.artist.setText(artistlist.get(position).getArtistName());
        holder.test.setText(String.valueOf(checkedarray.get(position)));

        holder.artistCheckBox.setChecked(false);

        if(checkedarray.get(position) == 1)
        {
            holder.artistCheckBox.setChecked(true);
        }
        else
        {
            holder.artistCheckBox.setChecked(false);
        }

//		if (artistChecked[position])
//			holder.artistCheckBox.setChecked(true);
//		else
//		   holder.artistCheckBox.setChecked(false);

        holder.artistCheckBox.setClickable(false);
//		holder.artistCheckBox.setOnClickListener(new OnClickListener()
//		{
//			@Override
//			public void onClick(View v)
//			{
//				// TODO Auto-generated method stub
//				if (holder.artistCheckBox.isChecked())
//				{
//					ArrayList<Integer> testarray;
//					itemChecked[position] = true;
//					
//					checkedarray.set(position, 1);
//					//checkedarray.clear();
//					testarray = checkedarray;
//
//					//genrelist.get(position).setGenreChecked(1);;
//					((ArtistListActivity)mContext).buttonsState();
//					notifyDataSetChanged();
//				}
//				else
//				{
//					ArrayList<Integer> testarray;
//					int pos = position;
//					itemChecked[position] = false;
//					
//					checkedarray.set(position, 0);
//					
//					testarray = checkedarray;
//					//genrelist.get(position).setGenreChecked(0);
//					notifyDataSetChanged();
//				}
//				
//				if(checkedarray.contains(1))
//				{
//					((ArtistListActivity)mContext).buttonsState();
//				}
//				else
//				{
//					((ArtistListActivity)mContext).buttonsStateSkipNext();
//				}
//
//			 
//			    
//		   }
//		 });


        // Listen for ListView Item Click
        view.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {
                if(checkedarray.get(position) == 0)
                {
                    holder.artistCheckBox.setChecked(true);
                    ArrayList<Integer> testarray;

                    checkedarray.set(position, 1);
                    testarray = checkedarray;

                    String Artist = arraylist.get(position).getArtistName();
                    ((ArtistListActivity)mContext).addArtist(Artist);

                    ((ArtistListActivity)mContext).buttonsState();
                    notifyDataSetChanged();
                }
                else
                {
                    holder.artistCheckBox.setChecked(false);

                    ArrayList<Integer> testarray;
                    int pos = position;

                    checkedarray.set(position, 0);
                    testarray = checkedarray;

                    String Artist = arraylist.get(position).getArtistName();
                    ((ArtistListActivity)mContext).removeArtist(Artist);
                    notifyDataSetChanged();
                }

                if(checkedarray.contains(1))
                {
                    ((ArtistListActivity)mContext).buttonsState();
                }
                else
                {
                    ((ArtistListActivity)mContext).buttonsStateSkipNext();
                }
            }
        });

        return view;
    }



}