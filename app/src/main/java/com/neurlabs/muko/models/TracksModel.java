package com.neurlabs.muko.models;

public class TracksModel {
	public String key;
	public String trackName;
	public String artistName;
//	public String albumName;
//	public String albumArt;
	public int mLikedOrUnliked;

	public TracksModel(String k, String name, String artist, int LikeUnlikeInfo/*, String album, String uri*/) {
		key = k;
		trackName = name;
		artistName = artist;
        mLikedOrUnliked = LikeUnlikeInfo;
//		albumName = album;
//		albumArt = uri;
	}
	
	public String getID(){
		return key;
	}
	
	public String getSongName(){
		return trackName;
	}
	
	public String getArtistName(){
		return artistName;
	}

	public int getLikeUnlikeInfo() {
		return mLikedOrUnliked;
	}

    public void setLikeUnlikeInfo(int LikeUnlikeInfo){
        mLikedOrUnliked = LikeUnlikeInfo;
    }
	
	
}
