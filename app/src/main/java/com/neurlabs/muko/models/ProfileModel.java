package com.neurlabs.muko.models;

public class ProfileModel {
	
	private String ProfileName;
	private int ProfileImage;
	private int CircleIcon;


	public ProfileModel(String ProfileName, int CircleIcon)
	{
		this.ProfileName = ProfileName;
		this.CircleIcon = CircleIcon;
	}
	
	public ProfileModel(String ProfileName, int CircleIcon, int ProfileImage)
	{
		this.ProfileName = ProfileName;
		this.CircleIcon = CircleIcon;
		this.ProfileImage = ProfileImage;
	}
	
	public ProfileModel(String ProfileName)
	{
		this.ProfileName = ProfileName;
	}


	public int getCircleIcon() {
		return CircleIcon;
	}

	public void setCircleIcon(int circleIcon) {
		CircleIcon = circleIcon;
	}

	public String getProfileName() {
		return ProfileName;
	}


	public void setProfileName(String profileName) {
		ProfileName = profileName;
	}


	public int getProfileImage() {
		return ProfileImage;
	}


	public void setProfileImage(int profileImage) {
		ProfileImage = profileImage;
	}

	
}
