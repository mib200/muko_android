package com.neurlabs.muko.models;

public class ArtistListModel
{
	public String key;
	public String trackName;
	public String artistName;
	
	
//	public String albumName;
//	public String albumArt;

//	public ArtistListModel(String k, String name, String artist/*, String album, String uri*/) {
//		key = k;
//		trackName = name;
//		artistName = artist;
////		albumName = album;
////		albumArt = uri;
//	}
	
	public String getID(){
		return key;
	}
	
	public String getSongName(){
		return trackName;
	}
	
	public String getArtistName(){
		return artistName;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
}
