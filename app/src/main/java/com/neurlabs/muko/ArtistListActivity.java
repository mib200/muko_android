package com.neurlabs.muko;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.neurlabs.muko.adapters.ArtistListAdapter;
import com.neurlabs.muko.models.ArtistListModel;
import com.neurlabs.muko.utilis.InternetStatus;
import com.neurlabs.muko.utilis.TinyDB;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class ArtistListActivity extends ActionBarActivity
{
    // Declare Variables
    ListView artistlistview;
    List<ParseObject> artistObject;
    ProgressDialog artistProgressDialog;
    ArtistListAdapter artistAdapter;
    private ArrayList<ArtistListModel> artistlist = new ArrayList<ArtistListModel>();
    private ArrayList<String> artistlistCheckbox = new ArrayList<String>();
    ArrayList<Integer> checked_checkbox = new ArrayList<Integer>();
    // Set the limit of objects to show
    private int limit = 1000;
    private int limitplus = 1000;
    String newTextparse;
    TextView tv_artistSelected;
    String ArtistGlobal = "";
    int parse_fetch_size;

    InternetStatus internetStatus = new InternetStatus();
    Boolean netStatus;

    Button btn_next;
    EditText et_searchQuery;

    Button btn_clear;
    Button btn_save;

    private View footer_artist;
    ProgressBar progressBar;
    Button btn_loadArtist;
    ImageView iv_back;
    SharedPreferences settings;
    private static String userKey = null;
    SharedPreferences nav_check;

    JSONArray parseJsonChecked_status = new JSONArray();
    private ArrayList<String> parseArrayChecked_status = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
//		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_artist_list);

        netStatus = internetStatus.isOnline(getApplicationContext());
        Typeface typeface_muko = Typeface.createFromAsset(getAssets(),"futura-condensed-medium.ttf");
        nav_check = getApplicationContext().getSharedPreferences("Muko_nav_check", MODE_PRIVATE);

        tv_artistSelected = (TextView) findViewById(R.id.activity_artist_list_textView_selected);
        btn_next = (Button) findViewById(R.id.activity_artist_list_button_next);
        btn_clear = (Button) findViewById(R.id.activity_artist_list_button_clear);
        btn_save = (Button) findViewById(R.id.activity_artist_list_button_save);

        iv_back = (ImageView) findViewById(R.id.activity_artist_list_imageview_back);
        et_searchQuery = (EditText) findViewById(R.id.activity_artist_list_edittext_search);
        et_searchQuery.setCursorVisible(false);
        et_searchQuery.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        et_searchQuery.setHintTextColor(getResources().getColor(R.color.muko_white));

        tv_artistSelected.setTypeface(typeface_muko);
        btn_next.setTypeface(typeface_muko);
        btn_clear.setTypeface(typeface_muko);
        btn_save.setTypeface(typeface_muko);
        et_searchQuery.setTypeface(typeface_muko);
//        btn_skip.setOnClickListener(new OnClickListener()
//        {
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent homeIntent = new Intent(ArtistListActivity.this,HomeActivity.class);
//				startActivity(homeIntent);
//			}
//		});

        btn_next.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {

                netStatus = internetStatus.isOnline(getApplicationContext());
                if(netStatus)
                {
                    artistProgressDialog = new ProgressDialog(ArtistListActivity.this);
                    // Set progressdialog title
                    artistProgressDialog.setTitle("Muko");
                    // Set progressdialog message
                    artistProgressDialog.setMessage("Loading...");
                    artistProgressDialog.setIndeterminate(false);
                    artistProgressDialog.setCancelable(false);
                    artistProgressDialog.setCanceledOnTouchOutside(false);
                    // Show progressdialog
                    artistProgressDialog.show();


                    TinyDB tinydb = new TinyDB(ArtistListActivity.this);
                    tinydb.putList("artistList", artistlistCheckbox);


                    SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                    String customerObjectId = muko_pref.getString("customerObjectId", null);          // getting String
                    final String userKey = muko_pref.getString("userKey", null);
                    final String objectid = muko_pref.getString("objectid", null);

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");

                    // Retrieve the object by id
                    query.getInBackground(objectid, new GetCallback<ParseObject>() {
                        public void done(ParseObject gameScore, ParseException e) {
                            if (e == null) {
                                // Now let's update it with some new data. In this case, only cheatMode and score
                                // will get sent to the Parse Cloud. playerName hasn't changed.
                                gameScore.put("artists_selected", artistlistCheckbox);
//                            ReusableComponents.getInstance().artistList_reusable = artistlistCheckbox;
                                gameScore.saveInBackground(new SaveCallback() {

                                    @Override
                                    public void done(ParseException e) {
                                        // TODO Auto-generated method stub
                                        if(e==null)
                                        {
                                            //Toast.makeText(getApplicationContext(), "genres_selected Has Been Updated!",Toast.LENGTH_SHORT).show();
                                            artistProgressDialog.dismiss();
                                            Intent artistIntent = new Intent(ArtistListActivity.this,HomeActivity.class);
                                            startActivity(artistIntent);
                                            finish();
                                        }
                                        else
                                        {
                                            artistProgressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Exception/n"+e, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                else
                {
                    artistProgressDialog.dismiss();
                    alert("Need internet connection");
                }

            }
        });

        btn_clear.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(netStatus)
                {
                    artistAdapter.clearChecked();

                    clearall();

                    netStatus = internetStatus.isOnline(getApplicationContext());
                    btn_clear.setBackgroundColor(Color.GRAY);
                    if(userKey == null || userKey.isEmpty())
                    {
                        btn_next.setVisibility(View.INVISIBLE);
                        btn_save.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        btn_next.setVisibility(View.VISIBLE);
                        btn_save.setVisibility(View.INVISIBLE);
                    }
                }
                else
                {
                    alert("Need Internet Connection");
                }


            }
        });

        btn_save.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                // TODO Save selected Artist to the Parse

                netStatus = internetStatus.isOnline(getApplicationContext());
                if(netStatus)
                {
                    artistProgressDialog = new ProgressDialog(ArtistListActivity.this);
                    // Set progressdialog title
                    artistProgressDialog.setTitle("Muko");
                    // Set progressdialog message
                    artistProgressDialog.setMessage("Loading...");
                    artistProgressDialog.setIndeterminate(false);
                    artistProgressDialog.setCancelable(false);
                    artistProgressDialog.setCanceledOnTouchOutside(false);
                    // Show progressdialog
                    artistProgressDialog.show();

                    TinyDB tinydb = new TinyDB(ArtistListActivity.this);
                    tinydb.putList("artistList", artistlistCheckbox);

                    SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                    String customerObjectId = muko_pref.getString("customerObjectId", null);          // getting String
                    final String userKey = muko_pref.getString("userKey", null);
                    final String objectid = muko_pref.getString("objectid", null);

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");

                    // Retrieve the object by id
                    query.getInBackground(objectid, new GetCallback<ParseObject>() {
                        public void done(ParseObject gameScore, ParseException e) {
                            if (e == null) {
                                // Now let's update it with some new data. In this case, only cheatMode and score
                                // will get sent to the Parse Cloud. playerName hasn't changed.
                                gameScore.put("artists_selected", artistlistCheckbox);
//                            ReusableComponents.getInstance().artistList_reusable = artistlistCheckbox;
                                gameScore.saveInBackground(new SaveCallback() {

                                    @Override
                                    public void done(ParseException e) {
                                        // TODO Auto-generated method stub
                                        if(e==null)
                                        {
                                            //Toast.makeText(getApplicationContext(), "genres_selected Has Been Updated!",Toast.LENGTH_SHORT).show();
                                            artistProgressDialog.dismiss();
                                            dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                                            dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
                                        }
                                        else
                                        {
                                            artistProgressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Exception/n"+e, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                else
                {
                    artistProgressDialog.dismiss();
                    alert("Need internet connection");
                }


            }
        });

        et_searchQuery.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
//				et_searchQuery.setVisibility(View.VISIBLE);
//				et_searchQuery.setEnabled(true);
                et_searchQuery.setCursorVisible(true);
            }
        });






        et_searchQuery.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                //Toast.makeText(getApplicationContext(), "onTextChanged - "+s, Toast.LENGTH_SHORT).show();
                newTextparse = s.toString();
                String text = newTextparse.toLowerCase(Locale.getDefault());
                String test = newTextparse;
                artistlistview.removeFooterView(footer_artist);

                if(newTextparse == "" || newTextparse == null || newTextparse.isEmpty())
                {
                    artistObject.clear();
                    artistlist.clear();
                    checked_checkbox.clear();
                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            // CODE HERE
                            new RemoteDataTask().execute();
                        }
                    }, 2000);

                    InputMethodManager inputMethodManager = (InputMethodManager)  getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
                else
                {
                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            // CODE HERE
                            netStatus = internetStatus.isOnline(getApplicationContext());
                            if(netStatus)
                            {
                                new SearchDataTask().execute();
                            }
                            else
                            {
                                alert("Need internet connection");
                            }

                        }
                    }, 2000);
                }


                //artistlistview.removeFooterView(footer_artist);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
                //Toast.makeText(getApplicationContext(), "beforeTextChanged - "+s, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                //Toast.makeText(getApplicationContext(), "afterTextChanged - " +s, Toast.LENGTH_SHORT).show();
            }
        });

        iv_back.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Checking for soft key is still present in this screen
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(iv_back.getWindowToken(), 0);

                dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            }
        });


        btn_save.setVisibility(View.INVISIBLE);

        userKey = nav_check.getString("userKey", null);

        if(userKey == null || userKey.isEmpty())
        {
            iv_back.setVisibility(View.VISIBLE);
            btn_save.setVisibility(View.VISIBLE);
            btn_save.setEnabled(true);
            btn_next.setVisibility(View.INVISIBLE);

        }
        else
        {
            iv_back.setVisibility(View.INVISIBLE);
            btn_save.setVisibility(View.INVISIBLE);
        }

        tv_artistSelected.setText("Pick more artist, or go next");

        if(netStatus)
        {
            getCheckedArtist();
            btn_clear.setEnabled(true);
        }
        else
        {
            btn_save.setEnabled(false);
            btn_clear.setEnabled(false);
            alert("Need Internet Connection");
        }

        //new RemoteDataTask().execute();
    }

    private class RemoteDataTask extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            // Create a progressdialog
            artistProgressDialog = new ProgressDialog(ArtistListActivity.this);
            // Set progressdialog title
            artistProgressDialog.setTitle("Muko");
            // Set progressdialog message
            artistProgressDialog.setMessage("Loading...");
            artistProgressDialog.setIndeterminate(false);
            artistProgressDialog.setCancelable(false);
            artistProgressDialog.setCanceledOnTouchOutside(false);
            // Show progressdialog
            artistProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            //checked_checkbox = new ArrayList<Integer>();
            try
            {
                // Locate the class table named "TestLimit" in Parse.com
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Artists");
                query.orderByAscending("createdAt");
                // Set the limit of objects to show
                query.setLimit(limit);
                artistObject = query.find();
                for (ParseObject artistsObject : artistObject)
                {
                    parse_fetch_size = artistObject.size();
                    List<ParseObject> test;
                    List<ParseObject> test1;
                    test = artistObject;
                    test1 = test;

                    ArtistListModel artistModel = new ArtistListModel();
                    artistModel.setArtistName((String) artistsObject.get("artistName"));
                    String artistcheck = artistsObject.getString("artistName");
                    artistlist.add(artistModel);

                    ArrayList<String> parseArrayChecked_status1 = new ArrayList<String>();
                    parseArrayChecked_status1=parseArrayChecked_status;

                    if(parseArrayChecked_status.contains(artistcheck))
                    {
                        checked_checkbox.add(1);
                    }
                    else
                    {
                        checked_checkbox.add(0);
                    }
                }
            }
            catch (ParseException e)
            {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            // Locate the ListView in listview.xml
            artistlistview = (ListView) findViewById(R.id.activity_artist_list_listview);

            // LoadMore layout inflate
            loadMoreLayout();
            btn_loadArtist = (Button) footer_artist.findViewById(R.id.artist_list_load_footer_layout_button);
            progressBar = (ProgressBar) footer_artist.findViewById(R.id.artist_list_load_footer_layout_progressBar);
            progressBar.setVisibility(View.INVISIBLE);


            int footer = artistlistview.getFooterViewsCount();

            if(footer == 0)
            {

            }
            else
            {
                artistlistview.removeFooterView(footer_artist);
            }
            if(footer==0)
            {
                if(parse_fetch_size > 998)
                {
                    // Adding Load More button to lisview at bottom
                    artistlistview.addFooterView(footer_artist);
                }
            }
            else
            {

            }

            // Pass the results into ListViewAdapter.java
            artistAdapter = new ArtistListAdapter(ArtistListActivity.this, artistlist,checked_checkbox);
            // Binds the Adapter to the ListView

            artistlistview.setAdapter(artistAdapter);

            /**
             * Listening to Load More button click event
             * */
            btn_loadArtist.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // Starting a new async task
                    progressBar.setVisibility(View.VISIBLE);
                    new LoadMoreDataTask().execute();
                }
            });


            // Close the progressdialog
            artistProgressDialog.dismiss();
            // Create an OnScrollListener
            artistlistview.setOnScrollListener(new OnScrollListener()
            {
                @Override
                public void onScrollStateChanged(AbsListView view,int scrollState)
                {
                    // TODO Auto-generated method stub
                    int threshold = 1;
                    int count = artistlistview.getCount();

                    if (scrollState == SCROLL_STATE_IDLE)
                    {
                        if (artistlistview.getLastVisiblePosition() >= count - threshold)
                        {
                            // Execute LoadMoreDataTask AsyncTask
                            //new LoadMoreDataTask().execute();
                        }
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
                {
                    // TODO Auto-generated method stub
                }

            });
        }

        private class LoadMoreDataTask extends AsyncTask<Void, Void, Void>
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                // Create a progressdialog if necessary
            }

            @Override
            protected Void doInBackground(Void... params)
            {
                //checked_checkbox = new ArrayList<Integer>();
                try
                {
                    // Locate the class table named "TestLimit" in Parse.com
                    ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Artists");
                    query.orderByAscending("createdAt");
                    // Add 1000 results to the default limit
                    query.setLimit(limit += limitplus);
                    query.setSkip(limit);
                    artistObject = query.find();
                    for (ParseObject artistsObject : artistObject)
                    {
                        ArtistListModel artistModel = new ArtistListModel();
                        artistModel.setArtistName((String) artistsObject.get("artistName"));
                        artistlist.add(artistModel);

                        String artistcheck = artistsObject.getString("artistName");

                        //artistlist.add(artistModel);
                        if(parseArrayChecked_status.contains(artistcheck))
                        {
                            checked_checkbox.add(1);
                        }
                        else
                        {
                            checked_checkbox.add(0);
                        }
                    }
                }
                catch (ParseException e)
                {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                // Locate artistlistview last item
                int position = artistlistview.getLastVisiblePosition();
                // Pass the results into ListViewAdapter.java
                artistAdapter = new ArtistListAdapter(ArtistListActivity.this, artistlist,checked_checkbox);
                // Binds the Adapter to the ListView
                artistlistview.setAdapter(artistAdapter);
                // Show the latest retrived results on the top
                artistlistview.setSelectionFromTop(position, 0);
                // Close the progressdialog
//				artistProgressDialog.dismiss();
                progressBar.setVisibility(View.INVISIBLE);
            }
        }

    }


    /**
     *
     * */
    private class SearchDataTask extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            // Create a progressdialog
            artistProgressDialog = new ProgressDialog(ArtistListActivity.this);
            // Set progressdialog title
            artistProgressDialog.setTitle("Muko");
            // Set progressdialog message
//			artistProgressDialog.setMessage("Loading...");
//			artistProgressDialog.setIndeterminate(false);
//			artistProgressDialog.setCancelable(false);
            artistProgressDialog.setCanceledOnTouchOutside(false);
//			// Show progressdialog
//			artistProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            String test= newTextparse;
            String test1 = test;
            //if(newTextparse != "" || newTextparse != null || !newTextparse.isEmpty())
            if(newTextparse == "" || newTextparse == null || newTextparse.isEmpty())
            {


//				try
//				{
//				
//				ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Artists");
//				query.orderByAscending("createdAt");
//				query.setLimit(limit);
//				artistObject = query.find();
//				
//				checked_checkbox.clear();
//				for (ParseObject artistsObject : artistObject)
//				{
//					parse_fetch_size = artistObject.size();
//					ArtistListModel artistModel = new ArtistListModel();
//					artistModel.setArtistName((String) artistsObject.get("artistName"));
//					artistlist.add(artistModel);
//					
//					String artistcheck = artistsObject.getString("artistName");
//					
//					//artistlist.add(artistModel);
//					if(parseArrayChecked_status.contains(artistcheck))
//					{
//						checked_checkbox.add(1);
//					}
//					else
//					{
//						checked_checkbox.add(0);
//					}
//				}
//				loadingParsedata();
//				}
//				catch (ParseException e)
//				{
//					Log.e("Error", e.getMessage());
//					e.printStackTrace();
//				}

            }
            else
            {
                ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Artists");
                query1.whereStartsWith("artistName", newTextparse);

                ParseQuery<ParseObject> query2 = ParseQuery.getQuery("Artists");
                query2.whereStartsWith("artistName", newTextparse.toUpperCase());

                ParseQuery<ParseObject> query3 = ParseQuery.getQuery("Artists");
//				query3.whereStartsWith("artistName", newTextparse.toLowerCase());
                query3.whereMatches("artistName", "("+newTextparse+")", "i");

//				query3.whereContains("artistName", newTextparse);
//				query3.whereMatches("artistName", "("+newTextparse+")", "i");

                List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
                queries.add(query1);
                queries.add(query2);
                queries.add(query3);
                Log.v("Parse query", "Sending query to parse");

                ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
                mainQuery.findInBackground(new FindCallback<ParseObject>()
                {
                    public void done(List<ParseObject> results, ParseException e)
                    {
                        // results has the list of players that win a lot or haven't won much.
                        if(e == null)
                        {
                            Log.v("Parse query", "Got result list from parse");
                            //Clears the previous data in the array list
                            artistlist.clear();
                            checked_checkbox.clear();
                            for (ParseObject artistsObject : results)
                            {
                                parse_fetch_size = results.size();
                                ArtistListModel artistModel = new ArtistListModel();
                                artistModel.setArtistName((String) artistsObject.get("artistName"));
                                artistlist.add(artistModel);

                                String artistcheck = artistsObject.getString("artistName");

                                //artistlist.add(artistModel);
                                if(parseArrayChecked_status.contains(artistcheck))
                                {
                                    checked_checkbox.add(1);
                                }
                                else
                                {
                                    checked_checkbox.add(0);
                                }
                            }

                            loadingParsedata();
                        }
                        else
                        {
                            //TODO ERROR MESSAGE
                        }
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
//			// Locate the ListView in listview.xml
//			artistlistview = (ListView) findViewById(R.id.activity_artist_list_listview);
//			
//			// LoadMore button
//			loadMoreLayout();
//			btn_loadArtist = (Button) footer_artist.findViewById(R.id.artist_list_load_footer_layout_button);
//			progressBar = (ProgressBar) footer_artist.findViewById(R.id.artist_list_load_footer_layout_progressBar);
//			progressBar.setVisibility(View.INVISIBLE);
//			
//			int footer = artistlistview.getFooterViewsCount();
//			if(footer == 0)
//			{
//				
//			}
//			else
//			{
//				for(int f=0;f<footer;f++)
//				{
//					artistlistview.removeFooterView(footer_artist);
//				}
//				
//			}
//			
//			if(footer==0)
//			{
//				int test = parse_fetch_size;
//				if(parse_fetch_size > 998)
//				{
//					// Adding Load More button to lisview at bottom
//					//artistlistview.addFooterView(footer_artist);
//				}
//			}
//			else
//			{
//				
//			}
//			
//			// Pass the results into ListViewAdapter.java
//			artistAdapter = new ArtistListAdapter(ArtistListActivity.this, artistlist,checked_checkbox);
//			// Binds the Adapter to the ListView
//			artistlistview.setAdapter(artistAdapter);
//			artistAdapter.notifyDataSetChanged();
//			Log.v("Parse query", "notifyDataSetChanged called");
//			
//			
//			/**
//			 * Listening to Load More button click event
//			 * */
//			btn_loadArtist.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					// Starting a new async task
//					progressBar.setVisibility(View.VISIBLE);
//					new LoadMoreDataTask_search().execute();
//				}
//			});
//			
//			
//			// Close the progressdialog
//			artistProgressDialog.dismiss();
            // Create an OnScrollListener
            artistlistview.setOnScrollListener(new OnScrollListener()
            {
                @Override
                public void onScrollStateChanged(AbsListView view,int scrollState)
                {
                    // TODO Auto-generated method stub
                    int threshold = 1;
                    int count = artistlistview.getCount();

                    if (scrollState == SCROLL_STATE_IDLE)
                    {
                        if (artistlistview.getLastVisiblePosition() >= count - threshold)
                        {
                            // Execute LoadMoreDataTask AsyncTask
                            //new LoadMoreDataTask().execute();
                        }
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
                {
                    // TODO Auto-generated method stub

                }

            });
        }

        public void loadingParsedata()
        {
            artistlistview = (ListView) findViewById(R.id.activity_artist_list_listview);

            // LoadMore button
            loadMoreLayout();

            btn_loadArtist = (Button) footer_artist.findViewById(R.id.artist_list_load_footer_layout_button);
            progressBar = (ProgressBar) footer_artist.findViewById(R.id.artist_list_load_footer_layout_progressBar);
            progressBar.setVisibility(View.INVISIBLE);

            int footer = artistlistview.getFooterViewsCount();
            if(footer == 0)
            {

            }
            else
            {
                for(int f=0;f<footer;f++)
                {
                    artistlistview.removeFooterView(footer_artist);
                    //artistlistview.removeAllViews();
                }

            }
            int footer1 = artistlistview.getFooterViewsCount();
            if(footer1==0)
            {
                int test = parse_fetch_size;
                if(parse_fetch_size > 998)
                {
                    // Adding Load More button to lisview at bottom
                    artistlistview.addFooterView(footer_artist);
                }
            }
            else
            {

            }

            // Pass the results into ListViewAdapter.java
            artistAdapter = new ArtistListAdapter(ArtistListActivity.this, artistlist,checked_checkbox);
            // Binds the Adapter to the ListView
            artistlistview.setAdapter(artistAdapter);
            artistAdapter.notifyDataSetChanged();
            Log.v("Parse query", "notifyDataSetChanged called");


            /**
             * Listening to Load More button click event
             * */
            btn_loadArtist.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // Starting a new async task
                    progressBar.setVisibility(View.VISIBLE);
                    new LoadMoreDataTask_search().execute();
                }
            });


            // Close the progressdialog
//			artistProgressDialog.dismiss();











            /******************************************************/

//			artistlistview = (ListView) findViewById(R.id.activity_artist_list_listview);
//			
//			// LoadMore button
//			loadMoreLayout();
//			btn_loadArtist = (Button) footer_artist.findViewById(R.id.artist_list_load_footer_layout_button);
//			progressBar = (ProgressBar) footer_artist.findViewById(R.id.artist_list_load_footer_layout_progressBar);
//			progressBar.setVisibility(View.INVISIBLE);
//			
//			
//			// Adding Load More button to lisview at bottom
//			//artistlistview.addFooterView(footer_artist);
//			
//			// Pass the results into ListViewAdapter.java
//			artistAdapter = new ArtistListAdapter(ArtistListActivity.this, artistlist,checked_checkbox);
//			// Binds the Adapter to the ListView
//			artistlistview.setAdapter(artistAdapter);
//			artistAdapter.notifyDataSetChanged();
            Log.v("Parse query", "notifyDataSetChanged called");

        }


        private class LoadMoreDataTask_search extends AsyncTask<Void, Void, Void>
        {
            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                // Create a progressdialog if necessary
            }

            @Override
            protected Void doInBackground(Void... params)
            {
                //checked_checkbox = new ArrayList<Integer>();
                try
                {
                    // Locate the class table named "TestLimit" in Parse.com
                    ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Artists");
                    query.orderByAscending("createdAt");
                    String test = newTextparse;
                    query.whereMatches("artistName","("+newTextparse+")", "i");
                    // Add 1000 results to the default limit
                    query.setLimit(limit += limitplus);
                    query.setSkip(limit);
                    artistObject = query.find();
                    for (ParseObject artistsObject : artistObject)
                    {
                        parse_fetch_size = artistObject.size();
                        ArtistListModel artistModel = new ArtistListModel();
                        artistModel.setArtistName((String) artistsObject.get("artistName"));
                        artistlist.add(artistModel);

                        String artistcheck = artistsObject.getString("artistName");

                        //artistlist.add(artistModel);
                        if(parseArrayChecked_status.contains(artistcheck))
                        {
                            checked_checkbox.add(1);
                        }
                        else
                        {
                            checked_checkbox.add(0);
                        }
                    }
                }
                catch (ParseException e)
                {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result)
            {
                // Locate artistlistview last item
                int position = artistlistview.getLastVisiblePosition();
                // Pass the results into ListViewAdapter.java
                artistAdapter = new ArtistListAdapter(ArtistListActivity.this, artistlist,checked_checkbox);
                // Binds the Adapter to the ListView
                artistlistview.setAdapter(artistAdapter);
                // Show the latest retrived results on the top
                artistlistview.setSelectionFromTop(position, 0);
                // Close the progressdialog
//				artistProgressDialog.dismiss();
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
    }



    public void buttonsState()
    {
        btn_clear.setVisibility(View.VISIBLE);
        btn_next.setVisibility(View.VISIBLE);
        btn_clear.setEnabled(true);
        btn_clear.setBackgroundColor(getResources().getColor(R.color.muko_button_blue));
    }

    public void buttonsStateSkipNext()
    {
        btn_clear.setEnabled(false);
        btn_clear.setBackgroundColor(Color.GRAY);
    }

    public void addArtist(String Artist)
    {
        artistlistCheckbox.add(Artist);

        String test = artistlistCheckbox.toString().replaceAll("\\[", "").replaceAll("\\]","");
        int artistSize = artistlistCheckbox.size();
        tv_artistSelected.setText("You picked "+artistSize+ " artists: "+test);

    }

    /**
     * Clears all the artist in the array.
     * Called when we click on the Clear button
     */
    public void clearall()
    {
        artistlistCheckbox.clear();
        if(userKey == null || userKey.isEmpty())
        {
            tv_artistSelected.setText("Pick more artist, or go back");
        }
        else
        {
            tv_artistSelected.setText("Pick more artist, or go next");
        }


    }

    /**
     * Removes one artist from the array. Accepts Artist parameter from the adapter.
     * Called when we uncheck the selected artist
     */
    public void removeArtist(String Artist)
    {
        artistlistCheckbox.remove(Artist);

        String test = artistlistCheckbox.toString().replaceAll("\\[", "").replaceAll("\\]","");
        int artistSize = artistlistCheckbox.size();
        tv_artistSelected.setText("You picked "+artistSize+ " artists: "+test);
    }

    /**
     * Method for inflating the footer layout at the bottom of the list.
     * Load more button, loads another 1000 artists
     */

    public void loadMoreLayout()
    {
        LayoutInflater inflater = (LayoutInflater) super.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        footer_artist = inflater.inflate(R.layout.artist_list_load_footer_layout, null);
    }

    /**
     * Method to Get Artist from the parse
     * */
    public void getCheckedArtist()
    {
        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
        final String userKey = muko_pref.getString("userKey", null);
        final String objectid = muko_pref.getString("objectid", null);

        artistProgressDialog = new ProgressDialog(ArtistListActivity.this);
        // Set progressdialog title
        artistProgressDialog.setTitle("Muko");
        // Set progressdialog message
        artistProgressDialog.setMessage("Loading...");
        artistProgressDialog.setIndeterminate(false);
        artistProgressDialog.setCancelable(false);
        artistProgressDialog.setCanceledOnTouchOutside(false);
        // Show progressdialog
        artistProgressDialog.show();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");
        query.whereEqualTo("userKey",userKey);
        query.findInBackground(new FindCallback<ParseObject>()
        {
            @Override
            public void done(List<ParseObject> parseList, ParseException e)
            {
                if (e == null)
                {
                    for (ParseObject parse : parseList)
                    {
                        String objectID = parse.getObjectId();
                        String firstname = parse.getString("firstName");

                        SharedPreferences muko_pref = getApplicationContext().getSharedPreferences("Muko", MODE_PRIVATE);
                        Editor editor_parse = muko_pref.edit();
                        editor_parse.putString("objectid", objectID);
                        editor_parse.commit();

                        parseJsonChecked_status = parse.getJSONArray("artists_selected");
                        JSONArray test = new JSONArray();
                        test = parseJsonChecked_status;

                        if (parseJsonChecked_status != null)
                        {
                            for (int i=0;i<parseJsonChecked_status.length();i++)
                            {
                                try
                                {
                                    parseArrayChecked_status.add(parseJsonChecked_status.get(i).toString());
                                    artistlistCheckbox = parseArrayChecked_status;
                                }
                                catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                        String artist = parseArrayChecked_status.toString().replaceAll("\\[", "").replaceAll("\\]","");
                        int artistSize = parseArrayChecked_status.size();
                        if(artistSize<1)
                        {
                            tv_artistSelected.setText("Pick more artist, or go next");
                            btn_clear.setEnabled(false);
                            btn_clear.setBackgroundColor(Color.GRAY);
                        }
                        else
                        {
                            tv_artistSelected.setText("You picked "+artistSize+ " artists : "+artist);
                            btn_clear.setBackgroundColor(getResources().getColor(R.color.muko_button_blue));
                            btn_clear.setEnabled(true);
                        }
                    }
                    // Close the progressdialog
                    artistProgressDialog.dismiss();
                    new RemoteDataTask().execute();
                }
                else
                {

                }
            }
        });
    }

    /**
     * Shows Alert Dialog. Accept string parameter
     * */
    @SuppressWarnings("deprecation")
    public void alert(String message)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(ArtistListActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("MUKO");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //TODO Close the app
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
}