package com.neurlabs.muko.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.neurlabs.muko.ArtistListActivity;
import com.neurlabs.muko.GenreListActivity;
import com.neurlabs.muko.HomeActivity;
import com.neurlabs.muko.R;
import com.neurlabs.muko.SplashScreenActivity;
import com.neurlabs.muko.adapters.ProfileListAdapter;
import com.neurlabs.muko.models.ProfileModel;
import com.neurlabs.muko.utilis.DataThrower;

public class ProfileFragment extends Fragment
{
	ListView profile_listView;
	final Context context = getActivity();
	List<ProfileModel> profileModelList = new ArrayList<ProfileModel>();
	ProfileListAdapter profileAdapter;
	SharedPreferences logout;
	ActionBar actionBar;
	
	public ProfileFragment(){}
	
    @Override
	public void onAttach(Activity a) {
	    super.onAttach(a);
//	    datathrower = (DataThrower) a;
	    actionBar = ((ActionBarActivity)a).getSupportActionBar();
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		
//		ActionBar actionBar = getActivity().getActionBar();
//		actionBar.setDisplayShowTitleEnabled(true);
//		actionBar.setDisplayShowCustomEnabled(true);
//		actionBar.setTitle("PROFILE");
		//actionBar.setBackgroundDrawable(new ColorDrawable("COLOR"));
		
        View rootView = inflater.inflate(R.layout.fragment_profile_layout, container, false);

        Typeface typeface_muko = Typeface.createFromAsset(getActivity().getAssets(),"futura-condensed-medium.ttf");
        
        profile_listView = (ListView)rootView.findViewById(R.id.fragment_profile_layout_listView);
        profileModelList = new ArrayList<ProfileModel>();
        
		initList();
		
		profileAdapter = new ProfileListAdapter(profileModelList, getActivity());
		profile_listView.setAdapter(profileAdapter);
		
		profile_listView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
			    if(position == 0)
			    {
			    	Intent intent = new Intent(getActivity(), GenreListActivity.class);
					startActivity(intent);
			    }
			    if(position == 1)
			    {
			    	Intent intent = new Intent(getActivity(), ArtistListActivity.class);
					startActivity(intent);
			    }
			    if(position==2)
			    {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    // Setting Dialog Title
                    alertDialog.setTitle("Logout");

                    // Setting Dialog Message
                    alertDialog.setMessage("Are you sure you want to logout?");

                    // Setting Icon to Dialog
                    //alertDialog.setIcon(R.drawable.delete);

                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int which) {

                            Log.w("ProfileFragment Muko", "Logged out.");
                            logout = /*getPreferences(MODE_PRIVATE);*/getActivity().getSharedPreferences("LogoutTrigger", Context.MODE_PRIVATE);
                            Editor editor = logout.edit();
                            editor.putBoolean("TriggerLogout", true);
                            editor.commit();
                            //TODO Logout alert box
                            Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            dialog.cancel();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
			    }
			    else
			    {
			    	
			    }
			}
		});
//        
        
        if (actionBar != null)
        {
            // Disable the default and enable the custom action bar
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            View customView = getActivity().getLayoutInflater().inflate(R.layout.profile_screen_fragment_custom_actionbar, null);
            TextView iv_title = (TextView)customView.findViewById(R.id.profile_screen_fragment_custom_actionbar_title_text);
            iv_title.setTypeface(typeface_muko);
            //TextView tv_title = (TextView) customView.findViewById(R.id.home_custom_actionbar_title_text);
            //tv_title.setText("PROFILE");
            actionBar.setCustomView(customView);
        }
//        


        return rootView;
    }
	
	private void initList() 
	{
        // We populate the planets
       
		profileModelList.add(new ProfileModel("Genres",R.drawable.genre_icon,R.drawable.genre_icon));
		profileModelList.add(new ProfileModel("Artists",R.drawable.artists_icon,R.drawable.artists_icon));
		profileModelList.add(new ProfileModel("Logout",R.drawable.logout_icon,R.drawable.logout_icon));
    }


}
