package com.neurlabs.muko.fragments;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.neurlabs.muko.DemoActivity;
import com.neurlabs.muko.GenreListActivity;
import com.neurlabs.muko.HomeActivity;
import com.neurlabs.muko.MucoHelpActivity;
import com.neurlabs.muko.MukoPlayListActivity;
import com.neurlabs.muko.R;
import com.neurlabs.muko.RippleBackground;
import com.neurlabs.muko.utilis.CustomKeyboard;
import com.neurlabs.muko.utilis.DataThrower;
import com.neurlabs.muko.utilis.InternetStatus;
import com.neurlabs.muko.utilis.SoftKeyboard;
import com.nuance.nmdp.speechkit.Recognition;
import com.nuance.nmdp.speechkit.Recognizer;
import com.nuance.nmdp.speechkit.SpeechError;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class HomeFragment extends Fragment
{
	private Handler _handler = null;
	Button btn_record;
	Button btn_search;
    Button btn_clear;
	ImageView iv_help;
    ImageView demo_btn;
	EditText et_query;
	static RippleBackground rippleBackground;
	private boolean onTapped = false;
	String searchquery;
	DataThrower datathrower;
	ImageView iv_clearText;
	JSONObject songsThumbInfoJsonObject = new JSONObject();
	JSONObject songsThumbInfoJsonObject_null = new JSONObject();
	ProgressDialog genreProgressDialog;
	SharedPreferences nav_check;
	ActionBar actionBar;
    CustomKeyboard keyboard = new CustomKeyboard();
    InputMethodManager im;
    SharedPreferences speechText;
    private String LastUserQuery;

    InternetStatus internetStatus = new InternetStatus();
    Boolean netStatus;

    public HomeFragment(){}
	
    @Override
	public void onAttach(Activity a) {
	    super.onAttach(a);
//	    datathrower = (DataThrower) a;
	    actionBar = ((ActionBarActivity)a).getSupportActionBar();
	}
    
    public void passData(String UserQuery) {
	    datathrower.userQuery(UserQuery);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		
        View rootView = inflater.inflate(R.layout.fragment_home_layout, container, false);

        Typeface typeface_muko = Typeface.createFromAsset(getActivity().getAssets(),"futura-condensed-medium.ttf");
        btn_record = (Button) rootView.findViewById(R.id.fragment_home_layout_button_microphone);
        btn_search = (Button) rootView.findViewById(R.id.fragment_home_layout_button_search);
        iv_help = (ImageView) rootView.findViewById(R.id.fragment_home_layout_imageView_help);
        et_query = (EditText) rootView.findViewById(R.id.fragment_home_layout_edittext_query); 
        rippleBackground=(RippleBackground) rootView.findViewById(R.id.ripple_layout);
        iv_clearText = (ImageView) rootView.findViewById(R.id.fragment_home_layout_imageView_cleartext);
        btn_clear = (Button) rootView.findViewById(R.id.fragment_home_layout_imageView_clearbutton);
        et_query.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        demo_btn = (ImageView) rootView.findViewById(R.id.fragment_home_layout_imageView_demo);

        netStatus = internetStatus.isOnline(getActivity().getApplicationContext());
        btn_record.setTypeface(typeface_muko);
        btn_search.setTypeface(typeface_muko);
        et_query.setTypeface(typeface_muko);

        speechText = getActivity().getSharedPreferences("LASTUSERQUERY", getActivity().MODE_PRIVATE);
        LastUserQuery = speechText.getString("LastUserQuery","I feel like laughing!");

        skipperdude = getActivity().getSharedPreferences("skipperdude", getActivity().MODE_PRIVATE);

        et_query.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    et_query.setCursorVisible(true);
                } else
                    et_query.setCursorVisible(false);
            }
        });
        
        et_query.setText("I feel like laughing!");
        et_query.setCursorVisible(false);

        _handler = new Handler();
        _listener = createListener();
        _currentRecognizer = null;
        keyboardCheck(rippleBackground);

        im = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);

//        SoftKeyboard softKeyboard;
//        softKeyboard = new SoftKeyboard(, im);
//        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged()
//        {
//
//            @Override
//            public void onSoftKeyboardHide()
//            {
//                et_query.setCursorVisible(false);
//            }
//
//            @Override
//            public void onSoftKeyboardShow()
//            {
//                et_query.setCursorVisible(true);
//            }
//        });

        btn_clear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                iv_clearText.performClick();
            }
        });

        iv_help.setOnClickListener(new OnClickListener()
        {
			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), MucoHelpActivity.class);
				startActivity(intent);
			}
		});

        demo_btn.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                // Create a progressdialog
                genreProgressDialog = new ProgressDialog(getActivity());
                // Set progressdialog title
                genreProgressDialog.setTitle("Muko");
                // Set progressdialog message
                genreProgressDialog.setMessage("Loading...");
                genreProgressDialog.setIndeterminate(false);
                genreProgressDialog.setCancelable(false);
                genreProgressDialog.setCanceledOnTouchOutside(false);
                // Show progressdialog
                genreProgressDialog.show();

                if(skipperdude.getBoolean("isThisDudesASkipper",false)){
                    userAlerttoLoginProperly();
                }

                netStatus = internetStatus.isOnline(getActivity().getApplicationContext());
                if(netStatus)
                {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_query.getWindowToken(), 0);

                    SharedPreferences muko_pref = getActivity().getSharedPreferences("Muko", getActivity().MODE_PRIVATE);
                    final String userKey = muko_pref.getString("userKey", null);

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");
                    query.whereEqualTo("userKey",userKey);

                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> parseList, ParseException e) {
                            if (e == null) {
                                for (ParseObject parse : parseList) {
                                    String objectID = parse.getObjectId();
                                    String firstname = parse.getString("firstName");

                                    songsThumbInfoJsonObject = parse.getJSONObject("songsThumbInfo");
                                    JSONObject test = new JSONObject();
                                    test = songsThumbInfoJsonObject_null;

                                    SharedPreferences muko_pref = getActivity().getSharedPreferences("Muko", getActivity().MODE_PRIVATE);
                                    Editor editor_parse = muko_pref.edit();
                                    editor_parse.putString("objectid", objectID);
                                    editor_parse.commit();

                                    if (songsThumbInfoJsonObject == null) {
                                        songsThumbInfoJsonObject = songsThumbInfoJsonObject_null;
                                    }
                                }
                                genreProgressDialog.dismiss();
                                Intent intent = new Intent(getActivity(), DemoActivity.class);
                                intent.putExtra("speechquery", "Speed Test Demo");
                                intent.putExtra("songsThumbInfo_Object", songsThumbInfoJsonObject.toString());
                                String test = et_query.getText().toString();
                                saveLastUserQuery(test);
                                startActivity(intent);
                            } else {
                                genreProgressDialog.dismiss();
                                alert("Need Internet Connection");
                            }
                        }
                    });
                }
                else
                {
                    genreProgressDialog.dismiss();
                    alert("Need Internet Connection");
                }



            }
        });
        
        iv_clearText.setOnClickListener(new OnClickListener()
        {
			@Override
			public void onClick(View v)
			{
				et_query.setText("");
			}
		});
        
        btn_search.setOnClickListener(new OnClickListener()
        {
			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
    			// Create a progressdialog
    			genreProgressDialog = new ProgressDialog(getActivity());
    			// Set progressdialog title
    			genreProgressDialog.setTitle("Muko");
    			// Set progressdialog message
    			genreProgressDialog.setMessage("Loading...");
    			genreProgressDialog.setIndeterminate(false);
    			genreProgressDialog.setCancelable(false);
    			genreProgressDialog.setCanceledOnTouchOutside(false);
    			// Show progressdialog
    			genreProgressDialog.show();

                if(skipperdude.getBoolean("isThisDudesASkipper",false)){
                    userAlerttoLoginProperly();
                }

                netStatus = internetStatus.isOnline(getActivity().getApplicationContext());
                if(netStatus)
                {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et_query.getWindowToken(), 0);

                    SharedPreferences muko_pref = getActivity().getSharedPreferences("Muko", getActivity().MODE_PRIVATE);
                    final String userKey = muko_pref.getString("userKey", null);

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");
                    query.whereEqualTo("userKey",userKey);

                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> parseList, ParseException e) {
                            if (e == null) {
                                for (ParseObject parse : parseList) {
                                    String objectID = parse.getObjectId();
                                    String firstname = parse.getString("firstName");

                                    songsThumbInfoJsonObject = parse.getJSONObject("songsThumbInfo");
                                    JSONObject test = new JSONObject();
                                    test = songsThumbInfoJsonObject_null;

                                    SharedPreferences muko_pref = getActivity().getSharedPreferences("Muko", getActivity().MODE_PRIVATE);
                                    Editor editor_parse = muko_pref.edit();
                                    editor_parse.putString("objectid", objectID);
                                    editor_parse.commit();

                                    if (songsThumbInfoJsonObject == null) {
                                        songsThumbInfoJsonObject = songsThumbInfoJsonObject_null;
                                    }
                                }
                                genreProgressDialog.dismiss();
                                Intent intent = new Intent(getActivity(), MukoPlayListActivity.class);
                                intent.putExtra("speechquery", et_query.getText().toString());
                                intent.putExtra("songsThumbInfo_Object", songsThumbInfoJsonObject.toString());
                                String test = et_query.getText().toString();
                                saveLastUserQuery(test);
                                startActivity(intent);
                            } else {
                                genreProgressDialog.dismiss();
                                alert("Need Internet Connection");
                            }
                        }
                    });
                }
                else
                {
                    genreProgressDialog.dismiss();
                    alert("Need Internet Connection");
                }
    			

    			
			}
		});

        et_query.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                et_query.setCursorVisible(true);
            }
        });
        
        et_query.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    et_query.setCursorVisible(false);
        			// Create a progressdialog
        			genreProgressDialog = new ProgressDialog(getActivity());
        			// Set progressdialog title
        			genreProgressDialog.setTitle("Muko");
        			// Set progressdialog message
        			genreProgressDialog.setMessage("Loading...");
        			genreProgressDialog.setIndeterminate(false);
        			genreProgressDialog.setCancelable(false);
        			genreProgressDialog.setCanceledOnTouchOutside(false);
        			// Show progressdialog
        			genreProgressDialog.show();

                    if(skipperdude.getBoolean("isThisDudesASkipper",false)){
                        userAlerttoLoginProperly();
                    }

                    netStatus = internetStatus.isOnline(getActivity().getApplicationContext());
                    if(netStatus)
                    {
                        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(et_query.getWindowToken(), 0);

                        SharedPreferences muko_pref = getActivity().getSharedPreferences("Muko", getActivity().MODE_PRIVATE);
                        final String userKey = muko_pref.getString("userKey", null);

                        ParseQuery<ParseObject> query = ParseQuery.getQuery("MKRdioUser");
                        query.whereEqualTo("userKey",userKey);

                        query.findInBackground(new FindCallback<ParseObject>()
                        {
                            @Override
                            public void done(List<ParseObject> parseList, ParseException e)
                            {
                                if (e == null)
                                {
                                    for (ParseObject parse : parseList)
                                    {
                                        String objectID = parse.getObjectId();
                                        String firstname = parse.getString("firstName");

                                        songsThumbInfoJsonObject = parse.getJSONObject("songsThumbInfo");
                                        JSONObject test = new JSONObject();
                                        test = songsThumbInfoJsonObject ;

                                        SharedPreferences muko_pref = getActivity().getSharedPreferences("Muko", getActivity().MODE_PRIVATE);
                                        Editor editor_parse = muko_pref.edit();
                                        editor_parse.putString("objectid", objectID);
                                        editor_parse.commit();

                                        if (songsThumbInfoJsonObject == null)
                                        {
                                            songsThumbInfoJsonObject = songsThumbInfoJsonObject_null;
                                        }
                                    }
                                    genreProgressDialog.dismiss();
                                    Intent intent = new Intent(getActivity(), MukoPlayListActivity.class);
                                    intent.putExtra("speechquery", et_query.getText().toString() );
                                    intent.putExtra("songsThumbInfo_Object", songsThumbInfoJsonObject.toString() );
                                    String test = et_query.getText().toString();
                                    saveLastUserQuery(test);
                                    startActivity(intent);
                                }
                                else
                                {
                                    genreProgressDialog.dismiss();
                                    alert("Error");
                                }
                            }
                        });
                    }
                    else
                    {
                        genreProgressDialog.dismiss();
                        alert("Need Internet Connection");
                    }
                	

    				
                    return true;
                }
                return false;
            }
        });
        
        btn_record.setOnClickListener(new OnClickListener()
        {
			@Override
			public void onClick(View v)
			{
                if(recordingUserQuery){
                    rippleBackground.stopRippleAnimation();
                    et_query.setText(LastUserQuery);
                    btn_search.setEnabled(true);
                    iv_clearText.setEnabled(true);
                    et_query.setEnabled(true);
                    btn_record.setBackgroundResource(R.drawable.microphone_btn);
                    _currentRecognizer.cancel();

                    return;
                }
				rippleBackground.startRippleAnimation();
				btn_search.setEnabled(false);
                iv_clearText.setEnabled(false);
                et_query.setText("Recoginizing Speech Query...");
                et_query.setEnabled(false);
				btn_record.setBackgroundResource(R.drawable.record_microphone);
	            _currentRecognizer = HomeActivity.getSpeechKit().createRecognizer(Recognizer.RecognizerType.Dictation, Recognizer.EndOfSpeechDetection.Long, "en_US", _listener, _handler);
	            
	            _currentRecognizer.start();
			}
		});
//        
        
        if (actionBar != null)
        {
            // Disable the default and enable the custom action bar
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            View customView = getActivity().getLayoutInflater().inflate(R.layout.home_fragment_custom_actionbar, null);
            TextView tv_title = (TextView) customView.findViewById(R.id.monthlysavings_fragment_custom_actionbar_title_text);
            tv_title.setTypeface(typeface_muko);
            //TextView tv_title = (TextView) customView.findViewById(R.id.home_custom_actionbar_title_text);
            //tv_title.setText("PROFILE");
            actionBar.setCustomView(customView);
        }
        
//        Editor editor = nav_check.edit();
//		editor.clear();
//		editor.commit();
        
        
        return rootView;
    }
    SharedPreferences skipperdude;
    /**
     * Alert User to Login
     */
    public void userAlerttoLoginProperly(){
        if(skipperdude.getBoolean("isThisDudesASkipper",false)){
            Toast.makeText(getActivity(),"Songs of 30 seconds each, for a better experience please Logout and Login with your Premium rdio" +
                    " account",Toast.LENGTH_LONG).show();
        }
    }

    /***
     * Save's Last User Query to Shard Preference
     * @param newQuery
     */
    public void saveLastUserQuery(String newQuery){
        Editor x = speechText.edit();
        x.putString("LastUserQuery", newQuery);
        x.commit();
    }

    @Override
    public void onResume(){
        super.onResume();
        LastUserQuery = speechText.getString("LastUserQuery","I feel like laughing!");
        et_query.setText(LastUserQuery);
//        Toast.makeText(getActivity(),"On Resume in Fragment called",Toast.LENGTH_LONG).show();
    }

	
	@Override
	public void onDestroy(){
		super.onDestroy();
	    if (_currentRecognizer !=  null)
	    {
	        _currentRecognizer.cancel();
	        _currentRecognizer = null;
	    }
	}
	
	private Recognizer.Listener _listener;
	private Recognizer _currentRecognizer;
    boolean recordingUserQuery = false;

	private Recognizer.Listener createListener()
	{
	    return new Recognizer.Listener()
	    {            
	        @Override
	        public void onRecordingBegin(Recognizer recognizer) 
	        {

                recordingUserQuery = true;

                Runnable r = new Runnable()
                {
                    @Override
					public void run()
                    {
                        if ( _currentRecognizer != null)
                        {
                            _handler.postDelayed(this, 500);
                        }
                    }
                };
                r.run();
	        	
	        }

	        @Override
	        public void onRecordingDone(Recognizer recognizer) 
	        {
                recordingUserQuery = false;
	        }

	        @Override
	        public void onError(Recognizer recognizer, SpeechError error) 
	        {
	        	if (recognizer != _currentRecognizer) return;
	            _currentRecognizer = null;

	            // Display the error + suggestion in the edit box
	            String detail = error.getErrorDetail();
	            String suggestion = error.getSuggestion();
	            
	            if (suggestion == null) suggestion = "";
//	            setResult(detail + "\n" + suggestion)
	            showerroralert(detail + "\n" + suggestion);
	            btn_record.setBackgroundResource(R.drawable.microphone_btn);
	            rippleBackground.stopRippleAnimation();
                et_query.setEnabled(true);
                iv_clearText.setEnabled(true);
                et_query.setText(LastUserQuery);
	            btn_search.setEnabled(true);
	            // for debugging purpose: printing out the speechkit session id
	            android.util.Log.d("Nuance SampleVoiceApp", "Recognizer.Listener.onError: session id ["
	                    + HomeActivity.getSpeechKit().getSessionId() + "]");
	        }

	        @Override
	        public void onResults(Recognizer recognizer, Recognition results) {
	            _currentRecognizer = null;
	            int count = results.getResultCount();
	            Recognition.Result [] rs = new Recognition.Result[count];
	            for (int i = 0; i < count; i++)
	            {
	                rs[i] = results.getResult(i);
	            }
	            setResults(rs);
	            // for debugging purpose: printing out the speechkit session id
	            android.util.Log.d("Nuance SampleVoiceApp", "Recognizer.Listener.onResults: session id ["
	                    + HomeActivity.getSpeechKit().getSessionId() + "]");
	        }
	    };
	}
	
	public void showerroralert(String message){
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}
	
	private void setResult(String result)
	{
		et_query.setText(result);
		btn_record.setBackgroundResource(R.drawable.microphone_btn);
		rippleBackground.stopRippleAnimation();
		searchquery = result;
//		passData(result);
        iv_clearText.setEnabled(true);
        et_query.setEnabled(true);
		btn_search.setEnabled(true);
		btn_search.performClick();
	}

	private void setResults(Recognition.Result[] results)
	{
//	    _arrayAdapter.clear();
	    if (results.length > 0)
	    {
	        setResult(results[0].getText());
	        
//	        for (int i = 0; i < results.length; i++)
//	            _arrayAdapter.add("[" + results[i].getScore() + "]: " + results[i].getText());
	    }  else
	    {
	        setResult("");
	    }
	}

    public void keyboardCheck(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    et_query.setCursorVisible(false);
                    keyboard.hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                keyboardCheck(innerView);
            }
        }
    }

    public void wearRequest(String query)
    {
        et_query.setText(query);
        searchquery = query;
//		passData(result);
        btn_search.setEnabled(true);
        btn_search.performClick();
    }

    /**
     * Shows Alert Dialog. Accept string parameter
     * */
    @SuppressWarnings("deprecation")
    public void alert(String message)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

        // Setting Dialog Title
        alertDialog.setTitle("MUKO");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.error);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //TODO Close the app
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


}
