package com.neurlabs.muko;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class MucoHelpActivity extends ActionBarActivity
{
	Button btn_gotit;
    TextView tv_line1;
    TextView tv_line2;
    TextView tv_line3;
    TextView tv_line4;
    TextView tv_line5;
    TextView tv_line6;
    TextView tv_line7;
    TextView tv_line8;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
//		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
	    getSupportActionBar().hide();
		setContentView(R.layout.activity_muco_help);

        Typeface typeface_muko = Typeface.createFromAsset(getAssets(),"futura-condensed-medium.ttf");
	
		btn_gotit = (Button) findViewById(R.id.muco_help_activity_button_gotit);
        tv_line1 = (TextView) findViewById(R.id.muco_help_activity_textview_line1);
        tv_line2 = (TextView) findViewById(R.id.muco_help_activity_textview_line2);
        tv_line3 = (TextView) findViewById(R.id.muco_help_activity_textview_line3);
        tv_line4 = (TextView) findViewById(R.id.muco_help_activity_textview_line4);
        tv_line5 = (TextView) findViewById(R.id.muco_help_activity_textview_line5);
        tv_line6 = (TextView) findViewById(R.id.muco_help_activity_textview_line6);
        tv_line7 = (TextView) findViewById(R.id.muco_help_activity_textview_line7);
        tv_line8 = (TextView) findViewById(R.id.muco_help_activity_textview_line8);

        btn_gotit.setTypeface(typeface_muko);
        tv_line1.setTypeface(typeface_muko);
        tv_line2.setTypeface(typeface_muko);
        tv_line3.setTypeface(typeface_muko);
        tv_line4.setTypeface(typeface_muko);
        tv_line5.setTypeface(typeface_muko);
        tv_line6.setTypeface(typeface_muko);
        tv_line7.setTypeface(typeface_muko);
        tv_line8.setTypeface(typeface_muko);
		
		btn_gotit.setOnClickListener(new OnClickListener()
		{	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
			}
		});
	}

	
}
